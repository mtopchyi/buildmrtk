﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void ParseWind::OnChangeSlider()
extern void ParseWind_OnChangeSlider_mCB56CDC37CBF3A50C793FD7F8945D162EFC5D27C (void);
// 0x00000002 System.Void ParseWind::.ctor()
extern void ParseWind__ctor_m78A0136F82E8E27607D96C94A261624FF90B9635 (void);
// 0x00000003 System.Void PartOfWind::ChangePosition(System.Single)
extern void PartOfWind_ChangePosition_mC74BF7EA14CD257939991A4F213C8E369EEFBD9F (void);
// 0x00000004 System.Void PartOfWind::.ctor()
extern void PartOfWind__ctor_m10C8BC534CA744F4F4223FAA565CFF34C96AFAC6 (void);
// 0x00000005 System.Void AnchorsManager::ChangeStateAll()
extern void AnchorsManager_ChangeStateAll_mD15B5C9DAEF00229FC0B69D150E56F1A2855EEE1 (void);
// 0x00000006 System.Void AnchorsManager::Awake()
extern void AnchorsManager_Awake_m998C316C90A5BFC3B413F0DCDD2B9E7D1864F72F (void);
// 0x00000007 System.Void AnchorsManager::Start()
extern void AnchorsManager_Start_m54FAFAC841140314D22F7BD34227F9DB3744ADA8 (void);
// 0x00000008 System.Void AnchorsManager::AllAnchorsTap()
extern void AnchorsManager_AllAnchorsTap_mA4F92305136FA54EDF0A19EBB964E49C247D8402 (void);
// 0x00000009 System.Void AnchorsManager::SubscribeOnEventAnchors()
extern void AnchorsManager_SubscribeOnEventAnchors_m0074851CA3D90C97A4411AAD0D39F6CE42D8D37C (void);
// 0x0000000A System.Void AnchorsManager::ChangeActiveAnchor(TooltipSpawner)
extern void AnchorsManager_ChangeActiveAnchor_mA4FBD1A34D1A68C468A15406D7EA691C32DE64C2 (void);
// 0x0000000B System.Void AnchorsManager::.ctor()
extern void AnchorsManager__ctor_mFC337F78F0246302DD8D670D81E4ECF0160BBC43 (void);
// 0x0000000C System.Void TooltipSpawner::add_Tapped(System.Action`1<TooltipSpawner>)
extern void TooltipSpawner_add_Tapped_mE772ACF3E46C85E8B4098A0953DC7715277FFC5F (void);
// 0x0000000D System.Void TooltipSpawner::remove_Tapped(System.Action`1<TooltipSpawner>)
extern void TooltipSpawner_remove_Tapped_mF6EC004071BA6B3CC042FA9DA05DC865DE4EB62B (void);
// 0x0000000E System.Void TooltipSpawner::ChangeState()
extern void TooltipSpawner_ChangeState_m87A3F1566826F2DB0C9F69CBA8132B75240D2AC9 (void);
// 0x0000000F System.Void TooltipSpawner::Tap()
extern void TooltipSpawner_Tap_mDFA9C2D75152A9CC0B8D1D798EE8093817D2BC16 (void);
// 0x00000010 System.Void TooltipSpawner::HandleTap()
extern void TooltipSpawner_HandleTap_m7F4509FC63025A814839006EFFD9620408CF8F19 (void);
// 0x00000011 System.Void TooltipSpawner::Awake()
extern void TooltipSpawner_Awake_m61F616FF5759F13C81D5FD823675B8EEF52EC534 (void);
// 0x00000012 System.Void TooltipSpawner::Update()
extern void TooltipSpawner_Update_mFEB1B21CAE84D1C66FBE5272F21213C8C976AF0A (void);
// 0x00000013 System.Void TooltipSpawner::.ctor()
extern void TooltipSpawner__ctor_m519001FF8E4A450994F1DFDA970FEDE5DF58C07A (void);
// 0x00000014 System.Void AnimationWindTurbine::ToggleAnimation()
extern void AnimationWindTurbine_ToggleAnimation_m9ADB35EDB97DB6BA9854738BCE55AE421F692A40 (void);
// 0x00000015 System.Void AnimationWindTurbine::Animate()
extern void AnimationWindTurbine_Animate_m0ABDDA2D69816A58BC2769EC7FA6FD1D5D783F14 (void);
// 0x00000016 System.Void AnimationWindTurbine::Awake()
extern void AnimationWindTurbine_Awake_m1F9DE8734EE59144A9CD695318FD1EE0BA0396D9 (void);
// 0x00000017 System.Void AnimationWindTurbine::Update()
extern void AnimationWindTurbine_Update_m3F1927A1B65B1B63CBEA1E07379D3857198F71FA (void);
// 0x00000018 System.Void AnimationWindTurbine::.ctor()
extern void AnimationWindTurbine__ctor_mFFFA2F5ED8BEFEA28F1A12FFC6D8220C3321F992 (void);
static Il2CppMethodPointer s_methodPointers[24] = 
{
	ParseWind_OnChangeSlider_mCB56CDC37CBF3A50C793FD7F8945D162EFC5D27C,
	ParseWind__ctor_m78A0136F82E8E27607D96C94A261624FF90B9635,
	PartOfWind_ChangePosition_mC74BF7EA14CD257939991A4F213C8E369EEFBD9F,
	PartOfWind__ctor_m10C8BC534CA744F4F4223FAA565CFF34C96AFAC6,
	AnchorsManager_ChangeStateAll_mD15B5C9DAEF00229FC0B69D150E56F1A2855EEE1,
	AnchorsManager_Awake_m998C316C90A5BFC3B413F0DCDD2B9E7D1864F72F,
	AnchorsManager_Start_m54FAFAC841140314D22F7BD34227F9DB3744ADA8,
	AnchorsManager_AllAnchorsTap_mA4F92305136FA54EDF0A19EBB964E49C247D8402,
	AnchorsManager_SubscribeOnEventAnchors_m0074851CA3D90C97A4411AAD0D39F6CE42D8D37C,
	AnchorsManager_ChangeActiveAnchor_mA4FBD1A34D1A68C468A15406D7EA691C32DE64C2,
	AnchorsManager__ctor_mFC337F78F0246302DD8D670D81E4ECF0160BBC43,
	TooltipSpawner_add_Tapped_mE772ACF3E46C85E8B4098A0953DC7715277FFC5F,
	TooltipSpawner_remove_Tapped_mF6EC004071BA6B3CC042FA9DA05DC865DE4EB62B,
	TooltipSpawner_ChangeState_m87A3F1566826F2DB0C9F69CBA8132B75240D2AC9,
	TooltipSpawner_Tap_mDFA9C2D75152A9CC0B8D1D798EE8093817D2BC16,
	TooltipSpawner_HandleTap_m7F4509FC63025A814839006EFFD9620408CF8F19,
	TooltipSpawner_Awake_m61F616FF5759F13C81D5FD823675B8EEF52EC534,
	TooltipSpawner_Update_mFEB1B21CAE84D1C66FBE5272F21213C8C976AF0A,
	TooltipSpawner__ctor_m519001FF8E4A450994F1DFDA970FEDE5DF58C07A,
	AnimationWindTurbine_ToggleAnimation_m9ADB35EDB97DB6BA9854738BCE55AE421F692A40,
	AnimationWindTurbine_Animate_m0ABDDA2D69816A58BC2769EC7FA6FD1D5D783F14,
	AnimationWindTurbine_Awake_m1F9DE8734EE59144A9CD695318FD1EE0BA0396D9,
	AnimationWindTurbine_Update_m3F1927A1B65B1B63CBEA1E07379D3857198F71FA,
	AnimationWindTurbine__ctor_mFFFA2F5ED8BEFEA28F1A12FFC6D8220C3321F992,
};
static const int32_t s_InvokerIndices[24] = 
{
	4512,
	4512,
	3698,
	4512,
	4512,
	4512,
	4512,
	4512,
	4512,
	3660,
	4512,
	3660,
	3660,
	4512,
	4512,
	4512,
	4512,
	4512,
	4512,
	4512,
	4512,
	4512,
	4512,
	4512,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	24,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
