﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// UnityEngine.InputSystem.Layouts.InputControlAttribute
struct InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8;
// UnityEngine.InputSystem.Layouts.InputControlLayoutAttribute
struct InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C;
// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// Microsoft.MixedReality.OpenXR.NativeLibTokenAttribute
struct NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeType* U3CConnectU3Ed__4_t99E03D1D6720EB9A51A3076B4D7D7087A794913D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadAsyncU3Ed__0_tEA68C7403FA60BAFB05AD91AD233559E5B8D9117_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadAsyncU3Ed__6_tA92A8FCF33BC6C780B51AD9E5F2F7262A63AA788_0_0_0_var;

struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct  ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.FlagsAttribute
struct  FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct  InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.CompilerServices.InternalsVisibleToAttribute::_assemblyName
	String_t* ____assemblyName_0;
	// System.Boolean System.Runtime.CompilerServices.InternalsVisibleToAttribute::_allInternalsVisible
	bool ____allInternalsVisible_1;

public:
	inline static int32_t get_offset_of__assemblyName_0() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____assemblyName_0)); }
	inline String_t* get__assemblyName_0() const { return ____assemblyName_0; }
	inline String_t** get_address_of__assemblyName_0() { return &____assemblyName_0; }
	inline void set__assemblyName_0(String_t* value)
	{
		____assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____assemblyName_0), (void*)value);
	}

	inline static int32_t get_offset_of__allInternalsVisible_1() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____allInternalsVisible_1)); }
	inline bool get__allInternalsVisible_1() const { return ____allInternalsVisible_1; }
	inline bool* get_address_of__allInternalsVisible_1() { return &____allInternalsVisible_1; }
	inline void set__allInternalsVisible_1(bool value)
	{
		____allInternalsVisible_1 = value;
	}
};


// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct  IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.ObsoleteAttribute
struct  ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// UnityEngine.Scripting.PreserveAttribute
struct  PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct  SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct  StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.UInt32
struct  UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct  AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.AttributeTargets
struct  AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.Layouts.InputControlAttribute
struct  InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.InputSystem.Layouts.InputControlAttribute::<layout>k__BackingField
	String_t* ___U3ClayoutU3Ek__BackingField_0;
	// System.String UnityEngine.InputSystem.Layouts.InputControlAttribute::<variants>k__BackingField
	String_t* ___U3CvariantsU3Ek__BackingField_1;
	// System.String UnityEngine.InputSystem.Layouts.InputControlAttribute::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_2;
	// System.String UnityEngine.InputSystem.Layouts.InputControlAttribute::<format>k__BackingField
	String_t* ___U3CformatU3Ek__BackingField_3;
	// System.String UnityEngine.InputSystem.Layouts.InputControlAttribute::<usage>k__BackingField
	String_t* ___U3CusageU3Ek__BackingField_4;
	// System.String[] UnityEngine.InputSystem.Layouts.InputControlAttribute::<usages>k__BackingField
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___U3CusagesU3Ek__BackingField_5;
	// System.String UnityEngine.InputSystem.Layouts.InputControlAttribute::<parameters>k__BackingField
	String_t* ___U3CparametersU3Ek__BackingField_6;
	// System.String UnityEngine.InputSystem.Layouts.InputControlAttribute::<processors>k__BackingField
	String_t* ___U3CprocessorsU3Ek__BackingField_7;
	// System.String UnityEngine.InputSystem.Layouts.InputControlAttribute::<alias>k__BackingField
	String_t* ___U3CaliasU3Ek__BackingField_8;
	// System.String[] UnityEngine.InputSystem.Layouts.InputControlAttribute::<aliases>k__BackingField
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___U3CaliasesU3Ek__BackingField_9;
	// System.String UnityEngine.InputSystem.Layouts.InputControlAttribute::<useStateFrom>k__BackingField
	String_t* ___U3CuseStateFromU3Ek__BackingField_10;
	// System.UInt32 UnityEngine.InputSystem.Layouts.InputControlAttribute::<bit>k__BackingField
	uint32_t ___U3CbitU3Ek__BackingField_11;
	// System.UInt32 UnityEngine.InputSystem.Layouts.InputControlAttribute::<offset>k__BackingField
	uint32_t ___U3CoffsetU3Ek__BackingField_12;
	// System.UInt32 UnityEngine.InputSystem.Layouts.InputControlAttribute::<sizeInBits>k__BackingField
	uint32_t ___U3CsizeInBitsU3Ek__BackingField_13;
	// System.Int32 UnityEngine.InputSystem.Layouts.InputControlAttribute::<arraySize>k__BackingField
	int32_t ___U3CarraySizeU3Ek__BackingField_14;
	// System.String UnityEngine.InputSystem.Layouts.InputControlAttribute::<displayName>k__BackingField
	String_t* ___U3CdisplayNameU3Ek__BackingField_15;
	// System.String UnityEngine.InputSystem.Layouts.InputControlAttribute::<shortDisplayName>k__BackingField
	String_t* ___U3CshortDisplayNameU3Ek__BackingField_16;
	// System.Boolean UnityEngine.InputSystem.Layouts.InputControlAttribute::<noisy>k__BackingField
	bool ___U3CnoisyU3Ek__BackingField_17;
	// System.Boolean UnityEngine.InputSystem.Layouts.InputControlAttribute::<synthetic>k__BackingField
	bool ___U3CsyntheticU3Ek__BackingField_18;
	// System.Object UnityEngine.InputSystem.Layouts.InputControlAttribute::<defaultState>k__BackingField
	RuntimeObject * ___U3CdefaultStateU3Ek__BackingField_19;
	// System.Object UnityEngine.InputSystem.Layouts.InputControlAttribute::<minValue>k__BackingField
	RuntimeObject * ___U3CminValueU3Ek__BackingField_20;
	// System.Object UnityEngine.InputSystem.Layouts.InputControlAttribute::<maxValue>k__BackingField
	RuntimeObject * ___U3CmaxValueU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_U3ClayoutU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3ClayoutU3Ek__BackingField_0)); }
	inline String_t* get_U3ClayoutU3Ek__BackingField_0() const { return ___U3ClayoutU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3ClayoutU3Ek__BackingField_0() { return &___U3ClayoutU3Ek__BackingField_0; }
	inline void set_U3ClayoutU3Ek__BackingField_0(String_t* value)
	{
		___U3ClayoutU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ClayoutU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CvariantsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CvariantsU3Ek__BackingField_1)); }
	inline String_t* get_U3CvariantsU3Ek__BackingField_1() const { return ___U3CvariantsU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CvariantsU3Ek__BackingField_1() { return &___U3CvariantsU3Ek__BackingField_1; }
	inline void set_U3CvariantsU3Ek__BackingField_1(String_t* value)
	{
		___U3CvariantsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CvariantsU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CnameU3Ek__BackingField_2)); }
	inline String_t* get_U3CnameU3Ek__BackingField_2() const { return ___U3CnameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_2() { return &___U3CnameU3Ek__BackingField_2; }
	inline void set_U3CnameU3Ek__BackingField_2(String_t* value)
	{
		___U3CnameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CformatU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CformatU3Ek__BackingField_3)); }
	inline String_t* get_U3CformatU3Ek__BackingField_3() const { return ___U3CformatU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CformatU3Ek__BackingField_3() { return &___U3CformatU3Ek__BackingField_3; }
	inline void set_U3CformatU3Ek__BackingField_3(String_t* value)
	{
		___U3CformatU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CformatU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CusageU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CusageU3Ek__BackingField_4)); }
	inline String_t* get_U3CusageU3Ek__BackingField_4() const { return ___U3CusageU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CusageU3Ek__BackingField_4() { return &___U3CusageU3Ek__BackingField_4; }
	inline void set_U3CusageU3Ek__BackingField_4(String_t* value)
	{
		___U3CusageU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CusageU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CusagesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CusagesU3Ek__BackingField_5)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_U3CusagesU3Ek__BackingField_5() const { return ___U3CusagesU3Ek__BackingField_5; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_U3CusagesU3Ek__BackingField_5() { return &___U3CusagesU3Ek__BackingField_5; }
	inline void set_U3CusagesU3Ek__BackingField_5(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___U3CusagesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CusagesU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparametersU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CparametersU3Ek__BackingField_6)); }
	inline String_t* get_U3CparametersU3Ek__BackingField_6() const { return ___U3CparametersU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CparametersU3Ek__BackingField_6() { return &___U3CparametersU3Ek__BackingField_6; }
	inline void set_U3CparametersU3Ek__BackingField_6(String_t* value)
	{
		___U3CparametersU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CparametersU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CprocessorsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CprocessorsU3Ek__BackingField_7)); }
	inline String_t* get_U3CprocessorsU3Ek__BackingField_7() const { return ___U3CprocessorsU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CprocessorsU3Ek__BackingField_7() { return &___U3CprocessorsU3Ek__BackingField_7; }
	inline void set_U3CprocessorsU3Ek__BackingField_7(String_t* value)
	{
		___U3CprocessorsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CprocessorsU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CaliasU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CaliasU3Ek__BackingField_8)); }
	inline String_t* get_U3CaliasU3Ek__BackingField_8() const { return ___U3CaliasU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CaliasU3Ek__BackingField_8() { return &___U3CaliasU3Ek__BackingField_8; }
	inline void set_U3CaliasU3Ek__BackingField_8(String_t* value)
	{
		___U3CaliasU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CaliasU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CaliasesU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CaliasesU3Ek__BackingField_9)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_U3CaliasesU3Ek__BackingField_9() const { return ___U3CaliasesU3Ek__BackingField_9; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_U3CaliasesU3Ek__BackingField_9() { return &___U3CaliasesU3Ek__BackingField_9; }
	inline void set_U3CaliasesU3Ek__BackingField_9(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___U3CaliasesU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CaliasesU3Ek__BackingField_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseStateFromU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CuseStateFromU3Ek__BackingField_10)); }
	inline String_t* get_U3CuseStateFromU3Ek__BackingField_10() const { return ___U3CuseStateFromU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CuseStateFromU3Ek__BackingField_10() { return &___U3CuseStateFromU3Ek__BackingField_10; }
	inline void set_U3CuseStateFromU3Ek__BackingField_10(String_t* value)
	{
		___U3CuseStateFromU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CuseStateFromU3Ek__BackingField_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CbitU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CbitU3Ek__BackingField_11)); }
	inline uint32_t get_U3CbitU3Ek__BackingField_11() const { return ___U3CbitU3Ek__BackingField_11; }
	inline uint32_t* get_address_of_U3CbitU3Ek__BackingField_11() { return &___U3CbitU3Ek__BackingField_11; }
	inline void set_U3CbitU3Ek__BackingField_11(uint32_t value)
	{
		___U3CbitU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CoffsetU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CoffsetU3Ek__BackingField_12)); }
	inline uint32_t get_U3CoffsetU3Ek__BackingField_12() const { return ___U3CoffsetU3Ek__BackingField_12; }
	inline uint32_t* get_address_of_U3CoffsetU3Ek__BackingField_12() { return &___U3CoffsetU3Ek__BackingField_12; }
	inline void set_U3CoffsetU3Ek__BackingField_12(uint32_t value)
	{
		___U3CoffsetU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CsizeInBitsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CsizeInBitsU3Ek__BackingField_13)); }
	inline uint32_t get_U3CsizeInBitsU3Ek__BackingField_13() const { return ___U3CsizeInBitsU3Ek__BackingField_13; }
	inline uint32_t* get_address_of_U3CsizeInBitsU3Ek__BackingField_13() { return &___U3CsizeInBitsU3Ek__BackingField_13; }
	inline void set_U3CsizeInBitsU3Ek__BackingField_13(uint32_t value)
	{
		___U3CsizeInBitsU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CarraySizeU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CarraySizeU3Ek__BackingField_14)); }
	inline int32_t get_U3CarraySizeU3Ek__BackingField_14() const { return ___U3CarraySizeU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CarraySizeU3Ek__BackingField_14() { return &___U3CarraySizeU3Ek__BackingField_14; }
	inline void set_U3CarraySizeU3Ek__BackingField_14(int32_t value)
	{
		___U3CarraySizeU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CdisplayNameU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CdisplayNameU3Ek__BackingField_15)); }
	inline String_t* get_U3CdisplayNameU3Ek__BackingField_15() const { return ___U3CdisplayNameU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CdisplayNameU3Ek__BackingField_15() { return &___U3CdisplayNameU3Ek__BackingField_15; }
	inline void set_U3CdisplayNameU3Ek__BackingField_15(String_t* value)
	{
		___U3CdisplayNameU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdisplayNameU3Ek__BackingField_15), (void*)value);
	}

	inline static int32_t get_offset_of_U3CshortDisplayNameU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CshortDisplayNameU3Ek__BackingField_16)); }
	inline String_t* get_U3CshortDisplayNameU3Ek__BackingField_16() const { return ___U3CshortDisplayNameU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CshortDisplayNameU3Ek__BackingField_16() { return &___U3CshortDisplayNameU3Ek__BackingField_16; }
	inline void set_U3CshortDisplayNameU3Ek__BackingField_16(String_t* value)
	{
		___U3CshortDisplayNameU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CshortDisplayNameU3Ek__BackingField_16), (void*)value);
	}

	inline static int32_t get_offset_of_U3CnoisyU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CnoisyU3Ek__BackingField_17)); }
	inline bool get_U3CnoisyU3Ek__BackingField_17() const { return ___U3CnoisyU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CnoisyU3Ek__BackingField_17() { return &___U3CnoisyU3Ek__BackingField_17; }
	inline void set_U3CnoisyU3Ek__BackingField_17(bool value)
	{
		___U3CnoisyU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CsyntheticU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CsyntheticU3Ek__BackingField_18)); }
	inline bool get_U3CsyntheticU3Ek__BackingField_18() const { return ___U3CsyntheticU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CsyntheticU3Ek__BackingField_18() { return &___U3CsyntheticU3Ek__BackingField_18; }
	inline void set_U3CsyntheticU3Ek__BackingField_18(bool value)
	{
		___U3CsyntheticU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CdefaultStateU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CdefaultStateU3Ek__BackingField_19)); }
	inline RuntimeObject * get_U3CdefaultStateU3Ek__BackingField_19() const { return ___U3CdefaultStateU3Ek__BackingField_19; }
	inline RuntimeObject ** get_address_of_U3CdefaultStateU3Ek__BackingField_19() { return &___U3CdefaultStateU3Ek__BackingField_19; }
	inline void set_U3CdefaultStateU3Ek__BackingField_19(RuntimeObject * value)
	{
		___U3CdefaultStateU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdefaultStateU3Ek__BackingField_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3CminValueU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CminValueU3Ek__BackingField_20)); }
	inline RuntimeObject * get_U3CminValueU3Ek__BackingField_20() const { return ___U3CminValueU3Ek__BackingField_20; }
	inline RuntimeObject ** get_address_of_U3CminValueU3Ek__BackingField_20() { return &___U3CminValueU3Ek__BackingField_20; }
	inline void set_U3CminValueU3Ek__BackingField_20(RuntimeObject * value)
	{
		___U3CminValueU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CminValueU3Ek__BackingField_20), (void*)value);
	}

	inline static int32_t get_offset_of_U3CmaxValueU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8, ___U3CmaxValueU3Ek__BackingField_21)); }
	inline RuntimeObject * get_U3CmaxValueU3Ek__BackingField_21() const { return ___U3CmaxValueU3Ek__BackingField_21; }
	inline RuntimeObject ** get_address_of_U3CmaxValueU3Ek__BackingField_21() { return &___U3CmaxValueU3Ek__BackingField_21; }
	inline void set_U3CmaxValueU3Ek__BackingField_21(RuntimeObject * value)
	{
		___U3CmaxValueU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmaxValueU3Ek__BackingField_21), (void*)value);
	}
};


// UnityEngine.InputSystem.Layouts.InputControlLayoutAttribute
struct  InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.InputSystem.Layouts.InputControlLayoutAttribute::<stateType>k__BackingField
	Type_t * ___U3CstateTypeU3Ek__BackingField_0;
	// System.String UnityEngine.InputSystem.Layouts.InputControlLayoutAttribute::<stateFormat>k__BackingField
	String_t* ___U3CstateFormatU3Ek__BackingField_1;
	// System.String[] UnityEngine.InputSystem.Layouts.InputControlLayoutAttribute::<commonUsages>k__BackingField
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___U3CcommonUsagesU3Ek__BackingField_2;
	// System.String UnityEngine.InputSystem.Layouts.InputControlLayoutAttribute::<variants>k__BackingField
	String_t* ___U3CvariantsU3Ek__BackingField_3;
	// System.Nullable`1<System.Boolean> UnityEngine.InputSystem.Layouts.InputControlLayoutAttribute::updateBeforeRenderInternal
	Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  ___updateBeforeRenderInternal_4;
	// System.Boolean UnityEngine.InputSystem.Layouts.InputControlLayoutAttribute::<isGenericTypeOfDevice>k__BackingField
	bool ___U3CisGenericTypeOfDeviceU3Ek__BackingField_5;
	// System.String UnityEngine.InputSystem.Layouts.InputControlLayoutAttribute::<displayName>k__BackingField
	String_t* ___U3CdisplayNameU3Ek__BackingField_6;
	// System.String UnityEngine.InputSystem.Layouts.InputControlLayoutAttribute::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_7;
	// System.Boolean UnityEngine.InputSystem.Layouts.InputControlLayoutAttribute::<hideInUI>k__BackingField
	bool ___U3ChideInUIU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CstateTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984, ___U3CstateTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CstateTypeU3Ek__BackingField_0() const { return ___U3CstateTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CstateTypeU3Ek__BackingField_0() { return &___U3CstateTypeU3Ek__BackingField_0; }
	inline void set_U3CstateTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CstateTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CstateTypeU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CstateFormatU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984, ___U3CstateFormatU3Ek__BackingField_1)); }
	inline String_t* get_U3CstateFormatU3Ek__BackingField_1() const { return ___U3CstateFormatU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CstateFormatU3Ek__BackingField_1() { return &___U3CstateFormatU3Ek__BackingField_1; }
	inline void set_U3CstateFormatU3Ek__BackingField_1(String_t* value)
	{
		___U3CstateFormatU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CstateFormatU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcommonUsagesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984, ___U3CcommonUsagesU3Ek__BackingField_2)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_U3CcommonUsagesU3Ek__BackingField_2() const { return ___U3CcommonUsagesU3Ek__BackingField_2; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_U3CcommonUsagesU3Ek__BackingField_2() { return &___U3CcommonUsagesU3Ek__BackingField_2; }
	inline void set_U3CcommonUsagesU3Ek__BackingField_2(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___U3CcommonUsagesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcommonUsagesU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CvariantsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984, ___U3CvariantsU3Ek__BackingField_3)); }
	inline String_t* get_U3CvariantsU3Ek__BackingField_3() const { return ___U3CvariantsU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CvariantsU3Ek__BackingField_3() { return &___U3CvariantsU3Ek__BackingField_3; }
	inline void set_U3CvariantsU3Ek__BackingField_3(String_t* value)
	{
		___U3CvariantsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CvariantsU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_updateBeforeRenderInternal_4() { return static_cast<int32_t>(offsetof(InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984, ___updateBeforeRenderInternal_4)); }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  get_updateBeforeRenderInternal_4() const { return ___updateBeforeRenderInternal_4; }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * get_address_of_updateBeforeRenderInternal_4() { return &___updateBeforeRenderInternal_4; }
	inline void set_updateBeforeRenderInternal_4(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  value)
	{
		___updateBeforeRenderInternal_4 = value;
	}

	inline static int32_t get_offset_of_U3CisGenericTypeOfDeviceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984, ___U3CisGenericTypeOfDeviceU3Ek__BackingField_5)); }
	inline bool get_U3CisGenericTypeOfDeviceU3Ek__BackingField_5() const { return ___U3CisGenericTypeOfDeviceU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CisGenericTypeOfDeviceU3Ek__BackingField_5() { return &___U3CisGenericTypeOfDeviceU3Ek__BackingField_5; }
	inline void set_U3CisGenericTypeOfDeviceU3Ek__BackingField_5(bool value)
	{
		___U3CisGenericTypeOfDeviceU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CdisplayNameU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984, ___U3CdisplayNameU3Ek__BackingField_6)); }
	inline String_t* get_U3CdisplayNameU3Ek__BackingField_6() const { return ___U3CdisplayNameU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CdisplayNameU3Ek__BackingField_6() { return &___U3CdisplayNameU3Ek__BackingField_6; }
	inline void set_U3CdisplayNameU3Ek__BackingField_6(String_t* value)
	{
		___U3CdisplayNameU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdisplayNameU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984, ___U3CdescriptionU3Ek__BackingField_7)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_7() const { return ___U3CdescriptionU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_7() { return &___U3CdescriptionU3Ek__BackingField_7; }
	inline void set_U3CdescriptionU3Ek__BackingField_7(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdescriptionU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3ChideInUIU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984, ___U3ChideInUIU3Ek__BackingField_8)); }
	inline bool get_U3ChideInUIU3Ek__BackingField_8() const { return ___U3ChideInUIU3Ek__BackingField_8; }
	inline bool* get_address_of_U3ChideInUIU3Ek__BackingField_8() { return &___U3ChideInUIU3Ek__BackingField_8; }
	inline void set_U3ChideInUIU3Ek__BackingField_8(bool value)
	{
		___U3ChideInUIU3Ek__BackingField_8 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct  IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// Microsoft.MixedReality.OpenXR.NativeLibToken
struct  NativeLibToken_t0B4A194D064257C7BAA4ECF633C15DB8C8957023 
{
public:
	// System.UInt64 Microsoft.MixedReality.OpenXR.NativeLibToken::value__
	uint64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NativeLibToken_t0B4A194D064257C7BAA4ECF633C15DB8C8957023, ___value___2)); }
	inline uint64_t get_value___2() const { return ___value___2; }
	inline uint64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint64_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RuntimeInitializeLoadType
struct  RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5 
{
public:
	// System.Int32 UnityEngine.RuntimeInitializeLoadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct  TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct  AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// Microsoft.MixedReality.OpenXR.NativeLibTokenAttribute
struct  NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// Microsoft.MixedReality.OpenXR.NativeLibToken Microsoft.MixedReality.OpenXR.NativeLibTokenAttribute::<NativeLibToken>k__BackingField
	uint64_t ___U3CNativeLibTokenU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNativeLibTokenU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885, ___U3CNativeLibTokenU3Ek__BackingField_0)); }
	inline uint64_t get_U3CNativeLibTokenU3Ek__BackingField_0() const { return ___U3CNativeLibTokenU3Ek__BackingField_0; }
	inline uint64_t* get_address_of_U3CNativeLibTokenU3Ek__BackingField_0() { return &___U3CNativeLibTokenU3Ek__BackingField_0; }
	inline void set_U3CNativeLibTokenU3Ek__BackingField_0(uint64_t value)
	{
		___U3CNativeLibTokenU3Ek__BackingField_0 = value;
	}
};


// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct  RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D  : public PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948
{
public:
	// UnityEngine.RuntimeInitializeLoadType UnityEngine.RuntimeInitializeOnLoadMethodAttribute::m_LoadType
	int32_t ___m_LoadType_0;

public:
	inline static int32_t get_offset_of_m_LoadType_0() { return static_cast<int32_t>(offsetof(RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D, ___m_LoadType_0)); }
	inline int32_t get_m_LoadType_0() const { return ___m_LoadType_0; }
	inline int32_t* get_address_of_m_LoadType_0() { return &___m_LoadType_0; }
	inline void set_m_LoadType_0(int32_t value)
	{
		___m_LoadType_0 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9 (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * __this, String_t* ___assemblyName0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Scripting.PreserveAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2 (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * __this, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Layouts.InputControlLayoutAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputControlLayoutAttribute__ctor_m2C35D9C0A7CDEB6A840A215F33FF0FA9DDFD80BF (InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Layouts.InputControlLayoutAttribute::set_displayName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InputControlLayoutAttribute_set_displayName_m51759C33760B60FB12DD2D2EDC8C431EA0FD1E20_inline (InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Layouts.InputControlLayoutAttribute::set_commonUsages(System.String[])
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InputControlLayoutAttribute_set_commonUsages_mE744432198FBB37B38B16119433EB748EED7CCF3_inline (InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Layouts.InputControlAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Layouts.InputControlAttribute::set_aliases(System.String[])
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InputControlAttribute_set_aliases_mA2A0291BD4112A24F62155E0B58726AD28D5D1C5_inline (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Layouts.InputControlAttribute::set_offset(System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InputControlAttribute_set_offset_mD1C8106D674D63F7FB7F0A28FE066191DC9F3671_inline (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * __this, uint32_t ___value0, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.OpenXR.NativeLibTokenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeLibTokenAttribute__ctor_m50A60218A276DE4E6F1880378DF7B2DA51FB3CDF (NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885 * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.OpenXR.NativeLibTokenAttribute::set_NativeLibToken(Microsoft.MixedReality.OpenXR.NativeLibToken)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NativeLibTokenAttribute_set_NativeLibToken_mCF242DA8234A359AC667DFB290D6E3274ECE7F1C_inline (NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885 * __this, uint64_t ___value0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516 (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * __this, int32_t ___loadType0, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
static void Microsoft_MixedReality_OpenXR_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[1];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x2E\x4D\x69\x78\x65\x64\x52\x65\x61\x6C\x69\x74\x79\x2E\x4F\x70\x65\x6E\x58\x52\x2E\x45\x64\x69\x74\x6F\x72"), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[3];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[4];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void AnchorSubsystemExtensions_tB5A59F746D2CB4DA8FA9A1DA683AC180BF62BFA5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AnchorSubsystemExtensions_tB5A59F746D2CB4DA8FA9A1DA683AC180BF62BFA5_CustomAttributesCacheGenerator_AnchorSubsystemExtensions_LoadAnchorStoreAsync_m6FE5CF203AAA962E70D9097D2832D42D94297AD8(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x65\x78\x74\x65\x6E\x73\x69\x6F\x6E\x20\x66\x75\x6E\x63\x74\x69\x6F\x6E\x20\x4C\x6F\x61\x64\x41\x6E\x63\x68\x6F\x72\x53\x74\x6F\x72\x65\x41\x73\x79\x6E\x63\x20\x69\x6E\x20\x6E\x61\x6D\x65\x73\x70\x61\x63\x65\x20\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x2E\x4D\x69\x78\x65\x64\x52\x65\x61\x6C\x69\x74\x79\x2E\x41\x52\x53\x75\x62\x73\x79\x73\x74\x65\x6D\x73\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x20\x61\x6E\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6D\x6F\x76\x65\x64\x20\x69\x6E\x20\x66\x75\x74\x75\x72\x65\x20\x72\x65\x6C\x65\x61\x73\x65\x73\x2E\x20\x55\x73\x65\x20\x74\x68\x65\x20\x76\x65\x72\x73\x69\x6F\x6E\x20\x69\x6E\x20\x6E\x61\x6D\x65\x73\x70\x61\x63\x65\x20\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x2E\x4D\x69\x78\x65\x64\x52\x65\x61\x6C\x69\x74\x79\x2E\x4F\x70\x65\x6E\x58\x52\x2E\x41\x52\x53\x75\x62\x73\x79\x73\x74\x65\x6D\x73\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void XRAnchorStore_tC9CDBA7E2D11BA4B75CC29A81431B1550E37E79D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x74\x79\x70\x65\x20\x58\x52\x41\x6E\x63\x68\x6F\x72\x53\x74\x6F\x72\x65\x20\x69\x6E\x20\x6E\x61\x6D\x65\x73\x70\x61\x63\x65\x20\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x2E\x4D\x69\x78\x65\x64\x52\x65\x61\x6C\x69\x74\x79\x2E\x41\x52\x53\x75\x62\x73\x79\x73\x74\x65\x6D\x73\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x20\x61\x6E\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6D\x6F\x76\x65\x64\x20\x69\x6E\x20\x66\x75\x74\x75\x72\x65\x20\x72\x65\x6C\x65\x61\x73\x65\x73\x2E\x20\x55\x73\x65\x20\x74\x68\x65\x20\x76\x65\x72\x73\x69\x6F\x6E\x20\x69\x6E\x20\x6E\x61\x6D\x65\x73\x70\x61\x63\x65\x20\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x2E\x4D\x69\x78\x65\x64\x52\x65\x61\x6C\x69\x74\x79\x2E\x4F\x70\x65\x6E\x58\x52\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void XRAnchorStore_tC9CDBA7E2D11BA4B75CC29A81431B1550E37E79D_CustomAttributesCacheGenerator_XRAnchorStore_LoadAsync_mDA1D03EF31EBE83909DFB324028B9BD3E0E2EE08(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadAsyncU3Ed__0_tEA68C7403FA60BAFB05AD91AD233559E5B8D9117_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CLoadAsyncU3Ed__0_tEA68C7403FA60BAFB05AD91AD233559E5B8D9117_0_0_0_var), NULL);
	}
}
static void XRAnchorStore_tC9CDBA7E2D11BA4B75CC29A81431B1550E37E79D_CustomAttributesCacheGenerator_XRAnchorStore_TryPersistAnchor_mDFC7EF535C882DE15CF999113AE78671EB2DD5D2(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x54\x72\x79\x50\x65\x72\x73\x69\x73\x74\x41\x6E\x63\x68\x6F\x72\x28\x73\x74\x72\x69\x6E\x67\x2C\x20\x54\x72\x61\x63\x6B\x61\x62\x6C\x65\x49\x64\x29\x20\x66\x75\x6E\x63\x74\x69\x6F\x6E\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x20\x61\x6E\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6D\x6F\x76\x65\x64\x20\x69\x6E\x20\x66\x75\x74\x75\x72\x65\x20\x72\x65\x6C\x65\x61\x73\x65\x73\x2E\x20\x55\x73\x65\x20\x74\x68\x65\x20\x76\x65\x72\x73\x69\x6F\x6E\x20\x54\x72\x79\x50\x65\x72\x73\x69\x73\x74\x41\x6E\x63\x68\x6F\x72\x28\x54\x72\x61\x63\x6B\x61\x62\x6C\x65\x49\x64\x2C\x20\x73\x74\x72\x69\x6E\x67\x29\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void U3CLoadAsyncU3Ed__0_tEA68C7403FA60BAFB05AD91AD233559E5B8D9117_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadAsyncU3Ed__0_tEA68C7403FA60BAFB05AD91AD233559E5B8D9117_CustomAttributesCacheGenerator_U3CLoadAsyncU3Ed__0_SetStateMachine_mB84B24F4974916FB514D4D019E083C7773C8ADE3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void AnchorConverter_t5AB99180C92E8DF674B25F5E40E2388DE421B8EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void HandMeshTracker_tA479DEB39A92E212DE310D3587B281DC9D7B0146_CustomAttributesCacheGenerator_U3CLeftU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandMeshTracker_tA479DEB39A92E212DE310D3587B281DC9D7B0146_CustomAttributesCacheGenerator_U3CRightU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandMeshTracker_tA479DEB39A92E212DE310D3587B281DC9D7B0146_CustomAttributesCacheGenerator_HandMeshTracker_get_Left_m834FB22088CF46F9EF46DAC8731E6DF270106FAF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandMeshTracker_tA479DEB39A92E212DE310D3587B281DC9D7B0146_CustomAttributesCacheGenerator_HandMeshTracker_get_Right_m6705D8B2C8AB6513519C4B8D8738FE36DFC33731(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandTracker_t3327910B18D234F06B185BFA99047E30046D0957_CustomAttributesCacheGenerator_U3CLeftU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandTracker_t3327910B18D234F06B185BFA99047E30046D0957_CustomAttributesCacheGenerator_U3CRightU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandTracker_t3327910B18D234F06B185BFA99047E30046D0957_CustomAttributesCacheGenerator_HandTracker_get_Left_m708C38E9BC40B0FBEB07E9163C3148E8007A1A7F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandTracker_t3327910B18D234F06B185BFA99047E30046D0957_CustomAttributesCacheGenerator_HandTracker_get_Right_m65535255C8267CCABFFDD58A6BE59F4DBCCAB159(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandJointLocation_t6417344072BEE803CA511D7582144CE369974CC2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void HandJointLocation_t6417344072BEE803CA511D7582144CE369974CC2_CustomAttributesCacheGenerator_U3CPoseU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandJointLocation_t6417344072BEE803CA511D7582144CE369974CC2_CustomAttributesCacheGenerator_U3CRadiusU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandJointLocation_t6417344072BEE803CA511D7582144CE369974CC2_CustomAttributesCacheGenerator_HandJointLocation_get_Pose_mEC69083D5F01D41D2737848B69B570C88771B652(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandJointLocation_t6417344072BEE803CA511D7582144CE369974CC2_CustomAttributesCacheGenerator_HandJointLocation_get_Radius_m59C2EE4AC8714AF1583CD70EA7CD5A68847C726F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpatialGraphNode_tB39D8AA29F3B1CD7895B4A415466DD283651D722_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void SpatialGraphNode_tB39D8AA29F3B1CD7895B4A415466DD283651D722_CustomAttributesCacheGenerator_U3CIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpatialGraphNode_tB39D8AA29F3B1CD7895B4A415466DD283651D722_CustomAttributesCacheGenerator_SpatialGraphNode_get_Id_m96236885EF759340AC6EE05414F163834A7268CD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpatialGraphNode_tB39D8AA29F3B1CD7895B4A415466DD283651D722_CustomAttributesCacheGenerator_SpatialGraphNode_set_Id_mF0E64EED4BA607D324C97A2D08183EC05AE760EC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void XRAnchorStore_t188192CAF2D549FA0523A675925575B745066B5E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void XRAnchorStore_t188192CAF2D549FA0523A675925575B745066B5E_CustomAttributesCacheGenerator_XRAnchorStore_LoadAsync_m822582D492817B68C688B330A1826EFC96EDA38D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadAsyncU3Ed__6_tA92A8FCF33BC6C780B51AD9E5F2F7262A63AA788_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CLoadAsyncU3Ed__6_tA92A8FCF33BC6C780B51AD9E5F2F7262A63AA788_0_0_0_var), NULL);
	}
}
static void U3CLoadAsyncU3Ed__6_tA92A8FCF33BC6C780B51AD9E5F2F7262A63AA788_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadAsyncU3Ed__6_tA92A8FCF33BC6C780B51AD9E5F2F7262A63AA788_CustomAttributesCacheGenerator_U3CLoadAsyncU3Ed__6_SetStateMachine_m3DB639B56B9ADE4D2CF850C4968E94B21C254D42(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_commonUsages = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 2);
		(_tmp_commonUsages)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x4C\x65\x66\x74\x48\x61\x6E\x64"));
		(_tmp_commonUsages)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x52\x69\x67\x68\x74\x48\x61\x6E\x64"));
		InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984 * tmp = (InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984 *)cache->attributes[0];
		InputControlLayoutAttribute__ctor_m2C35D9C0A7CDEB6A840A215F33FF0FA9DDFD80BF(tmp, NULL);
		InputControlLayoutAttribute_set_displayName_m51759C33760B60FB12DD2D2EDC8C431EA0FD1E20_inline(tmp, il2cpp_codegen_string_new_wrapper("\x48\x50\x20\x52\x65\x76\x65\x72\x62\x20\x47\x32\x20\x43\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72\x20\x4F\x70\x65\x6E\x58\x52"), NULL);
		InputControlLayoutAttribute_set_commonUsages_mE744432198FBB37B38B16119433EB748EED7CCF3_inline(tmp, _tmp_commonUsages, NULL);
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[1];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CthumbstickU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CgripU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CgripPressedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CmenuU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CprimaryButtonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CsecondaryButtonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CtriggerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CtriggerPressedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CthumbstickClickedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CdevicePoseU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CpointerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CisTrackedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CtrackingStateU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CdevicePositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CdeviceRotationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CpointerPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CpointerRotationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_thumbstick_m90CA4687E93563B0DEB602449DAE1FFFC911F62C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_thumbstick_mB63719233F55DCED55B0DEFE3F5D893D8D7B989C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_grip_mCE781A33AAF069E177C6EF5100164BD66236C810(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_grip_m80A88B6E2D47AD5CC2722B7E10B4185FA96696DC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_gripPressed_mEA1F4C574F8BB5A1D1A44FA98FD43AB5F52EE1B4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_gripPressed_m862349937A453211DF89235CDE719DFDC01DDC0E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_menu_mC0A16B388913E40C8EED70AE5F8CE640957F706A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_menu_mF0C460935A0A1C28FE859EF75BB5FC3E2140F691(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_primaryButton_m378BDF8328E8A6DE3B0A33C985A464949669D8B8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_primaryButton_mEA85F1585A82C77FA5693E33D0F7996783BCEA44(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_secondaryButton_m426DCE2368DD70DDFC5B3D0F67F9D6A70D0525DD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_secondaryButton_m70E3935734AC36C792D1A54AFFDCF1C429E0353C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_trigger_mF66176140D4592DE7122078E6242464EC87F63C8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_trigger_m5E824031767A7B4E03A4DDD6A73C590D414A3229(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_triggerPressed_m8ED9173BF9C6028DD0970D25D15AC87DEAF7DA45(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_triggerPressed_m743356C12B8EFD746028482D7E57F5513914D3CE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_thumbstickClicked_m528DA9346A052CB8CF6D6BFBDE6AD670CC3A4846(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_thumbstickClicked_mF3DC1E0A64B894C96E88ACEDC198EB1769D8B9F1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_devicePose_m4CA8FBCD335A15BFC9C1D2DFAC011CCB7FF6D9C7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_devicePose_mC0EAE470D05077DF7492B88D1A0549458217EA59(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_pointer_m3FBCC9B7054E2FE2C0B78C8EB6B7D68330D42BC0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_pointer_mD3E0295F051BB52FD90B7C704C1EF3D509F78466(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_isTracked_m6DEA5B5F2EECC0129E12B53ECF10812A570125FC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_isTracked_mF02911DC0F1C1E2D6C8EDF61E4D7DEAE9CF85B5C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_trackingState_mAF48A26032D476F52E4538464FA9183F5CB8E1BC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_trackingState_m6387B052D3C16EDB339858D9E5B38EA1028975CA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_devicePosition_m684527E5770BAAEE45AFED473D4DACE9F5061B96(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_devicePosition_m448F72744C9775C8F30D2865077F87A5A7E80011(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_deviceRotation_mDDDAD598D77716DB2322BFB0E9E7FC86A19FCFF9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_deviceRotation_mF9F5F1DEBF0B7D67F3B41E68E9288AE1A2349160(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_pointerPosition_m494846ED88B2D8FE279793989489381FDD3F2060(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_pointerPosition_m9E7EEC6DF082911E9BA713EB47E901118FF1EE52(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_pointerRotation_m9B28C8E1D41987CFBC8AA2594EC7BD3BA47566C7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_pointerRotation_m96F55CDCC866F08C06204FE1445AC66E24D79CF1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____thumbstick_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_aliases = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 2);
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x50\x72\x69\x6D\x61\x72\x79\x32\x44\x41\x78\x69\x73"));
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x4A\x6F\x79\x73\x74\x69\x63\x6B"));
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[1];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
		InputControlAttribute_set_aliases_mA2A0291BD4112A24F62155E0B58726AD28D5D1C5_inline(tmp, _tmp_aliases, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____grip_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_aliases = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 2);
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x47\x72\x69\x70\x41\x78\x69\x73"));
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x73\x71\x75\x65\x65\x7A\x65"));
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[1];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
		InputControlAttribute_set_aliases_mA2A0291BD4112A24F62155E0B58726AD28D5D1C5_inline(tmp, _tmp_aliases, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____gripPressed_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_aliases = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 2);
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x47\x72\x69\x70\x42\x75\x74\x74\x6F\x6E"));
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x73\x71\x75\x65\x65\x7A\x65\x43\x6C\x69\x63\x6B\x65\x64"));
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[1];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
		InputControlAttribute_set_aliases_mA2A0291BD4112A24F62155E0B58726AD28D5D1C5_inline(tmp, _tmp_aliases, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____menu_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_aliases = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 1);
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x6D\x65\x6E\x75\x42\x75\x74\x74\x6F\x6E"));
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[1];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
		InputControlAttribute_set_aliases_mA2A0291BD4112A24F62155E0B58726AD28D5D1C5_inline(tmp, _tmp_aliases, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____primaryButton_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_aliases = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 4);
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x41"));
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x58"));
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), il2cpp_codegen_string_new_wrapper("\x62\x75\x74\x74\x6F\x6E\x41"));
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), il2cpp_codegen_string_new_wrapper("\x62\x75\x74\x74\x6F\x6E\x58"));
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[0];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
		InputControlAttribute_set_aliases_mA2A0291BD4112A24F62155E0B58726AD28D5D1C5_inline(tmp, _tmp_aliases, NULL);
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[1];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____secondaryButton_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_aliases = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 4);
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x42"));
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x59"));
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), il2cpp_codegen_string_new_wrapper("\x62\x75\x74\x74\x6F\x6E\x42"));
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), il2cpp_codegen_string_new_wrapper("\x62\x75\x74\x74\x6F\x6E\x59"));
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[0];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
		InputControlAttribute_set_aliases_mA2A0291BD4112A24F62155E0B58726AD28D5D1C5_inline(tmp, _tmp_aliases, NULL);
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[1];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____trigger_PropertyInfo(CustomAttributesCache* cache)
{
	{
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[0];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[1];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____triggerPressed_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_aliases = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 3);
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x69\x6E\x64\x65\x78\x42\x75\x74\x74\x6F\x6E"));
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x69\x6E\x64\x65\x78\x54\x6F\x75\x63\x68\x65\x64"));
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), il2cpp_codegen_string_new_wrapper("\x74\x72\x69\x67\x67\x65\x72\x62\x75\x74\x74\x6F\x6E"));
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[1];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
		InputControlAttribute_set_aliases_mA2A0291BD4112A24F62155E0B58726AD28D5D1C5_inline(tmp, _tmp_aliases, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____thumbstickClicked_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_aliases = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 1);
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x6A\x6F\x79\x73\x74\x69\x63\x6B\x4F\x72\x50\x61\x64\x50\x72\x65\x73\x73\x65\x64"));
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[0];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
		InputControlAttribute_set_aliases_mA2A0291BD4112A24F62155E0B58726AD28D5D1C5_inline(tmp, _tmp_aliases, NULL);
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[1];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____devicePose_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_aliases = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 2);
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x64\x65\x76\x69\x63\x65"));
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x67\x72\x69\x70\x50\x6F\x73\x65"));
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[0];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
		InputControlAttribute_set_offset_mD1C8106D674D63F7FB7F0A28FE066191DC9F3671_inline(tmp, 0LL, NULL);
		InputControlAttribute_set_aliases_mA2A0291BD4112A24F62155E0B58726AD28D5D1C5_inline(tmp, _tmp_aliases, NULL);
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[1];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____pointer_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_aliases = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 1);
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x61\x69\x6D\x50\x6F\x73\x65"));
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[0];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
		InputControlAttribute_set_offset_mD1C8106D674D63F7FB7F0A28FE066191DC9F3671_inline(tmp, 0LL, NULL);
		InputControlAttribute_set_aliases_mA2A0291BD4112A24F62155E0B58726AD28D5D1C5_inline(tmp, _tmp_aliases, NULL);
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[1];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____isTracked_PropertyInfo(CustomAttributesCache* cache)
{
	{
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[0];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
		InputControlAttribute_set_offset_mD1C8106D674D63F7FB7F0A28FE066191DC9F3671_inline(tmp, 26LL, NULL);
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[1];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____trackingState_PropertyInfo(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
	{
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[1];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
		InputControlAttribute_set_offset_mD1C8106D674D63F7FB7F0A28FE066191DC9F3671_inline(tmp, 28LL, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____devicePosition_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_aliases = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 1);
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x67\x72\x69\x70\x50\x6F\x73\x69\x74\x69\x6F\x6E"));
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[0];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
		InputControlAttribute_set_offset_mD1C8106D674D63F7FB7F0A28FE066191DC9F3671_inline(tmp, 32LL, NULL);
		InputControlAttribute_set_aliases_mA2A0291BD4112A24F62155E0B58726AD28D5D1C5_inline(tmp, _tmp_aliases, NULL);
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[1];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____deviceRotation_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_aliases = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 3);
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x64\x65\x76\x69\x63\x65\x4F\x72\x69\x65\x6E\x74\x61\x74\x69\x6F\x6E"));
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x67\x72\x69\x70\x52\x6F\x74\x61\x74\x69\x6F\x6E"));
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), il2cpp_codegen_string_new_wrapper("\x67\x72\x69\x70\x4F\x72\x69\x65\x6E\x74\x61\x74\x69\x6F\x6E"));
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[0];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
		InputControlAttribute_set_offset_mD1C8106D674D63F7FB7F0A28FE066191DC9F3671_inline(tmp, 44LL, NULL);
		InputControlAttribute_set_aliases_mA2A0291BD4112A24F62155E0B58726AD28D5D1C5_inline(tmp, _tmp_aliases, NULL);
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[1];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____pointerPosition_PropertyInfo(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
	{
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[1];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
		InputControlAttribute_set_offset_mD1C8106D674D63F7FB7F0A28FE066191DC9F3671_inline(tmp, 92LL, NULL);
	}
}
static void HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____pointerRotation_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_aliases = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 1);
		(_tmp_aliases)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x70\x6F\x69\x6E\x74\x65\x72\x4F\x72\x69\x65\x6E\x74\x61\x74\x69\x6F\x6E"));
		InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * tmp = (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 *)cache->attributes[1];
		InputControlAttribute__ctor_mD95991F783A7335D8C33A2F159B878F49310D4DD(tmp, NULL);
		InputControlAttribute_set_offset_mD1C8106D674D63F7FB7F0A28FE066191DC9F3671_inline(tmp, 104LL, NULL);
		InputControlAttribute_set_aliases_mA2A0291BD4112A24F62155E0B58726AD28D5D1C5_inline(tmp, _tmp_aliases, NULL);
	}
}
static void HandTrackingFeaturePlugin_tBD66BC307E7AFEE63C94C33939131C967917FA28_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885 * tmp = (NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885 *)cache->attributes[0];
		NativeLibTokenAttribute__ctor_m50A60218A276DE4E6F1880378DF7B2DA51FB3CDF(tmp, NULL);
		NativeLibTokenAttribute_set_NativeLibToken_mCF242DA8234A359AC667DFB290D6E3274ECE7F1C_inline(tmp, 2LL, NULL);
	}
}
static void HoloLensFeaturePlugin_t7DBD30C239739EAFFB7711BDB562985907897606_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885 * tmp = (NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885 *)cache->attributes[0];
		NativeLibTokenAttribute__ctor_m50A60218A276DE4E6F1880378DF7B2DA51FB3CDF(tmp, NULL);
		NativeLibTokenAttribute_set_NativeLibToken_mCF242DA8234A359AC667DFB290D6E3274ECE7F1C_inline(tmp, 1LL, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_U3CSystemIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_U3CSessionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_U3CSessionStateU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_U3CSceneOriginSpaceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_U3CIsAnchorExtensionSupportedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_get_Instance_m856697DBBE9ACDDA9DBEC30AC982DE83C54C3C08(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_set_Instance_mF9A1F53CF1611583D0CFB6BF2585D7001540474D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_get_SystemId_m459184F68B27882DF83BAAE4B5F48866FF19C5FA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_set_SystemId_m6A54A5F9A42FCB76E36E09DD9A4B1475E7D65BD7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_get_Session_mFB429C810F15FD1E263D7442745B2560BB433361(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_set_Session_m536506957954E3925F96061AF725043CE677D24D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_get_SessionState_mAF01E3EEFA8CA3502F36C58C97F8590ABC9E74A2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_set_SessionState_m96318C37B8405D34B5118030898FF20FB6261AB4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_get_SceneOriginSpace_m8528C0EF816F36F79EA22BE8E928BAC4919DDC7D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_set_SceneOriginSpace_m62F6FC53CD5F94235500EFF2625374E9BB71C05F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_get_IsAnchorExtensionSupported_m39F9A85A9F308065B77145022128FA85FDB87F0F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_set_IsAnchorExtensionSupported_m29B7C2766A4873A5D88F0DE6D9FA6AE6EA5414AF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_U3COnSubsystemCreateU3Eb__31_0_m7BFABC06D135282C6CF695AF56E1AF62E2A54785(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_U3COnSubsystemStartU3Eb__32_0_mA2935B0BBD5139B350D952458134045FFBFB4998(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_U3COnSubsystemStopU3Eb__33_0_m897469C0A358E68AB582CCB8C3485D37C7A0DDEC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_U3COnSubsystemDestroyU3Eb__34_0_mB01CE4556E7326990F5D5B404861A85F03F38EE1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 4LL, NULL);
	}
}
static void NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885_CustomAttributesCacheGenerator_U3CNativeLibTokenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885_CustomAttributesCacheGenerator_NativeLibTokenAttribute_get_NativeLibToken_mC21339A02A50AAF0A09469B71CE8F598281B18DF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885_CustomAttributesCacheGenerator_NativeLibTokenAttribute_set_NativeLibToken_mCF242DA8234A359AC667DFB290D6E3274ECE7F1C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t2A1CC1E659F53E9D2E719F31E3215BF1A5F3911F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AnchorSubsystem_t2B0BBDF9E87DC47D84CCE15AADB38E986788A81A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void AnchorSubsystem_t2B0BBDF9E87DC47D84CCE15AADB38E986788A81A_CustomAttributesCacheGenerator_AnchorSubsystem_RegisterDescriptor_mD1EA19E524C831940D55D6E869398D7467743A5F(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 4LL, NULL);
	}
}
static void PlaneSubsystem_t205C08800A7B7F42AFBB6919380A6638C0E6D43B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void PlaneSubsystem_t205C08800A7B7F42AFBB6919380A6638C0E6D43B_CustomAttributesCacheGenerator_PlaneSubsystem_RegisterDescriptor_mC49177E583BFA41281C5F20A9A3C3EB7E5929114(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 4LL, NULL);
	}
}
static void RaycastSubsystem_t12CBE986BE4482DF95595A906E5260CCD67D0516_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void RaycastSubsystem_t12CBE986BE4482DF95595A906E5260CCD67D0516_CustomAttributesCacheGenerator_RaycastSubsystem_RegisterDescriptor_mDFFDCFFE9613F761ABF930299B6E11011BDEDBF9(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 4LL, NULL);
	}
}
static void HandPoseType_t2C3C5F7D293F34DE79339A54972BD31FCC8085CB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x74\x79\x70\x65\x20\x50\x72\x65\x76\x69\x65\x77\x2E\x48\x61\x6E\x64\x50\x6F\x73\x65\x54\x79\x70\x65\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x20\x61\x6E\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6D\x6F\x76\x65\x64\x20\x69\x6E\x20\x66\x75\x74\x75\x72\x65\x20\x72\x65\x6C\x65\x61\x73\x65\x73\x2E\x20\x55\x73\x65\x20\x74\x68\x65\x20\x6E\x6F\x6E\x2D\x50\x72\x65\x76\x69\x65\x77\x20\x6E\x61\x6D\x65\x73\x70\x61\x63\x65\x20\x74\x79\x70\x65\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void HandTracker_t90D954B16904CEE895918391ACF55F69A5F036F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x74\x79\x70\x65\x20\x50\x72\x65\x76\x69\x65\x77\x2E\x48\x61\x6E\x64\x54\x72\x61\x63\x6B\x65\x72\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x20\x61\x6E\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6D\x6F\x76\x65\x64\x20\x69\x6E\x20\x66\x75\x74\x75\x72\x65\x20\x72\x65\x6C\x65\x61\x73\x65\x73\x2E\x20\x55\x73\x65\x20\x74\x68\x65\x20\x6E\x6F\x6E\x2D\x50\x72\x65\x76\x69\x65\x77\x20\x6E\x61\x6D\x65\x73\x70\x61\x63\x65\x20\x74\x79\x70\x65\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void HandTracker_t90D954B16904CEE895918391ACF55F69A5F036F4_CustomAttributesCacheGenerator_HandTracker_TryLocateHandJoints_m1043ECB88351DA411C4CAA43BA9C336D98BC3DEC(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x74\x79\x70\x65\x20\x50\x72\x65\x76\x69\x65\x77\x2E\x48\x61\x6E\x64\x4A\x6F\x69\x6E\x74\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x20\x61\x6E\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6D\x6F\x76\x65\x64\x20\x69\x6E\x20\x66\x75\x74\x75\x72\x65\x20\x72\x65\x6C\x65\x61\x73\x65\x73\x2E\x20\x55\x73\x65\x20\x74\x68\x65\x20\x6E\x6F\x6E\x2D\x50\x72\x65\x76\x69\x65\x77\x20\x6E\x61\x6D\x65\x73\x70\x61\x63\x65\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void Handedness_t48A5B6258FF57DF499793422B1876B35A5921FB6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x74\x79\x70\x65\x20\x50\x72\x65\x76\x69\x65\x77\x2E\x48\x61\x6E\x64\x65\x64\x6E\x65\x73\x73\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x20\x61\x6E\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6D\x6F\x76\x65\x64\x20\x69\x6E\x20\x66\x75\x74\x75\x72\x65\x20\x72\x65\x6C\x65\x61\x73\x65\x73\x2E\x20\x55\x73\x65\x20\x74\x68\x65\x20\x6E\x6F\x6E\x2D\x50\x72\x65\x76\x69\x65\x77\x20\x6E\x61\x6D\x65\x73\x70\x61\x63\x65\x20\x74\x79\x70\x65\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void HandJoint_tEB7D2AA38CFCA88388F40D29FF881463C735D981_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x74\x79\x70\x65\x20\x50\x72\x65\x76\x69\x65\x77\x2E\x48\x61\x6E\x64\x4A\x6F\x69\x6E\x74\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x20\x61\x6E\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6D\x6F\x76\x65\x64\x20\x69\x6E\x20\x66\x75\x74\x75\x72\x65\x20\x72\x65\x6C\x65\x61\x73\x65\x73\x2E\x20\x55\x73\x65\x20\x74\x68\x65\x20\x6E\x6F\x6E\x2D\x50\x72\x65\x76\x69\x65\x77\x20\x6E\x61\x6D\x65\x73\x70\x61\x63\x65\x20\x74\x79\x70\x65\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x74\x79\x70\x65\x20\x50\x72\x65\x76\x69\x65\x77\x2E\x48\x61\x6E\x64\x4A\x6F\x69\x6E\x74\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x20\x61\x6E\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6D\x6F\x76\x65\x64\x20\x69\x6E\x20\x66\x75\x74\x75\x72\x65\x20\x72\x65\x6C\x65\x61\x73\x65\x73\x2E\x20\x55\x73\x65\x20\x74\x68\x65\x20\x6E\x6F\x6E\x2D\x50\x72\x65\x76\x69\x65\x77\x20\x6E\x61\x6D\x65\x73\x70\x61\x63\x65\x20\x74\x79\x70\x65\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[1];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator_U3CPoseFlagsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator_U3CRotationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator_U3CPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator_U3CRadiusU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator_HandJointLocation_get_PoseFlags_m1CD68F1D99A888442B52E06B2464692300714423(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator_HandJointLocation_get_Rotation_mE08792490B53A7951D778D5C2F9F7F7B89494E3D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator_HandJointLocation_get_Position_mD2FACF4383DD4FC9EE45505113C56C09C6636042(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator_HandJointLocation_get_Radius_mDCE67E74FD5000DD8235D62B0A014B661F42BF83(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PoseFlags_t3A2A4FF35F44B9E1D00B1773A02A4B0BB728D3E3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[1];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x74\x79\x70\x65\x20\x50\x72\x65\x76\x69\x65\x77\x2E\x50\x6F\x73\x65\x46\x6C\x61\x67\x73\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x20\x61\x6E\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6D\x6F\x76\x65\x64\x20\x69\x6E\x20\x66\x75\x74\x75\x72\x65\x20\x72\x65\x6C\x65\x61\x73\x65\x73\x2E"), NULL);
	}
}
static void AppRemoting_tD5F0CEC33C78A09D9DDF0ECFC8CCDAD2F953D5B1_CustomAttributesCacheGenerator_U3CConfigurationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppRemoting_tD5F0CEC33C78A09D9DDF0ECFC8CCDAD2F953D5B1_CustomAttributesCacheGenerator_AppRemoting_get_Configuration_m518C2AFDA11E111274D91A3877515F2EE21B93A3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppRemoting_tD5F0CEC33C78A09D9DDF0ECFC8CCDAD2F953D5B1_CustomAttributesCacheGenerator_AppRemoting_set_Configuration_mA37F1AC0EE17FCA0DFD43484FA0608E0D203DFD1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppRemoting_tD5F0CEC33C78A09D9DDF0ECFC8CCDAD2F953D5B1_CustomAttributesCacheGenerator_AppRemoting_Connect_mFDF9ACF483419C1C1D1072B7B4E60AC4D714A3CF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CConnectU3Ed__4_t99E03D1D6720EB9A51A3076B4D7D7087A794913D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CConnectU3Ed__4_t99E03D1D6720EB9A51A3076B4D7D7087A794913D_0_0_0_var), NULL);
	}
}
static void U3CConnectU3Ed__4_t99E03D1D6720EB9A51A3076B4D7D7087A794913D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConnectU3Ed__4_t99E03D1D6720EB9A51A3076B4D7D7087A794913D_CustomAttributesCacheGenerator_U3CConnectU3Ed__4__ctor_m9B7169E748BDE0D567455598D91A91A7D2D60CA3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConnectU3Ed__4_t99E03D1D6720EB9A51A3076B4D7D7087A794913D_CustomAttributesCacheGenerator_U3CConnectU3Ed__4_System_IDisposable_Dispose_mF2F8F709F0C863F20922D10016EF3769BF9CABA7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConnectU3Ed__4_t99E03D1D6720EB9A51A3076B4D7D7087A794913D_CustomAttributesCacheGenerator_U3CConnectU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAEAED26889E7742A923273906179CB6DB875EF7D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConnectU3Ed__4_t99E03D1D6720EB9A51A3076B4D7D7087A794913D_CustomAttributesCacheGenerator_U3CConnectU3Ed__4_System_Collections_IEnumerator_Reset_mCAB5E7A2915C5628DF53ABD73DD21D5536C5A493(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConnectU3Ed__4_t99E03D1D6720EB9A51A3076B4D7D7087A794913D_CustomAttributesCacheGenerator_U3CConnectU3Ed__4_System_Collections_IEnumerator_get_Current_m5FC03290D91E6AC38AFA1D30CD249EB00999BAAC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void AppRemotingPlugin_t4EA8D9F04222E8CBEC1BD1D4019C8BC9EA882F43_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885 * tmp = (NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885 *)cache->attributes[0];
		NativeLibTokenAttribute__ctor_m50A60218A276DE4E6F1880378DF7B2DA51FB3CDF(tmp, NULL);
		NativeLibTokenAttribute_set_NativeLibToken_mCF242DA8234A359AC667DFB290D6E3274ECE7F1C_inline(tmp, 3LL, NULL);
	}
}
static void U3CU3Ec_tE05F461AB62BC4F2CE34CC9B60D2D61411FCCDCE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditorRemotingPlugin_t734E968BC26B3B3B989BAC5CA5817475ADD6A213_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885 * tmp = (NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885 *)cache->attributes[0];
		NativeLibTokenAttribute__ctor_m50A60218A276DE4E6F1880378DF7B2DA51FB3CDF(tmp, NULL);
		NativeLibTokenAttribute_set_NativeLibToken_mCF242DA8234A359AC667DFB290D6E3274ECE7F1C_inline(tmp, 3LL, NULL);
	}
}
static void EditorRemotingPlugin_t734E968BC26B3B3B989BAC5CA5817475ADD6A213_CustomAttributesCacheGenerator_m_remoteHostName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x68\x6F\x73\x74\x20\x6E\x61\x6D\x65\x20\x6F\x72\x20\x49\x50\x20\x61\x64\x64\x72\x65\x73\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x72\x75\x6E\x6E\x69\x6E\x67\x20\x69\x6E\x20\x6E\x65\x74\x77\x6F\x72\x6B\x20\x73\x65\x72\x76\x65\x72\x20\x6D\x6F\x64\x65\x20\x74\x6F\x20\x63\x6F\x6E\x6E\x65\x63\x74\x20\x74\x6F\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EditorRemotingPlugin_t734E968BC26B3B3B989BAC5CA5817475ADD6A213_CustomAttributesCacheGenerator_m_remoteHostPort(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x70\x6F\x72\x74\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x65\x72\x76\x65\x72\x27\x73\x20\x68\x61\x6E\x64\x73\x68\x61\x6B\x65\x20\x70\x6F\x72\x74\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EditorRemotingPlugin_t734E968BC26B3B3B989BAC5CA5817475ADD6A213_CustomAttributesCacheGenerator_m_maxBitrate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6D\x61\x78\x20\x62\x69\x74\x72\x61\x74\x65\x20\x69\x6E\x20\x4B\x62\x70\x73\x20\x74\x6F\x20\x75\x73\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x6F\x6E\x6E\x65\x63\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void EditorRemotingPlugin_t734E968BC26B3B3B989BAC5CA5817475ADD6A213_CustomAttributesCacheGenerator_m_videoCodec(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x76\x69\x64\x65\x6F\x20\x63\x6F\x64\x65\x63\x20\x74\x6F\x20\x75\x73\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x6F\x6E\x6E\x65\x63\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void EditorRemotingPlugin_t734E968BC26B3B3B989BAC5CA5817475ADD6A213_CustomAttributesCacheGenerator_m_enableAudio(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x2F\x64\x69\x73\x61\x62\x6C\x65\x20\x61\x75\x64\x69\x6F\x20\x72\x65\x6D\x6F\x74\x69\x6E\x67\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EditorRemotingPlugin_t734E968BC26B3B3B989BAC5CA5817475ADD6A213_CustomAttributesCacheGenerator_EditorRemotingPlugin_U3COnSystemChangeU3Eb__13_0_m2AD00416A38236601D9DC419598D621ABF7F64A7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARAnchorExtensions_tCD94D89AF20632073D34D7FA9A35D07E2C57B144_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[1];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void ARAnchorExtensions_tCD94D89AF20632073D34D7FA9A35D07E2C57B144_CustomAttributesCacheGenerator_ARAnchorExtensions_GetOpenXRHandle_m07C39102695713B89B09A0AC5930F96FE51A7A80(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AnchorManagerExtensions_t8C5C59841E7A464C3D0115EC2319484A56B2C386_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[1];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void AnchorManagerExtensions_t8C5C59841E7A464C3D0115EC2319484A56B2C386_CustomAttributesCacheGenerator_AnchorManagerExtensions_LoadAnchorStoreAsync_m7FFB9D7FFB415A46DDB8822EE8FE81A220056F7B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void XRAnchorExtensions_t3810800404C0B971594FD62A513FF6EB59DE82F3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void XRAnchorExtensions_t3810800404C0B971594FD62A513FF6EB59DE82F3_CustomAttributesCacheGenerator_XRAnchorExtensions_GetOpenXRHandle_m3AB1B804A8570EA669D000A64CAC3B0BD1C08B94(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AnchorSubsystemExtensions_tD4092A164D3A2E5FFF1D1E24295042D0E8247CF8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[1];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void AnchorSubsystemExtensions_tD4092A164D3A2E5FFF1D1E24295042D0E8247CF8_CustomAttributesCacheGenerator_AnchorSubsystemExtensions_LoadAnchorStoreAsync_m04BE110C74F6AEA0055C448C3C74C02EBACA231D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Microsoft_MixedReality_OpenXR_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Microsoft_MixedReality_OpenXR_AttributeGenerators[176] = 
{
	AnchorSubsystemExtensions_tB5A59F746D2CB4DA8FA9A1DA683AC180BF62BFA5_CustomAttributesCacheGenerator,
	XRAnchorStore_tC9CDBA7E2D11BA4B75CC29A81431B1550E37E79D_CustomAttributesCacheGenerator,
	U3CLoadAsyncU3Ed__0_tEA68C7403FA60BAFB05AD91AD233559E5B8D9117_CustomAttributesCacheGenerator,
	AnchorConverter_t5AB99180C92E8DF674B25F5E40E2388DE421B8EE_CustomAttributesCacheGenerator,
	HandJointLocation_t6417344072BEE803CA511D7582144CE369974CC2_CustomAttributesCacheGenerator,
	SpatialGraphNode_tB39D8AA29F3B1CD7895B4A415466DD283651D722_CustomAttributesCacheGenerator,
	XRAnchorStore_t188192CAF2D549FA0523A675925575B745066B5E_CustomAttributesCacheGenerator,
	U3CLoadAsyncU3Ed__6_tA92A8FCF33BC6C780B51AD9E5F2F7262A63AA788_CustomAttributesCacheGenerator,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator,
	HandTrackingFeaturePlugin_tBD66BC307E7AFEE63C94C33939131C967917FA28_CustomAttributesCacheGenerator,
	HoloLensFeaturePlugin_t7DBD30C239739EAFFB7711BDB562985907897606_CustomAttributesCacheGenerator,
	NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885_CustomAttributesCacheGenerator,
	U3CU3Ec_t2A1CC1E659F53E9D2E719F31E3215BF1A5F3911F_CustomAttributesCacheGenerator,
	AnchorSubsystem_t2B0BBDF9E87DC47D84CCE15AADB38E986788A81A_CustomAttributesCacheGenerator,
	PlaneSubsystem_t205C08800A7B7F42AFBB6919380A6638C0E6D43B_CustomAttributesCacheGenerator,
	RaycastSubsystem_t12CBE986BE4482DF95595A906E5260CCD67D0516_CustomAttributesCacheGenerator,
	HandPoseType_t2C3C5F7D293F34DE79339A54972BD31FCC8085CB_CustomAttributesCacheGenerator,
	HandTracker_t90D954B16904CEE895918391ACF55F69A5F036F4_CustomAttributesCacheGenerator,
	Handedness_t48A5B6258FF57DF499793422B1876B35A5921FB6_CustomAttributesCacheGenerator,
	HandJoint_tEB7D2AA38CFCA88388F40D29FF881463C735D981_CustomAttributesCacheGenerator,
	HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator,
	PoseFlags_t3A2A4FF35F44B9E1D00B1773A02A4B0BB728D3E3_CustomAttributesCacheGenerator,
	U3CConnectU3Ed__4_t99E03D1D6720EB9A51A3076B4D7D7087A794913D_CustomAttributesCacheGenerator,
	AppRemotingPlugin_t4EA8D9F04222E8CBEC1BD1D4019C8BC9EA882F43_CustomAttributesCacheGenerator,
	U3CU3Ec_tE05F461AB62BC4F2CE34CC9B60D2D61411FCCDCE_CustomAttributesCacheGenerator,
	EditorRemotingPlugin_t734E968BC26B3B3B989BAC5CA5817475ADD6A213_CustomAttributesCacheGenerator,
	ARAnchorExtensions_tCD94D89AF20632073D34D7FA9A35D07E2C57B144_CustomAttributesCacheGenerator,
	AnchorManagerExtensions_t8C5C59841E7A464C3D0115EC2319484A56B2C386_CustomAttributesCacheGenerator,
	XRAnchorExtensions_t3810800404C0B971594FD62A513FF6EB59DE82F3_CustomAttributesCacheGenerator,
	AnchorSubsystemExtensions_tD4092A164D3A2E5FFF1D1E24295042D0E8247CF8_CustomAttributesCacheGenerator,
	HandMeshTracker_tA479DEB39A92E212DE310D3587B281DC9D7B0146_CustomAttributesCacheGenerator_U3CLeftU3Ek__BackingField,
	HandMeshTracker_tA479DEB39A92E212DE310D3587B281DC9D7B0146_CustomAttributesCacheGenerator_U3CRightU3Ek__BackingField,
	HandTracker_t3327910B18D234F06B185BFA99047E30046D0957_CustomAttributesCacheGenerator_U3CLeftU3Ek__BackingField,
	HandTracker_t3327910B18D234F06B185BFA99047E30046D0957_CustomAttributesCacheGenerator_U3CRightU3Ek__BackingField,
	HandJointLocation_t6417344072BEE803CA511D7582144CE369974CC2_CustomAttributesCacheGenerator_U3CPoseU3Ek__BackingField,
	HandJointLocation_t6417344072BEE803CA511D7582144CE369974CC2_CustomAttributesCacheGenerator_U3CRadiusU3Ek__BackingField,
	SpatialGraphNode_tB39D8AA29F3B1CD7895B4A415466DD283651D722_CustomAttributesCacheGenerator_U3CIdU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CthumbstickU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CgripU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CgripPressedU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CmenuU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CprimaryButtonU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CsecondaryButtonU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CtriggerU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CtriggerPressedU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CthumbstickClickedU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CdevicePoseU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CpointerU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CisTrackedU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CtrackingStateU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CdevicePositionU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CdeviceRotationU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CpointerPositionU3Ek__BackingField,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_U3CpointerRotationU3Ek__BackingField,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_U3CSystemIdU3Ek__BackingField,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_U3CSessionU3Ek__BackingField,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_U3CSessionStateU3Ek__BackingField,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_U3CSceneOriginSpaceU3Ek__BackingField,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_U3CIsAnchorExtensionSupportedU3Ek__BackingField,
	NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885_CustomAttributesCacheGenerator_U3CNativeLibTokenU3Ek__BackingField,
	HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator_U3CPoseFlagsU3Ek__BackingField,
	HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator_U3CRotationU3Ek__BackingField,
	HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator_U3CPositionU3Ek__BackingField,
	HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator_U3CRadiusU3Ek__BackingField,
	AppRemoting_tD5F0CEC33C78A09D9DDF0ECFC8CCDAD2F953D5B1_CustomAttributesCacheGenerator_U3CConfigurationU3Ek__BackingField,
	EditorRemotingPlugin_t734E968BC26B3B3B989BAC5CA5817475ADD6A213_CustomAttributesCacheGenerator_m_remoteHostName,
	EditorRemotingPlugin_t734E968BC26B3B3B989BAC5CA5817475ADD6A213_CustomAttributesCacheGenerator_m_remoteHostPort,
	EditorRemotingPlugin_t734E968BC26B3B3B989BAC5CA5817475ADD6A213_CustomAttributesCacheGenerator_m_maxBitrate,
	EditorRemotingPlugin_t734E968BC26B3B3B989BAC5CA5817475ADD6A213_CustomAttributesCacheGenerator_m_videoCodec,
	EditorRemotingPlugin_t734E968BC26B3B3B989BAC5CA5817475ADD6A213_CustomAttributesCacheGenerator_m_enableAudio,
	AnchorSubsystemExtensions_tB5A59F746D2CB4DA8FA9A1DA683AC180BF62BFA5_CustomAttributesCacheGenerator_AnchorSubsystemExtensions_LoadAnchorStoreAsync_m6FE5CF203AAA962E70D9097D2832D42D94297AD8,
	XRAnchorStore_tC9CDBA7E2D11BA4B75CC29A81431B1550E37E79D_CustomAttributesCacheGenerator_XRAnchorStore_LoadAsync_mDA1D03EF31EBE83909DFB324028B9BD3E0E2EE08,
	XRAnchorStore_tC9CDBA7E2D11BA4B75CC29A81431B1550E37E79D_CustomAttributesCacheGenerator_XRAnchorStore_TryPersistAnchor_mDFC7EF535C882DE15CF999113AE78671EB2DD5D2,
	U3CLoadAsyncU3Ed__0_tEA68C7403FA60BAFB05AD91AD233559E5B8D9117_CustomAttributesCacheGenerator_U3CLoadAsyncU3Ed__0_SetStateMachine_mB84B24F4974916FB514D4D019E083C7773C8ADE3,
	HandMeshTracker_tA479DEB39A92E212DE310D3587B281DC9D7B0146_CustomAttributesCacheGenerator_HandMeshTracker_get_Left_m834FB22088CF46F9EF46DAC8731E6DF270106FAF,
	HandMeshTracker_tA479DEB39A92E212DE310D3587B281DC9D7B0146_CustomAttributesCacheGenerator_HandMeshTracker_get_Right_m6705D8B2C8AB6513519C4B8D8738FE36DFC33731,
	HandTracker_t3327910B18D234F06B185BFA99047E30046D0957_CustomAttributesCacheGenerator_HandTracker_get_Left_m708C38E9BC40B0FBEB07E9163C3148E8007A1A7F,
	HandTracker_t3327910B18D234F06B185BFA99047E30046D0957_CustomAttributesCacheGenerator_HandTracker_get_Right_m65535255C8267CCABFFDD58A6BE59F4DBCCAB159,
	HandJointLocation_t6417344072BEE803CA511D7582144CE369974CC2_CustomAttributesCacheGenerator_HandJointLocation_get_Pose_mEC69083D5F01D41D2737848B69B570C88771B652,
	HandJointLocation_t6417344072BEE803CA511D7582144CE369974CC2_CustomAttributesCacheGenerator_HandJointLocation_get_Radius_m59C2EE4AC8714AF1583CD70EA7CD5A68847C726F,
	SpatialGraphNode_tB39D8AA29F3B1CD7895B4A415466DD283651D722_CustomAttributesCacheGenerator_SpatialGraphNode_get_Id_m96236885EF759340AC6EE05414F163834A7268CD,
	SpatialGraphNode_tB39D8AA29F3B1CD7895B4A415466DD283651D722_CustomAttributesCacheGenerator_SpatialGraphNode_set_Id_mF0E64EED4BA607D324C97A2D08183EC05AE760EC,
	XRAnchorStore_t188192CAF2D549FA0523A675925575B745066B5E_CustomAttributesCacheGenerator_XRAnchorStore_LoadAsync_m822582D492817B68C688B330A1826EFC96EDA38D,
	U3CLoadAsyncU3Ed__6_tA92A8FCF33BC6C780B51AD9E5F2F7262A63AA788_CustomAttributesCacheGenerator_U3CLoadAsyncU3Ed__6_SetStateMachine_m3DB639B56B9ADE4D2CF850C4968E94B21C254D42,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_thumbstick_m90CA4687E93563B0DEB602449DAE1FFFC911F62C,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_thumbstick_mB63719233F55DCED55B0DEFE3F5D893D8D7B989C,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_grip_mCE781A33AAF069E177C6EF5100164BD66236C810,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_grip_m80A88B6E2D47AD5CC2722B7E10B4185FA96696DC,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_gripPressed_mEA1F4C574F8BB5A1D1A44FA98FD43AB5F52EE1B4,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_gripPressed_m862349937A453211DF89235CDE719DFDC01DDC0E,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_menu_mC0A16B388913E40C8EED70AE5F8CE640957F706A,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_menu_mF0C460935A0A1C28FE859EF75BB5FC3E2140F691,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_primaryButton_m378BDF8328E8A6DE3B0A33C985A464949669D8B8,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_primaryButton_mEA85F1585A82C77FA5693E33D0F7996783BCEA44,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_secondaryButton_m426DCE2368DD70DDFC5B3D0F67F9D6A70D0525DD,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_secondaryButton_m70E3935734AC36C792D1A54AFFDCF1C429E0353C,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_trigger_mF66176140D4592DE7122078E6242464EC87F63C8,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_trigger_m5E824031767A7B4E03A4DDD6A73C590D414A3229,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_triggerPressed_m8ED9173BF9C6028DD0970D25D15AC87DEAF7DA45,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_triggerPressed_m743356C12B8EFD746028482D7E57F5513914D3CE,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_thumbstickClicked_m528DA9346A052CB8CF6D6BFBDE6AD670CC3A4846,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_thumbstickClicked_mF3DC1E0A64B894C96E88ACEDC198EB1769D8B9F1,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_devicePose_m4CA8FBCD335A15BFC9C1D2DFAC011CCB7FF6D9C7,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_devicePose_mC0EAE470D05077DF7492B88D1A0549458217EA59,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_pointer_m3FBCC9B7054E2FE2C0B78C8EB6B7D68330D42BC0,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_pointer_mD3E0295F051BB52FD90B7C704C1EF3D509F78466,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_isTracked_m6DEA5B5F2EECC0129E12B53ECF10812A570125FC,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_isTracked_mF02911DC0F1C1E2D6C8EDF61E4D7DEAE9CF85B5C,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_trackingState_mAF48A26032D476F52E4538464FA9183F5CB8E1BC,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_trackingState_m6387B052D3C16EDB339858D9E5B38EA1028975CA,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_devicePosition_m684527E5770BAAEE45AFED473D4DACE9F5061B96,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_devicePosition_m448F72744C9775C8F30D2865077F87A5A7E80011,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_deviceRotation_mDDDAD598D77716DB2322BFB0E9E7FC86A19FCFF9,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_deviceRotation_mF9F5F1DEBF0B7D67F3B41E68E9288AE1A2349160,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_pointerPosition_m494846ED88B2D8FE279793989489381FDD3F2060,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_pointerPosition_m9E7EEC6DF082911E9BA713EB47E901118FF1EE52,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_get_pointerRotation_m9B28C8E1D41987CFBC8AA2594EC7BD3BA47566C7,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_set_pointerRotation_m96F55CDCC866F08C06204FE1445AC66E24D79CF1,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_get_Instance_m856697DBBE9ACDDA9DBEC30AC982DE83C54C3C08,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_set_Instance_mF9A1F53CF1611583D0CFB6BF2585D7001540474D,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_get_SystemId_m459184F68B27882DF83BAAE4B5F48866FF19C5FA,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_set_SystemId_m6A54A5F9A42FCB76E36E09DD9A4B1475E7D65BD7,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_get_Session_mFB429C810F15FD1E263D7442745B2560BB433361,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_set_Session_m536506957954E3925F96061AF725043CE677D24D,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_get_SessionState_mAF01E3EEFA8CA3502F36C58C97F8590ABC9E74A2,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_set_SessionState_m96318C37B8405D34B5118030898FF20FB6261AB4,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_get_SceneOriginSpace_m8528C0EF816F36F79EA22BE8E928BAC4919DDC7D,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_set_SceneOriginSpace_m62F6FC53CD5F94235500EFF2625374E9BB71C05F,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_get_IsAnchorExtensionSupported_m39F9A85A9F308065B77145022128FA85FDB87F0F,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_set_IsAnchorExtensionSupported_m29B7C2766A4873A5D88F0DE6D9FA6AE6EA5414AF,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_U3COnSubsystemCreateU3Eb__31_0_m7BFABC06D135282C6CF695AF56E1AF62E2A54785,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_U3COnSubsystemStartU3Eb__32_0_mA2935B0BBD5139B350D952458134045FFBFB4998,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_U3COnSubsystemStopU3Eb__33_0_m897469C0A358E68AB582CCB8C3485D37C7A0DDEC,
	OpenXRFeaturePlugin_1_t502C90675E8EEE52F048C77AC9CC43E1500C4729_CustomAttributesCacheGenerator_OpenXRFeaturePlugin_1_U3COnSubsystemDestroyU3Eb__34_0_mB01CE4556E7326990F5D5B404861A85F03F38EE1,
	NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885_CustomAttributesCacheGenerator_NativeLibTokenAttribute_get_NativeLibToken_mC21339A02A50AAF0A09469B71CE8F598281B18DF,
	NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885_CustomAttributesCacheGenerator_NativeLibTokenAttribute_set_NativeLibToken_mCF242DA8234A359AC667DFB290D6E3274ECE7F1C,
	AnchorSubsystem_t2B0BBDF9E87DC47D84CCE15AADB38E986788A81A_CustomAttributesCacheGenerator_AnchorSubsystem_RegisterDescriptor_mD1EA19E524C831940D55D6E869398D7467743A5F,
	PlaneSubsystem_t205C08800A7B7F42AFBB6919380A6638C0E6D43B_CustomAttributesCacheGenerator_PlaneSubsystem_RegisterDescriptor_mC49177E583BFA41281C5F20A9A3C3EB7E5929114,
	RaycastSubsystem_t12CBE986BE4482DF95595A906E5260CCD67D0516_CustomAttributesCacheGenerator_RaycastSubsystem_RegisterDescriptor_mDFFDCFFE9613F761ABF930299B6E11011BDEDBF9,
	HandTracker_t90D954B16904CEE895918391ACF55F69A5F036F4_CustomAttributesCacheGenerator_HandTracker_TryLocateHandJoints_m1043ECB88351DA411C4CAA43BA9C336D98BC3DEC,
	HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator_HandJointLocation_get_PoseFlags_m1CD68F1D99A888442B52E06B2464692300714423,
	HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator_HandJointLocation_get_Rotation_mE08792490B53A7951D778D5C2F9F7F7B89494E3D,
	HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator_HandJointLocation_get_Position_mD2FACF4383DD4FC9EE45505113C56C09C6636042,
	HandJointLocation_t1F00613833A64AA1507D85B4613D2D9CBC3DE282_CustomAttributesCacheGenerator_HandJointLocation_get_Radius_mDCE67E74FD5000DD8235D62B0A014B661F42BF83,
	AppRemoting_tD5F0CEC33C78A09D9DDF0ECFC8CCDAD2F953D5B1_CustomAttributesCacheGenerator_AppRemoting_get_Configuration_m518C2AFDA11E111274D91A3877515F2EE21B93A3,
	AppRemoting_tD5F0CEC33C78A09D9DDF0ECFC8CCDAD2F953D5B1_CustomAttributesCacheGenerator_AppRemoting_set_Configuration_mA37F1AC0EE17FCA0DFD43484FA0608E0D203DFD1,
	AppRemoting_tD5F0CEC33C78A09D9DDF0ECFC8CCDAD2F953D5B1_CustomAttributesCacheGenerator_AppRemoting_Connect_mFDF9ACF483419C1C1D1072B7B4E60AC4D714A3CF,
	U3CConnectU3Ed__4_t99E03D1D6720EB9A51A3076B4D7D7087A794913D_CustomAttributesCacheGenerator_U3CConnectU3Ed__4__ctor_m9B7169E748BDE0D567455598D91A91A7D2D60CA3,
	U3CConnectU3Ed__4_t99E03D1D6720EB9A51A3076B4D7D7087A794913D_CustomAttributesCacheGenerator_U3CConnectU3Ed__4_System_IDisposable_Dispose_mF2F8F709F0C863F20922D10016EF3769BF9CABA7,
	U3CConnectU3Ed__4_t99E03D1D6720EB9A51A3076B4D7D7087A794913D_CustomAttributesCacheGenerator_U3CConnectU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAEAED26889E7742A923273906179CB6DB875EF7D,
	U3CConnectU3Ed__4_t99E03D1D6720EB9A51A3076B4D7D7087A794913D_CustomAttributesCacheGenerator_U3CConnectU3Ed__4_System_Collections_IEnumerator_Reset_mCAB5E7A2915C5628DF53ABD73DD21D5536C5A493,
	U3CConnectU3Ed__4_t99E03D1D6720EB9A51A3076B4D7D7087A794913D_CustomAttributesCacheGenerator_U3CConnectU3Ed__4_System_Collections_IEnumerator_get_Current_m5FC03290D91E6AC38AFA1D30CD249EB00999BAAC,
	EditorRemotingPlugin_t734E968BC26B3B3B989BAC5CA5817475ADD6A213_CustomAttributesCacheGenerator_EditorRemotingPlugin_U3COnSystemChangeU3Eb__13_0_m2AD00416A38236601D9DC419598D621ABF7F64A7,
	ARAnchorExtensions_tCD94D89AF20632073D34D7FA9A35D07E2C57B144_CustomAttributesCacheGenerator_ARAnchorExtensions_GetOpenXRHandle_m07C39102695713B89B09A0AC5930F96FE51A7A80,
	AnchorManagerExtensions_t8C5C59841E7A464C3D0115EC2319484A56B2C386_CustomAttributesCacheGenerator_AnchorManagerExtensions_LoadAnchorStoreAsync_m7FFB9D7FFB415A46DDB8822EE8FE81A220056F7B,
	XRAnchorExtensions_t3810800404C0B971594FD62A513FF6EB59DE82F3_CustomAttributesCacheGenerator_XRAnchorExtensions_GetOpenXRHandle_m3AB1B804A8570EA669D000A64CAC3B0BD1C08B94,
	AnchorSubsystemExtensions_tD4092A164D3A2E5FFF1D1E24295042D0E8247CF8_CustomAttributesCacheGenerator_AnchorSubsystemExtensions_LoadAnchorStoreAsync_m04BE110C74F6AEA0055C448C3C74C02EBACA231D,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____thumbstick_PropertyInfo,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____grip_PropertyInfo,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____gripPressed_PropertyInfo,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____menu_PropertyInfo,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____primaryButton_PropertyInfo,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____secondaryButton_PropertyInfo,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____trigger_PropertyInfo,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____triggerPressed_PropertyInfo,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____thumbstickClicked_PropertyInfo,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____devicePose_PropertyInfo,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____pointer_PropertyInfo,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____isTracked_PropertyInfo,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____trackingState_PropertyInfo,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____devicePosition_PropertyInfo,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____deviceRotation_PropertyInfo,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____pointerPosition_PropertyInfo,
	HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA_CustomAttributesCacheGenerator_HPMixedRealityController_tEEF1392329E622907D7C1D948FDA19BF8D7431AA____pointerRotation_PropertyInfo,
	Microsoft_MixedReality_OpenXR_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InputControlLayoutAttribute_set_displayName_m51759C33760B60FB12DD2D2EDC8C431EA0FD1E20_inline (InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string displayName { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CdisplayNameU3Ek__BackingField_6(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InputControlLayoutAttribute_set_commonUsages_mE744432198FBB37B38B16119433EB748EED7CCF3_inline (InputControlLayoutAttribute_tD4D1C69B76A853B381AF67C608C42CAA19FEB984 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___value0, const RuntimeMethod* method)
{
	{
		// public string[] commonUsages { get; set; }
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = ___value0;
		__this->set_U3CcommonUsagesU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InputControlAttribute_set_aliases_mA2A0291BD4112A24F62155E0B58726AD28D5D1C5_inline (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___value0, const RuntimeMethod* method)
{
	{
		// public string[] aliases { get; set; }
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = ___value0;
		__this->set_U3CaliasesU3Ek__BackingField_9(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InputControlAttribute_set_offset_mD1C8106D674D63F7FB7F0A28FE066191DC9F3671_inline (InputControlAttribute_tCFC1C1312D7E91421F9D1B43C1B7A8E99B7B1CB8 * __this, uint32_t ___value0, const RuntimeMethod* method)
{
	{
		// public uint offset { get; set; } = InputStateBlock.InvalidOffset;
		uint32_t L_0 = ___value0;
		__this->set_U3CoffsetU3Ek__BackingField_12(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NativeLibTokenAttribute_set_NativeLibToken_mCF242DA8234A359AC667DFB290D6E3274ECE7F1C_inline (NativeLibTokenAttribute_t101C20607652D293AF5438CC036549EF78009885 * __this, uint64_t ___value0, const RuntimeMethod* method)
{
	{
		// public NativeLibToken NativeLibToken { get; set; }
		uint64_t L_0 = ___value0;
		__this->set_U3CNativeLibTokenU3Ek__BackingField_0(L_0);
		return;
	}
}
