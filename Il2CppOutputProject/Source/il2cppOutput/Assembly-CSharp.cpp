﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Action`1<TooltipSpawner>
struct Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5;
// System.Collections.Generic.List`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer>
struct List_1_tCC75F0E785C19F0A0419EF1B4CE550AB9D7D9C4E;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<PartOfWind>
struct List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD;
// System.Collections.Generic.List`1<TooltipSpawner>
struct List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// PartOfWind[]
struct PartOfWindU5BU5D_tA1826ADB5FCF4A6D4A69A81917ED5C106EE0B948;
// TooltipSpawner[]
struct TooltipSpawnerU5BU5D_tAF74AB44BDAB88CFC79010351C363C7FD05266E8;
// AnchorsManager
struct AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC;
// AnimationWindTurbine
struct AnimationWindTurbine_t4BF2E55E0F8593B7F88AA65AA3303BE55D302062;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer
struct IMixedRealityPointer_t7F0507B7CE0C9CDF14089C77B4F5A079AE031B4D;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// ParseWind
struct ParseWind_t4ABEBB3AABE820B96DD97941E446E648C8AD516C;
// PartOfWind
struct PartOfWind_t675167C1ED83369524C5CDE22EEB3CBFF95869E2;
// Microsoft.MixedReality.Toolkit.UI.PinchSlider
struct PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB;
// Microsoft.MixedReality.Toolkit.UI.PrefabSpawner
struct PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7;
// Microsoft.MixedReality.Toolkit.UI.SliderEvent
struct SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A;
// Microsoft.MixedReality.Toolkit.Experimental.UI.StepSlider
struct StepSlider_tD03ECCB31FD5B24E2C0E2C3B981B89C840C39ACB;
// System.String
struct String_t;
// Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner
struct ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447;
// TooltipSpawner
struct TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C RuntimeClass* Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1_Invoke_mB325466CD8F2E6D4ECD81C594DC95027469165AC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_m667E5E34007C1D87D8312377ABC92E72E1DDDCB9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AnchorsManager_ChangeActiveAnchor_mA4FBD1A34D1A68C468A15406D7EA691C32DE64C2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mD643E85EDAC4C30B2A8311E2BF006DA050EDEFFB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mEB6F59307D7B220604F8BF85B5E403C6CB8EB5B8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m0147E66892FD97D8307DB5E727B5177A22E582DD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mE3AC4D65283C491DD7D72CB75834911030B8D401_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m169AEC581C3039A4FE73767BC83B5191827B7B13_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m4FCC4BC0A3A876E6D9494F53E7574EDA93EB3401_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m15901002F3A52C49BF62161FED450C3A6B9CA7B5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_mDE06CDC58BB81D52E19406DF046B000078C00AEC_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tFBC2A5BA5AC9D38DF0A6A6F318DD010DA6886212 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<PartOfWind>
struct  List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PartOfWindU5BU5D_tA1826ADB5FCF4A6D4A69A81917ED5C106EE0B948* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD, ____items_1)); }
	inline PartOfWindU5BU5D_tA1826ADB5FCF4A6D4A69A81917ED5C106EE0B948* get__items_1() const { return ____items_1; }
	inline PartOfWindU5BU5D_tA1826ADB5FCF4A6D4A69A81917ED5C106EE0B948** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PartOfWindU5BU5D_tA1826ADB5FCF4A6D4A69A81917ED5C106EE0B948* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	PartOfWindU5BU5D_tA1826ADB5FCF4A6D4A69A81917ED5C106EE0B948* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD_StaticFields, ____emptyArray_5)); }
	inline PartOfWindU5BU5D_tA1826ADB5FCF4A6D4A69A81917ED5C106EE0B948* get__emptyArray_5() const { return ____emptyArray_5; }
	inline PartOfWindU5BU5D_tA1826ADB5FCF4A6D4A69A81917ED5C106EE0B948** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(PartOfWindU5BU5D_tA1826ADB5FCF4A6D4A69A81917ED5C106EE0B948* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<TooltipSpawner>
struct  List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TooltipSpawnerU5BU5D_tAF74AB44BDAB88CFC79010351C363C7FD05266E8* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2, ____items_1)); }
	inline TooltipSpawnerU5BU5D_tAF74AB44BDAB88CFC79010351C363C7FD05266E8* get__items_1() const { return ____items_1; }
	inline TooltipSpawnerU5BU5D_tAF74AB44BDAB88CFC79010351C363C7FD05266E8** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TooltipSpawnerU5BU5D_tAF74AB44BDAB88CFC79010351C363C7FD05266E8* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TooltipSpawnerU5BU5D_tAF74AB44BDAB88CFC79010351C363C7FD05266E8* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2_StaticFields, ____emptyArray_5)); }
	inline TooltipSpawnerU5BU5D_tAF74AB44BDAB88CFC79010351C363C7FD05266E8* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TooltipSpawnerU5BU5D_tAF74AB44BDAB88CFC79010351C363C7FD05266E8** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TooltipSpawnerU5BU5D_tAF74AB44BDAB88CFC79010351C363C7FD05266E8* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<PartOfWind>
struct  Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	PartOfWind_t675167C1ED83369524C5CDE22EEB3CBFF95869E2 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006, ___list_0)); }
	inline List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD * get_list_0() const { return ___list_0; }
	inline List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006, ___current_3)); }
	inline PartOfWind_t675167C1ED83369524C5CDE22EEB3CBFF95869E2 * get_current_3() const { return ___current_3; }
	inline PartOfWind_t675167C1ED83369524C5CDE22EEB3CBFF95869E2 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(PartOfWind_t675167C1ED83369524C5CDE22EEB3CBFF95869E2 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<TooltipSpawner>
struct  Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900, ___list_0)); }
	inline List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2 * get_list_0() const { return ___list_0; }
	inline List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900, ___current_3)); }
	inline TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * get_current_3() const { return ___current_3; }
	inline TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// Microsoft.MixedReality.Toolkit.Utilities.AxisType
struct  AxisType_tFA382360CBEDF34BB549410E81A1B1D1FA76A459 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.Utilities.AxisType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisType_tFA382360CBEDF34BB549410E81A1B1D1FA76A459, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.UI.ConnectorFollowType
struct  ConnectorFollowType_tF74943D81D15B06C47A89202664D96C959B12295 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.UI.ConnectorFollowType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectorFollowType_tF74943D81D15B06C47A89202664D96C959B12295, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.UI.ConnectorOrientType
struct  ConnectorOrientType_tDBEF782A0927ACFB0A063D2E81DD8D1AB92700DF 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.UI.ConnectorOrientType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectorOrientType_tDBEF782A0927ACFB0A063D2E81DD8D1AB92700DF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.UI.ConnectorPivotDirection
struct  ConnectorPivotDirection_t5DE51E29DD6CC13CF359C451F21E680A8FC9F07C 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.UI.ConnectorPivotDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectorPivotDirection_t5DE51E29DD6CC13CF359C451F21E680A8FC9F07C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.UI.ConnectorPivotMode
struct  ConnectorPivotMode_tB0157EA4A4EB00A70E9DF73B4EC2E2445DA4E5D5 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.UI.ConnectorPivotMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectorPivotMode_tB0157EA4A4EB00A70E9DF73B4EC2E2445DA4E5D5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// Microsoft.MixedReality.Toolkit.UI.SliderAxis
struct  SliderAxis_t678A81BAF2FC7FB65D2DCE532AB425CDD4C3BFE2 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.UI.SliderAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SliderAxis_t678A81BAF2FC7FB65D2DCE532AB425CDD4C3BFE2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Space
struct  Space_t568D704D2B0AAC3E5894DDFF13DB2E02E2CD539E 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Space_t568D704D2B0AAC3E5894DDFF13DB2E02E2CD539E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.UI.PrefabSpawner/AppearType
struct  AppearType_t90703008729C558E06D08733CD29C10368773E7B 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.UI.PrefabSpawner/AppearType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AppearType_t90703008729C558E06D08733CD29C10368773E7B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.UI.PrefabSpawner/RemainType
struct  RemainType_t2309F41E30C5035567A1E40ED24C2824DCFF6BBA 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.UI.PrefabSpawner/RemainType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RemainType_t2309F41E30C5035567A1E40ED24C2824DCFF6BBA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.UI.PrefabSpawner/VanishType
struct  VanishType_tCF74DC870B4C5F6AC87509AFCCAF717435FC0483 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.UI.PrefabSpawner/VanishType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VanishType_tCF74DC870B4C5F6AC87509AFCCAF717435FC0483, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner/SettingsMode
struct  SettingsMode_tC01B96084B6539F4EA17B5C999BAED20A5E2DD1F 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner/SettingsMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SettingsMode_tC01B96084B6539F4EA17B5C999BAED20A5E2DD1F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Nullable`1<Microsoft.MixedReality.Toolkit.UI.SliderAxis>
struct  Nullable_1_t92F5760966FC3E12D913444A132AD4F575C089E3 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t92F5760966FC3E12D913444A132AD4F575C089E3, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t92F5760966FC3E12D913444A132AD4F575C089E3, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction
struct  MixedRealityInputAction_tF7F97EE20DEA505B55C4ACFDAE9DB4E9C27D2690 
{
public:
	// System.UInt32 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction::id
	uint32_t ___id_1;
	// System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction::description
	String_t* ___description_2;
	// Microsoft.MixedReality.Toolkit.Utilities.AxisType Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction::axisConstraint
	int32_t ___axisConstraint_3;

public:
	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(MixedRealityInputAction_tF7F97EE20DEA505B55C4ACFDAE9DB4E9C27D2690, ___id_1)); }
	inline uint32_t get_id_1() const { return ___id_1; }
	inline uint32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(uint32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_description_2() { return static_cast<int32_t>(offsetof(MixedRealityInputAction_tF7F97EE20DEA505B55C4ACFDAE9DB4E9C27D2690, ___description_2)); }
	inline String_t* get_description_2() const { return ___description_2; }
	inline String_t** get_address_of_description_2() { return &___description_2; }
	inline void set_description_2(String_t* value)
	{
		___description_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___description_2), (void*)value);
	}

	inline static int32_t get_offset_of_axisConstraint_3() { return static_cast<int32_t>(offsetof(MixedRealityInputAction_tF7F97EE20DEA505B55C4ACFDAE9DB4E9C27D2690, ___axisConstraint_3)); }
	inline int32_t get_axisConstraint_3() const { return ___axisConstraint_3; }
	inline int32_t* get_address_of_axisConstraint_3() { return &___axisConstraint_3; }
	inline void set_axisConstraint_3(int32_t value)
	{
		___axisConstraint_3 = value;
	}
};

struct MixedRealityInputAction_tF7F97EE20DEA505B55C4ACFDAE9DB4E9C27D2690_StaticFields
{
public:
	// Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction::<None>k__BackingField
	MixedRealityInputAction_tF7F97EE20DEA505B55C4ACFDAE9DB4E9C27D2690  ___U3CNoneU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNoneU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MixedRealityInputAction_tF7F97EE20DEA505B55C4ACFDAE9DB4E9C27D2690_StaticFields, ___U3CNoneU3Ek__BackingField_0)); }
	inline MixedRealityInputAction_tF7F97EE20DEA505B55C4ACFDAE9DB4E9C27D2690  get_U3CNoneU3Ek__BackingField_0() const { return ___U3CNoneU3Ek__BackingField_0; }
	inline MixedRealityInputAction_tF7F97EE20DEA505B55C4ACFDAE9DB4E9C27D2690 * get_address_of_U3CNoneU3Ek__BackingField_0() { return &___U3CNoneU3Ek__BackingField_0; }
	inline void set_U3CNoneU3Ek__BackingField_0(MixedRealityInputAction_tF7F97EE20DEA505B55C4ACFDAE9DB4E9C27D2690  value)
	{
		___U3CNoneU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CNoneU3Ek__BackingField_0))->___description_2), (void*)NULL);
	}
};

// Native definition for P/Invoke marshalling of Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction
struct MixedRealityInputAction_tF7F97EE20DEA505B55C4ACFDAE9DB4E9C27D2690_marshaled_pinvoke
{
	uint32_t ___id_1;
	char* ___description_2;
	int32_t ___axisConstraint_3;
};
// Native definition for COM marshalling of Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction
struct MixedRealityInputAction_tF7F97EE20DEA505B55C4ACFDAE9DB4E9C27D2690_marshaled_com
{
	uint32_t ___id_1;
	Il2CppChar* ___description_2;
	int32_t ___axisConstraint_3;
};

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Action`1<TooltipSpawner>
struct  Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// AnchorsManager
struct  AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Collections.Generic.List`1<TooltipSpawner> AnchorsManager::anchors
	List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2 * ___anchors_5;
	// TooltipSpawner AnchorsManager::activeAnchorTooltip
	TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * ___activeAnchorTooltip_6;
	// System.Boolean AnchorsManager::isAll
	bool ___isAll_7;

public:
	inline static int32_t get_offset_of_anchors_5() { return static_cast<int32_t>(offsetof(AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC, ___anchors_5)); }
	inline List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2 * get_anchors_5() const { return ___anchors_5; }
	inline List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2 ** get_address_of_anchors_5() { return &___anchors_5; }
	inline void set_anchors_5(List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2 * value)
	{
		___anchors_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anchors_5), (void*)value);
	}

	inline static int32_t get_offset_of_activeAnchorTooltip_6() { return static_cast<int32_t>(offsetof(AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC, ___activeAnchorTooltip_6)); }
	inline TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * get_activeAnchorTooltip_6() const { return ___activeAnchorTooltip_6; }
	inline TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE ** get_address_of_activeAnchorTooltip_6() { return &___activeAnchorTooltip_6; }
	inline void set_activeAnchorTooltip_6(TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * value)
	{
		___activeAnchorTooltip_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___activeAnchorTooltip_6), (void*)value);
	}

	inline static int32_t get_offset_of_isAll_7() { return static_cast<int32_t>(offsetof(AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC, ___isAll_7)); }
	inline bool get_isAll_7() const { return ___isAll_7; }
	inline bool* get_address_of_isAll_7() { return &___isAll_7; }
	inline void set_isAll_7(bool value)
	{
		___isAll_7 = value;
	}
};

struct AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC_StaticFields
{
public:
	// AnchorsManager AnchorsManager::Instance
	AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC_StaticFields, ___Instance_4)); }
	inline AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC * get_Instance_4() const { return ___Instance_4; }
	inline AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Instance_4), (void*)value);
	}
};


// AnimationWindTurbine
struct  AnimationWindTurbine_t4BF2E55E0F8593B7F88AA65AA3303BE55D302062  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject AnimationWindTurbine::blades
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___blades_4;
	// System.Boolean AnimationWindTurbine::isAnimated
	bool ___isAnimated_5;
	// System.Single AnimationWindTurbine::speed
	float ___speed_6;
	// System.Single AnimationWindTurbine::xAngles
	float ___xAngles_7;

public:
	inline static int32_t get_offset_of_blades_4() { return static_cast<int32_t>(offsetof(AnimationWindTurbine_t4BF2E55E0F8593B7F88AA65AA3303BE55D302062, ___blades_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_blades_4() const { return ___blades_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_blades_4() { return &___blades_4; }
	inline void set_blades_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___blades_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___blades_4), (void*)value);
	}

	inline static int32_t get_offset_of_isAnimated_5() { return static_cast<int32_t>(offsetof(AnimationWindTurbine_t4BF2E55E0F8593B7F88AA65AA3303BE55D302062, ___isAnimated_5)); }
	inline bool get_isAnimated_5() const { return ___isAnimated_5; }
	inline bool* get_address_of_isAnimated_5() { return &___isAnimated_5; }
	inline void set_isAnimated_5(bool value)
	{
		___isAnimated_5 = value;
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(AnimationWindTurbine_t4BF2E55E0F8593B7F88AA65AA3303BE55D302062, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_xAngles_7() { return static_cast<int32_t>(offsetof(AnimationWindTurbine_t4BF2E55E0F8593B7F88AA65AA3303BE55D302062, ___xAngles_7)); }
	inline float get_xAngles_7() const { return ___xAngles_7; }
	inline float* get_address_of_xAngles_7() { return &___xAngles_7; }
	inline void set_xAngles_7(float value)
	{
		___xAngles_7 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Input.BaseFocusHandler
struct  BaseFocusHandler_t2A6499860D7862CF297709FEF5126E3553E08ABB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.BaseFocusHandler::focusEnabled
	bool ___focusEnabled_4;
	// System.Collections.Generic.List`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer> Microsoft.MixedReality.Toolkit.Input.BaseFocusHandler::<Focusers>k__BackingField
	List_1_tCC75F0E785C19F0A0419EF1B4CE550AB9D7D9C4E * ___U3CFocusersU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_focusEnabled_4() { return static_cast<int32_t>(offsetof(BaseFocusHandler_t2A6499860D7862CF297709FEF5126E3553E08ABB, ___focusEnabled_4)); }
	inline bool get_focusEnabled_4() const { return ___focusEnabled_4; }
	inline bool* get_address_of_focusEnabled_4() { return &___focusEnabled_4; }
	inline void set_focusEnabled_4(bool value)
	{
		___focusEnabled_4 = value;
	}

	inline static int32_t get_offset_of_U3CFocusersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BaseFocusHandler_t2A6499860D7862CF297709FEF5126E3553E08ABB, ___U3CFocusersU3Ek__BackingField_5)); }
	inline List_1_tCC75F0E785C19F0A0419EF1B4CE550AB9D7D9C4E * get_U3CFocusersU3Ek__BackingField_5() const { return ___U3CFocusersU3Ek__BackingField_5; }
	inline List_1_tCC75F0E785C19F0A0419EF1B4CE550AB9D7D9C4E ** get_address_of_U3CFocusersU3Ek__BackingField_5() { return &___U3CFocusersU3Ek__BackingField_5; }
	inline void set_U3CFocusersU3Ek__BackingField_5(List_1_tCC75F0E785C19F0A0419EF1B4CE550AB9D7D9C4E * value)
	{
		___U3CFocusersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFocusersU3Ek__BackingField_5), (void*)value);
	}
};


// ParseWind
struct  ParseWind_t4ABEBB3AABE820B96DD97941E446E648C8AD516C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Microsoft.MixedReality.Toolkit.Experimental.UI.StepSlider ParseWind::slider
	StepSlider_tD03ECCB31FD5B24E2C0E2C3B981B89C840C39ACB * ___slider_4;
	// System.Collections.Generic.List`1<PartOfWind> ParseWind::parts
	List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD * ___parts_5;

public:
	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(ParseWind_t4ABEBB3AABE820B96DD97941E446E648C8AD516C, ___slider_4)); }
	inline StepSlider_tD03ECCB31FD5B24E2C0E2C3B981B89C840C39ACB * get_slider_4() const { return ___slider_4; }
	inline StepSlider_tD03ECCB31FD5B24E2C0E2C3B981B89C840C39ACB ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(StepSlider_tD03ECCB31FD5B24E2C0E2C3B981B89C840C39ACB * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___slider_4), (void*)value);
	}

	inline static int32_t get_offset_of_parts_5() { return static_cast<int32_t>(offsetof(ParseWind_t4ABEBB3AABE820B96DD97941E446E648C8AD516C, ___parts_5)); }
	inline List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD * get_parts_5() const { return ___parts_5; }
	inline List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD ** get_address_of_parts_5() { return &___parts_5; }
	inline void set_parts_5(List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD * value)
	{
		___parts_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parts_5), (void*)value);
	}
};


// PartOfWind
struct  PartOfWind_t675167C1ED83369524C5CDE22EEB3CBFF95869E2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform PartOfWind::startPosition
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___startPosition_4;
	// UnityEngine.Transform PartOfWind::endPosition
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___endPosition_5;

public:
	inline static int32_t get_offset_of_startPosition_4() { return static_cast<int32_t>(offsetof(PartOfWind_t675167C1ED83369524C5CDE22EEB3CBFF95869E2, ___startPosition_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_startPosition_4() const { return ___startPosition_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_startPosition_4() { return &___startPosition_4; }
	inline void set_startPosition_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___startPosition_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___startPosition_4), (void*)value);
	}

	inline static int32_t get_offset_of_endPosition_5() { return static_cast<int32_t>(offsetof(PartOfWind_t675167C1ED83369524C5CDE22EEB3CBFF95869E2, ___endPosition_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_endPosition_5() const { return ___endPosition_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_endPosition_5() { return &___endPosition_5; }
	inline void set_endPosition_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___endPosition_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___endPosition_5), (void*)value);
	}
};


// Microsoft.MixedReality.Toolkit.UI.PinchSlider
struct  PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.UI.PinchSlider::thumbRoot
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___thumbRoot_4;
	// System.Single Microsoft.MixedReality.Toolkit.UI.PinchSlider::sliderValue
	float ___sliderValue_5;
	// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.UI.PinchSlider::trackVisuals
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___trackVisuals_6;
	// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.UI.PinchSlider::tickMarks
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___tickMarks_7;
	// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.UI.PinchSlider::thumbVisuals
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___thumbVisuals_8;
	// Microsoft.MixedReality.Toolkit.UI.SliderAxis Microsoft.MixedReality.Toolkit.UI.PinchSlider::sliderAxis
	int32_t ___sliderAxis_9;
	// System.Nullable`1<Microsoft.MixedReality.Toolkit.UI.SliderAxis> Microsoft.MixedReality.Toolkit.UI.PinchSlider::previousSliderAxis
	Nullable_1_t92F5760966FC3E12D913444A132AD4F575C089E3  ___previousSliderAxis_10;
	// System.Single Microsoft.MixedReality.Toolkit.UI.PinchSlider::sliderStartDistance
	float ___sliderStartDistance_11;
	// System.Single Microsoft.MixedReality.Toolkit.UI.PinchSlider::sliderEndDistance
	float ___sliderEndDistance_12;
	// Microsoft.MixedReality.Toolkit.UI.SliderEvent Microsoft.MixedReality.Toolkit.UI.PinchSlider::OnValueUpdated
	SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A * ___OnValueUpdated_13;
	// Microsoft.MixedReality.Toolkit.UI.SliderEvent Microsoft.MixedReality.Toolkit.UI.PinchSlider::OnInteractionStarted
	SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A * ___OnInteractionStarted_14;
	// Microsoft.MixedReality.Toolkit.UI.SliderEvent Microsoft.MixedReality.Toolkit.UI.PinchSlider::OnInteractionEnded
	SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A * ___OnInteractionEnded_15;
	// Microsoft.MixedReality.Toolkit.UI.SliderEvent Microsoft.MixedReality.Toolkit.UI.PinchSlider::OnHoverEntered
	SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A * ___OnHoverEntered_16;
	// Microsoft.MixedReality.Toolkit.UI.SliderEvent Microsoft.MixedReality.Toolkit.UI.PinchSlider::OnHoverExited
	SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A * ___OnHoverExited_17;
	// UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.UI.PinchSlider::sliderThumbOffset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___sliderThumbOffset_18;
	// System.Single Microsoft.MixedReality.Toolkit.UI.PinchSlider::<StartSliderValue>k__BackingField
	float ___U3CStartSliderValueU3Ek__BackingField_19;
	// UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.UI.PinchSlider::<StartPointerPosition>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CStartPointerPositionU3Ek__BackingField_20;
	// Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer Microsoft.MixedReality.Toolkit.UI.PinchSlider::<ActivePointer>k__BackingField
	RuntimeObject* ___U3CActivePointerU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_thumbRoot_4() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___thumbRoot_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_thumbRoot_4() const { return ___thumbRoot_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_thumbRoot_4() { return &___thumbRoot_4; }
	inline void set_thumbRoot_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___thumbRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___thumbRoot_4), (void*)value);
	}

	inline static int32_t get_offset_of_sliderValue_5() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___sliderValue_5)); }
	inline float get_sliderValue_5() const { return ___sliderValue_5; }
	inline float* get_address_of_sliderValue_5() { return &___sliderValue_5; }
	inline void set_sliderValue_5(float value)
	{
		___sliderValue_5 = value;
	}

	inline static int32_t get_offset_of_trackVisuals_6() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___trackVisuals_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_trackVisuals_6() const { return ___trackVisuals_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_trackVisuals_6() { return &___trackVisuals_6; }
	inline void set_trackVisuals_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___trackVisuals_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackVisuals_6), (void*)value);
	}

	inline static int32_t get_offset_of_tickMarks_7() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___tickMarks_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_tickMarks_7() const { return ___tickMarks_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_tickMarks_7() { return &___tickMarks_7; }
	inline void set_tickMarks_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___tickMarks_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tickMarks_7), (void*)value);
	}

	inline static int32_t get_offset_of_thumbVisuals_8() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___thumbVisuals_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_thumbVisuals_8() const { return ___thumbVisuals_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_thumbVisuals_8() { return &___thumbVisuals_8; }
	inline void set_thumbVisuals_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___thumbVisuals_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___thumbVisuals_8), (void*)value);
	}

	inline static int32_t get_offset_of_sliderAxis_9() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___sliderAxis_9)); }
	inline int32_t get_sliderAxis_9() const { return ___sliderAxis_9; }
	inline int32_t* get_address_of_sliderAxis_9() { return &___sliderAxis_9; }
	inline void set_sliderAxis_9(int32_t value)
	{
		___sliderAxis_9 = value;
	}

	inline static int32_t get_offset_of_previousSliderAxis_10() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___previousSliderAxis_10)); }
	inline Nullable_1_t92F5760966FC3E12D913444A132AD4F575C089E3  get_previousSliderAxis_10() const { return ___previousSliderAxis_10; }
	inline Nullable_1_t92F5760966FC3E12D913444A132AD4F575C089E3 * get_address_of_previousSliderAxis_10() { return &___previousSliderAxis_10; }
	inline void set_previousSliderAxis_10(Nullable_1_t92F5760966FC3E12D913444A132AD4F575C089E3  value)
	{
		___previousSliderAxis_10 = value;
	}

	inline static int32_t get_offset_of_sliderStartDistance_11() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___sliderStartDistance_11)); }
	inline float get_sliderStartDistance_11() const { return ___sliderStartDistance_11; }
	inline float* get_address_of_sliderStartDistance_11() { return &___sliderStartDistance_11; }
	inline void set_sliderStartDistance_11(float value)
	{
		___sliderStartDistance_11 = value;
	}

	inline static int32_t get_offset_of_sliderEndDistance_12() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___sliderEndDistance_12)); }
	inline float get_sliderEndDistance_12() const { return ___sliderEndDistance_12; }
	inline float* get_address_of_sliderEndDistance_12() { return &___sliderEndDistance_12; }
	inline void set_sliderEndDistance_12(float value)
	{
		___sliderEndDistance_12 = value;
	}

	inline static int32_t get_offset_of_OnValueUpdated_13() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___OnValueUpdated_13)); }
	inline SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A * get_OnValueUpdated_13() const { return ___OnValueUpdated_13; }
	inline SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A ** get_address_of_OnValueUpdated_13() { return &___OnValueUpdated_13; }
	inline void set_OnValueUpdated_13(SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A * value)
	{
		___OnValueUpdated_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnValueUpdated_13), (void*)value);
	}

	inline static int32_t get_offset_of_OnInteractionStarted_14() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___OnInteractionStarted_14)); }
	inline SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A * get_OnInteractionStarted_14() const { return ___OnInteractionStarted_14; }
	inline SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A ** get_address_of_OnInteractionStarted_14() { return &___OnInteractionStarted_14; }
	inline void set_OnInteractionStarted_14(SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A * value)
	{
		___OnInteractionStarted_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnInteractionStarted_14), (void*)value);
	}

	inline static int32_t get_offset_of_OnInteractionEnded_15() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___OnInteractionEnded_15)); }
	inline SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A * get_OnInteractionEnded_15() const { return ___OnInteractionEnded_15; }
	inline SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A ** get_address_of_OnInteractionEnded_15() { return &___OnInteractionEnded_15; }
	inline void set_OnInteractionEnded_15(SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A * value)
	{
		___OnInteractionEnded_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnInteractionEnded_15), (void*)value);
	}

	inline static int32_t get_offset_of_OnHoverEntered_16() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___OnHoverEntered_16)); }
	inline SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A * get_OnHoverEntered_16() const { return ___OnHoverEntered_16; }
	inline SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A ** get_address_of_OnHoverEntered_16() { return &___OnHoverEntered_16; }
	inline void set_OnHoverEntered_16(SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A * value)
	{
		___OnHoverEntered_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnHoverEntered_16), (void*)value);
	}

	inline static int32_t get_offset_of_OnHoverExited_17() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___OnHoverExited_17)); }
	inline SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A * get_OnHoverExited_17() const { return ___OnHoverExited_17; }
	inline SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A ** get_address_of_OnHoverExited_17() { return &___OnHoverExited_17; }
	inline void set_OnHoverExited_17(SliderEvent_tA3EEFC6ECD84EB2ABFDD8E5D635D59BFAA30F28A * value)
	{
		___OnHoverExited_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnHoverExited_17), (void*)value);
	}

	inline static int32_t get_offset_of_sliderThumbOffset_18() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___sliderThumbOffset_18)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_sliderThumbOffset_18() const { return ___sliderThumbOffset_18; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_sliderThumbOffset_18() { return &___sliderThumbOffset_18; }
	inline void set_sliderThumbOffset_18(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___sliderThumbOffset_18 = value;
	}

	inline static int32_t get_offset_of_U3CStartSliderValueU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___U3CStartSliderValueU3Ek__BackingField_19)); }
	inline float get_U3CStartSliderValueU3Ek__BackingField_19() const { return ___U3CStartSliderValueU3Ek__BackingField_19; }
	inline float* get_address_of_U3CStartSliderValueU3Ek__BackingField_19() { return &___U3CStartSliderValueU3Ek__BackingField_19; }
	inline void set_U3CStartSliderValueU3Ek__BackingField_19(float value)
	{
		___U3CStartSliderValueU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CStartPointerPositionU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___U3CStartPointerPositionU3Ek__BackingField_20)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CStartPointerPositionU3Ek__BackingField_20() const { return ___U3CStartPointerPositionU3Ek__BackingField_20; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CStartPointerPositionU3Ek__BackingField_20() { return &___U3CStartPointerPositionU3Ek__BackingField_20; }
	inline void set_U3CStartPointerPositionU3Ek__BackingField_20(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CStartPointerPositionU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CActivePointerU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB, ___U3CActivePointerU3Ek__BackingField_21)); }
	inline RuntimeObject* get_U3CActivePointerU3Ek__BackingField_21() const { return ___U3CActivePointerU3Ek__BackingField_21; }
	inline RuntimeObject** get_address_of_U3CActivePointerU3Ek__BackingField_21() { return &___U3CActivePointerU3Ek__BackingField_21; }
	inline void set_U3CActivePointerU3Ek__BackingField_21(RuntimeObject* value)
	{
		___U3CActivePointerU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CActivePointerU3Ek__BackingField_21), (void*)value);
	}
};


// Microsoft.MixedReality.Toolkit.UI.PrefabSpawner
struct  PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7  : public BaseFocusHandler_t2A6499860D7862CF297709FEF5126E3553E08ABB
{
public:
	// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.UI.PrefabSpawner::prefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___prefab_6;
	// Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.UI.PrefabSpawner::tooltipToggleAction
	MixedRealityInputAction_tF7F97EE20DEA505B55C4ACFDAE9DB4E9C27D2690  ___tooltipToggleAction_7;
	// Microsoft.MixedReality.Toolkit.UI.PrefabSpawner/AppearType Microsoft.MixedReality.Toolkit.UI.PrefabSpawner::appearType
	int32_t ___appearType_8;
	// Microsoft.MixedReality.Toolkit.UI.PrefabSpawner/VanishType Microsoft.MixedReality.Toolkit.UI.PrefabSpawner::vanishType
	int32_t ___vanishType_9;
	// Microsoft.MixedReality.Toolkit.UI.PrefabSpawner/RemainType Microsoft.MixedReality.Toolkit.UI.PrefabSpawner::remainType
	int32_t ___remainType_10;
	// System.Single Microsoft.MixedReality.Toolkit.UI.PrefabSpawner::appearDelay
	float ___appearDelay_11;
	// System.Single Microsoft.MixedReality.Toolkit.UI.PrefabSpawner::vanishDelay
	float ___vanishDelay_12;
	// System.Single Microsoft.MixedReality.Toolkit.UI.PrefabSpawner::lifetime
	float ___lifetime_13;
	// System.Boolean Microsoft.MixedReality.Toolkit.UI.PrefabSpawner::keepWorldRotation
	bool ___keepWorldRotation_14;
	// System.Single Microsoft.MixedReality.Toolkit.UI.PrefabSpawner::focusEnterTime
	float ___focusEnterTime_15;
	// System.Single Microsoft.MixedReality.Toolkit.UI.PrefabSpawner::focusExitTime
	float ___focusExitTime_16;
	// System.Single Microsoft.MixedReality.Toolkit.UI.PrefabSpawner::tappedTime
	float ___tappedTime_17;
	// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.UI.PrefabSpawner::spawnable
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___spawnable_18;

public:
	inline static int32_t get_offset_of_prefab_6() { return static_cast<int32_t>(offsetof(PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7, ___prefab_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_prefab_6() const { return ___prefab_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_prefab_6() { return &___prefab_6; }
	inline void set_prefab_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___prefab_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefab_6), (void*)value);
	}

	inline static int32_t get_offset_of_tooltipToggleAction_7() { return static_cast<int32_t>(offsetof(PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7, ___tooltipToggleAction_7)); }
	inline MixedRealityInputAction_tF7F97EE20DEA505B55C4ACFDAE9DB4E9C27D2690  get_tooltipToggleAction_7() const { return ___tooltipToggleAction_7; }
	inline MixedRealityInputAction_tF7F97EE20DEA505B55C4ACFDAE9DB4E9C27D2690 * get_address_of_tooltipToggleAction_7() { return &___tooltipToggleAction_7; }
	inline void set_tooltipToggleAction_7(MixedRealityInputAction_tF7F97EE20DEA505B55C4ACFDAE9DB4E9C27D2690  value)
	{
		___tooltipToggleAction_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___tooltipToggleAction_7))->___description_2), (void*)NULL);
	}

	inline static int32_t get_offset_of_appearType_8() { return static_cast<int32_t>(offsetof(PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7, ___appearType_8)); }
	inline int32_t get_appearType_8() const { return ___appearType_8; }
	inline int32_t* get_address_of_appearType_8() { return &___appearType_8; }
	inline void set_appearType_8(int32_t value)
	{
		___appearType_8 = value;
	}

	inline static int32_t get_offset_of_vanishType_9() { return static_cast<int32_t>(offsetof(PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7, ___vanishType_9)); }
	inline int32_t get_vanishType_9() const { return ___vanishType_9; }
	inline int32_t* get_address_of_vanishType_9() { return &___vanishType_9; }
	inline void set_vanishType_9(int32_t value)
	{
		___vanishType_9 = value;
	}

	inline static int32_t get_offset_of_remainType_10() { return static_cast<int32_t>(offsetof(PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7, ___remainType_10)); }
	inline int32_t get_remainType_10() const { return ___remainType_10; }
	inline int32_t* get_address_of_remainType_10() { return &___remainType_10; }
	inline void set_remainType_10(int32_t value)
	{
		___remainType_10 = value;
	}

	inline static int32_t get_offset_of_appearDelay_11() { return static_cast<int32_t>(offsetof(PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7, ___appearDelay_11)); }
	inline float get_appearDelay_11() const { return ___appearDelay_11; }
	inline float* get_address_of_appearDelay_11() { return &___appearDelay_11; }
	inline void set_appearDelay_11(float value)
	{
		___appearDelay_11 = value;
	}

	inline static int32_t get_offset_of_vanishDelay_12() { return static_cast<int32_t>(offsetof(PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7, ___vanishDelay_12)); }
	inline float get_vanishDelay_12() const { return ___vanishDelay_12; }
	inline float* get_address_of_vanishDelay_12() { return &___vanishDelay_12; }
	inline void set_vanishDelay_12(float value)
	{
		___vanishDelay_12 = value;
	}

	inline static int32_t get_offset_of_lifetime_13() { return static_cast<int32_t>(offsetof(PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7, ___lifetime_13)); }
	inline float get_lifetime_13() const { return ___lifetime_13; }
	inline float* get_address_of_lifetime_13() { return &___lifetime_13; }
	inline void set_lifetime_13(float value)
	{
		___lifetime_13 = value;
	}

	inline static int32_t get_offset_of_keepWorldRotation_14() { return static_cast<int32_t>(offsetof(PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7, ___keepWorldRotation_14)); }
	inline bool get_keepWorldRotation_14() const { return ___keepWorldRotation_14; }
	inline bool* get_address_of_keepWorldRotation_14() { return &___keepWorldRotation_14; }
	inline void set_keepWorldRotation_14(bool value)
	{
		___keepWorldRotation_14 = value;
	}

	inline static int32_t get_offset_of_focusEnterTime_15() { return static_cast<int32_t>(offsetof(PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7, ___focusEnterTime_15)); }
	inline float get_focusEnterTime_15() const { return ___focusEnterTime_15; }
	inline float* get_address_of_focusEnterTime_15() { return &___focusEnterTime_15; }
	inline void set_focusEnterTime_15(float value)
	{
		___focusEnterTime_15 = value;
	}

	inline static int32_t get_offset_of_focusExitTime_16() { return static_cast<int32_t>(offsetof(PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7, ___focusExitTime_16)); }
	inline float get_focusExitTime_16() const { return ___focusExitTime_16; }
	inline float* get_address_of_focusExitTime_16() { return &___focusExitTime_16; }
	inline void set_focusExitTime_16(float value)
	{
		___focusExitTime_16 = value;
	}

	inline static int32_t get_offset_of_tappedTime_17() { return static_cast<int32_t>(offsetof(PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7, ___tappedTime_17)); }
	inline float get_tappedTime_17() const { return ___tappedTime_17; }
	inline float* get_address_of_tappedTime_17() { return &___tappedTime_17; }
	inline void set_tappedTime_17(float value)
	{
		___tappedTime_17 = value;
	}

	inline static int32_t get_offset_of_spawnable_18() { return static_cast<int32_t>(offsetof(PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7, ___spawnable_18)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_spawnable_18() const { return ___spawnable_18; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_spawnable_18() { return &___spawnable_18; }
	inline void set_spawnable_18(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___spawnable_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spawnable_18), (void*)value);
	}
};


// Microsoft.MixedReality.Toolkit.Experimental.UI.StepSlider
struct  StepSlider_tD03ECCB31FD5B24E2C0E2C3B981B89C840C39ACB  : public PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.Experimental.UI.StepSlider::sliderStepDivisions
	int32_t ___sliderStepDivisions_23;
	// System.Single Microsoft.MixedReality.Toolkit.Experimental.UI.StepSlider::sliderStepVal
	float ___sliderStepVal_24;

public:
	inline static int32_t get_offset_of_sliderStepDivisions_23() { return static_cast<int32_t>(offsetof(StepSlider_tD03ECCB31FD5B24E2C0E2C3B981B89C840C39ACB, ___sliderStepDivisions_23)); }
	inline int32_t get_sliderStepDivisions_23() const { return ___sliderStepDivisions_23; }
	inline int32_t* get_address_of_sliderStepDivisions_23() { return &___sliderStepDivisions_23; }
	inline void set_sliderStepDivisions_23(int32_t value)
	{
		___sliderStepDivisions_23 = value;
	}

	inline static int32_t get_offset_of_sliderStepVal_24() { return static_cast<int32_t>(offsetof(StepSlider_tD03ECCB31FD5B24E2C0E2C3B981B89C840C39ACB, ___sliderStepVal_24)); }
	inline float get_sliderStepVal_24() const { return ___sliderStepVal_24; }
	inline float* get_address_of_sliderStepVal_24() { return &___sliderStepVal_24; }
	inline void set_sliderStepVal_24(float value)
	{
		___sliderStepVal_24 = value;
	}
};


// Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner
struct  ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447  : public PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7
{
public:
	// Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner/SettingsMode Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner::settingsMode
	int32_t ___settingsMode_19;
	// System.Boolean Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner::showBackground
	bool ___showBackground_20;
	// System.Boolean Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner::showOutline
	bool ___showOutline_21;
	// System.Boolean Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner::showConnector
	bool ___showConnector_22;
	// Microsoft.MixedReality.Toolkit.UI.ConnectorFollowType Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner::followType
	int32_t ___followType_23;
	// Microsoft.MixedReality.Toolkit.UI.ConnectorPivotMode Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner::pivotMode
	int32_t ___pivotMode_24;
	// Microsoft.MixedReality.Toolkit.UI.ConnectorPivotDirection Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner::pivotDirection
	int32_t ___pivotDirection_25;
	// Microsoft.MixedReality.Toolkit.UI.ConnectorOrientType Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner::pivotDirectionOrient
	int32_t ___pivotDirectionOrient_26;
	// UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner::manualPivotDirection
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___manualPivotDirection_27;
	// UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner::manualPivotLocalPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___manualPivotLocalPosition_28;
	// System.Single Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner::pivotDistance
	float ___pivotDistance_29;
	// System.String Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner::toolTipText
	String_t* ___toolTipText_30;
	// UnityEngine.Transform Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner::anchor
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___anchor_31;

public:
	inline static int32_t get_offset_of_settingsMode_19() { return static_cast<int32_t>(offsetof(ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447, ___settingsMode_19)); }
	inline int32_t get_settingsMode_19() const { return ___settingsMode_19; }
	inline int32_t* get_address_of_settingsMode_19() { return &___settingsMode_19; }
	inline void set_settingsMode_19(int32_t value)
	{
		___settingsMode_19 = value;
	}

	inline static int32_t get_offset_of_showBackground_20() { return static_cast<int32_t>(offsetof(ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447, ___showBackground_20)); }
	inline bool get_showBackground_20() const { return ___showBackground_20; }
	inline bool* get_address_of_showBackground_20() { return &___showBackground_20; }
	inline void set_showBackground_20(bool value)
	{
		___showBackground_20 = value;
	}

	inline static int32_t get_offset_of_showOutline_21() { return static_cast<int32_t>(offsetof(ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447, ___showOutline_21)); }
	inline bool get_showOutline_21() const { return ___showOutline_21; }
	inline bool* get_address_of_showOutline_21() { return &___showOutline_21; }
	inline void set_showOutline_21(bool value)
	{
		___showOutline_21 = value;
	}

	inline static int32_t get_offset_of_showConnector_22() { return static_cast<int32_t>(offsetof(ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447, ___showConnector_22)); }
	inline bool get_showConnector_22() const { return ___showConnector_22; }
	inline bool* get_address_of_showConnector_22() { return &___showConnector_22; }
	inline void set_showConnector_22(bool value)
	{
		___showConnector_22 = value;
	}

	inline static int32_t get_offset_of_followType_23() { return static_cast<int32_t>(offsetof(ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447, ___followType_23)); }
	inline int32_t get_followType_23() const { return ___followType_23; }
	inline int32_t* get_address_of_followType_23() { return &___followType_23; }
	inline void set_followType_23(int32_t value)
	{
		___followType_23 = value;
	}

	inline static int32_t get_offset_of_pivotMode_24() { return static_cast<int32_t>(offsetof(ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447, ___pivotMode_24)); }
	inline int32_t get_pivotMode_24() const { return ___pivotMode_24; }
	inline int32_t* get_address_of_pivotMode_24() { return &___pivotMode_24; }
	inline void set_pivotMode_24(int32_t value)
	{
		___pivotMode_24 = value;
	}

	inline static int32_t get_offset_of_pivotDirection_25() { return static_cast<int32_t>(offsetof(ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447, ___pivotDirection_25)); }
	inline int32_t get_pivotDirection_25() const { return ___pivotDirection_25; }
	inline int32_t* get_address_of_pivotDirection_25() { return &___pivotDirection_25; }
	inline void set_pivotDirection_25(int32_t value)
	{
		___pivotDirection_25 = value;
	}

	inline static int32_t get_offset_of_pivotDirectionOrient_26() { return static_cast<int32_t>(offsetof(ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447, ___pivotDirectionOrient_26)); }
	inline int32_t get_pivotDirectionOrient_26() const { return ___pivotDirectionOrient_26; }
	inline int32_t* get_address_of_pivotDirectionOrient_26() { return &___pivotDirectionOrient_26; }
	inline void set_pivotDirectionOrient_26(int32_t value)
	{
		___pivotDirectionOrient_26 = value;
	}

	inline static int32_t get_offset_of_manualPivotDirection_27() { return static_cast<int32_t>(offsetof(ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447, ___manualPivotDirection_27)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_manualPivotDirection_27() const { return ___manualPivotDirection_27; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_manualPivotDirection_27() { return &___manualPivotDirection_27; }
	inline void set_manualPivotDirection_27(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___manualPivotDirection_27 = value;
	}

	inline static int32_t get_offset_of_manualPivotLocalPosition_28() { return static_cast<int32_t>(offsetof(ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447, ___manualPivotLocalPosition_28)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_manualPivotLocalPosition_28() const { return ___manualPivotLocalPosition_28; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_manualPivotLocalPosition_28() { return &___manualPivotLocalPosition_28; }
	inline void set_manualPivotLocalPosition_28(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___manualPivotLocalPosition_28 = value;
	}

	inline static int32_t get_offset_of_pivotDistance_29() { return static_cast<int32_t>(offsetof(ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447, ___pivotDistance_29)); }
	inline float get_pivotDistance_29() const { return ___pivotDistance_29; }
	inline float* get_address_of_pivotDistance_29() { return &___pivotDistance_29; }
	inline void set_pivotDistance_29(float value)
	{
		___pivotDistance_29 = value;
	}

	inline static int32_t get_offset_of_toolTipText_30() { return static_cast<int32_t>(offsetof(ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447, ___toolTipText_30)); }
	inline String_t* get_toolTipText_30() const { return ___toolTipText_30; }
	inline String_t** get_address_of_toolTipText_30() { return &___toolTipText_30; }
	inline void set_toolTipText_30(String_t* value)
	{
		___toolTipText_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toolTipText_30), (void*)value);
	}

	inline static int32_t get_offset_of_anchor_31() { return static_cast<int32_t>(offsetof(ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447, ___anchor_31)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_anchor_31() const { return ___anchor_31; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_anchor_31() { return &___anchor_31; }
	inline void set_anchor_31(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___anchor_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anchor_31), (void*)value);
	}
};


// TooltipSpawner
struct  TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE  : public ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447
{
public:
	// System.Action`1<TooltipSpawner> TooltipSpawner::Tapped
	Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * ___Tapped_32;
	// System.Boolean TooltipSpawner::IsActive
	bool ___IsActive_33;
	// System.Boolean TooltipSpawner::CanActive
	bool ___CanActive_34;
	// UnityEngine.Transform TooltipSpawner::anchorTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___anchorTransform_35;
	// UnityEngine.Transform TooltipSpawner::parentTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parentTransform_36;
	// System.Single TooltipSpawner::sizeScale
	float ___sizeScale_37;

public:
	inline static int32_t get_offset_of_Tapped_32() { return static_cast<int32_t>(offsetof(TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE, ___Tapped_32)); }
	inline Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * get_Tapped_32() const { return ___Tapped_32; }
	inline Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 ** get_address_of_Tapped_32() { return &___Tapped_32; }
	inline void set_Tapped_32(Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * value)
	{
		___Tapped_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Tapped_32), (void*)value);
	}

	inline static int32_t get_offset_of_IsActive_33() { return static_cast<int32_t>(offsetof(TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE, ___IsActive_33)); }
	inline bool get_IsActive_33() const { return ___IsActive_33; }
	inline bool* get_address_of_IsActive_33() { return &___IsActive_33; }
	inline void set_IsActive_33(bool value)
	{
		___IsActive_33 = value;
	}

	inline static int32_t get_offset_of_CanActive_34() { return static_cast<int32_t>(offsetof(TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE, ___CanActive_34)); }
	inline bool get_CanActive_34() const { return ___CanActive_34; }
	inline bool* get_address_of_CanActive_34() { return &___CanActive_34; }
	inline void set_CanActive_34(bool value)
	{
		___CanActive_34 = value;
	}

	inline static int32_t get_offset_of_anchorTransform_35() { return static_cast<int32_t>(offsetof(TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE, ___anchorTransform_35)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_anchorTransform_35() const { return ___anchorTransform_35; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_anchorTransform_35() { return &___anchorTransform_35; }
	inline void set_anchorTransform_35(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___anchorTransform_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anchorTransform_35), (void*)value);
	}

	inline static int32_t get_offset_of_parentTransform_36() { return static_cast<int32_t>(offsetof(TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE, ___parentTransform_36)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_parentTransform_36() const { return ___parentTransform_36; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_parentTransform_36() { return &___parentTransform_36; }
	inline void set_parentTransform_36(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___parentTransform_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parentTransform_36), (void*)value);
	}

	inline static int32_t get_offset_of_sizeScale_37() { return static_cast<int32_t>(offsetof(TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE, ___sizeScale_37)); }
	inline float get_sizeScale_37() const { return ___sizeScale_37; }
	inline float* get_address_of_sizeScale_37() { return &___sizeScale_37; }
	inline void set_sizeScale_37(float value)
	{
		___sizeScale_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif


// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mA671E933C9D3DAE4E3F71D34FDDA971739618158_gshared (Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::Invoke(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1_Invoke_m587509C88BB83721D7918D89DF07606BB752D744_gshared (Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);

// System.Void AnchorsManager::AllAnchorsTap()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnchorsManager_AllAnchorsTap_mA4F92305136FA54EDF0A19EBB964E49C247D8402 (AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void AnchorsManager::SubscribeOnEventAnchors()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnchorsManager_SubscribeOnEventAnchors_m0074851CA3D90C97A4411AAD0D39F6CE42D8D37C (AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<TooltipSpawner>::GetEnumerator()
inline Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900  List_1_GetEnumerator_mDE06CDC58BB81D52E19406DF046B000078C00AEC (List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900  (*) (List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2 *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<TooltipSpawner>::get_Current()
inline TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * Enumerator_get_Current_m4FCC4BC0A3A876E6D9494F53E7574EDA93EB3401_inline (Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900 * __this, const RuntimeMethod* method)
{
	return ((  TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * (*) (Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Void TooltipSpawner::ChangeState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipSpawner_ChangeState_m87A3F1566826F2DB0C9F69CBA8132B75240D2AC9 (TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<TooltipSpawner>::MoveNext()
inline bool Enumerator_MoveNext_mE3AC4D65283C491DD7D72CB75834911030B8D401 (Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<TooltipSpawner>::Dispose()
inline void Enumerator_Dispose_mD643E85EDAC4C30B2A8311E2BF006DA050EDEFFB (Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900 *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.Void System.Action`1<TooltipSpawner>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m667E5E34007C1D87D8312377ABC92E72E1DDDCB9 (Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mA671E933C9D3DAE4E3F71D34FDDA971739618158_gshared)(__this, ___object0, ___method1, method);
}
// System.Void TooltipSpawner::add_Tapped(System.Action`1<TooltipSpawner>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipSpawner_add_Tapped_mE772ACF3E46C85E8B4098A0953DC7715277FFC5F (TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * __this, Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void TooltipSpawner::Tap()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipSpawner_Tap_mDFA9C2D75152A9CC0B8D1D798EE8093817D2BC16 (TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_eulerAngles_mCF1E10C36ED1F03804A1D10A9BAB272E0EA8766F (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single,UnityEngine.Space)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_mE77655C011C18F49CAD740CED7940EF1C7000357 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, float ___xAngle0, float ___yAngle1, float ___zAngle2, int32_t ___relativeTo3, const RuntimeMethod* method);
// System.Void AnimationWindTurbine::Animate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationWindTurbine_Animate_m0ABDDA2D69816A58BC2769EC7FA6FD1D5D783F14 (AnimationWindTurbine_t4BF2E55E0F8593B7F88AA65AA3303BE55D302062 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<PartOfWind>::GetEnumerator()
inline Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006  List_1_GetEnumerator_m15901002F3A52C49BF62161FED450C3A6B9CA7B5 (List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006  (*) (List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<PartOfWind>::get_Current()
inline PartOfWind_t675167C1ED83369524C5CDE22EEB3CBFF95869E2 * Enumerator_get_Current_m169AEC581C3039A4FE73767BC83B5191827B7B13_inline (Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006 * __this, const RuntimeMethod* method)
{
	return ((  PartOfWind_t675167C1ED83369524C5CDE22EEB3CBFF95869E2 * (*) (Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Single Microsoft.MixedReality.Toolkit.UI.PinchSlider::get_SliderValue()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float PinchSlider_get_SliderValue_m44C37EBD34DD63E6FBB6EEF83F8F52AD88E0D600_inline (PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB * __this, const RuntimeMethod* method);
// System.Void PartOfWind::ChangePosition(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PartOfWind_ChangePosition_mC74BF7EA14CD257939991A4F213C8E369EEFBD9F (PartOfWind_t675167C1ED83369524C5CDE22EEB3CBFF95869E2 * __this, float ___value0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<PartOfWind>::MoveNext()
inline bool Enumerator_MoveNext_m0147E66892FD97D8307DB5E727B5177A22E582DD (Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<PartOfWind>::Dispose()
inline void Enumerator_Dispose_mEB6F59307D7B220604F8BF85B5E403C6CB8EB5B8 (Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006 *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55 (Delegate_t * ___a0, Delegate_t * ___b1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4 (Delegate_t * ___source0, Delegate_t * ___value1, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.UI.PrefabSpawner::HandleTap()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PrefabSpawner_HandleTap_mE4D022D7F25B1C19BA9EAA92CA64FE8F93FFD179 (PrefabSpawner_t32E9BA303546B91D131C95C57CB2B33B6238E9F7 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<TooltipSpawner>::Invoke(!0)
inline void Action_1_Invoke_mB325466CD8F2E6D4ECD81C594DC95027469165AC (Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * __this, TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 *, TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE *, const RuntimeMethod*))Action_1_Invoke_m587509C88BB83721D7918D89DF07606BB752D744_gshared)(__this, ___obj0, method);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline (float ___d0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToolTipSpawner__ctor_m6F9E2CCD2106F582CD1A2CF59CD740C96B4EBC12 (ToolTipSpawner_t0F2B425635400B35C16DF77CD40646B173789447 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C (float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AnchorsManager::ChangeStateAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnchorsManager_ChangeStateAll_mD15B5C9DAEF00229FC0B69D150E56F1A2855EEE1 (AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC * __this, const RuntimeMethod* method)
{
	{
		// isAll = !isAll;
		bool L_0 = __this->get_isAll_7();
		__this->set_isAll_7((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		// AllAnchorsTap();
		AnchorsManager_AllAnchorsTap_mA4F92305136FA54EDF0A19EBB964E49C247D8402(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AnchorsManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnchorsManager_Awake_m998C316C90A5BFC3B413F0DCDD2B9E7D1864F72F (AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Instance == null)
		AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC * L_0 = ((AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC_StaticFields*)il2cpp_codegen_static_fields_for(AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC_il2cpp_TypeInfo_var))->get_Instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		// Instance = this;
		((AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC_StaticFields*)il2cpp_codegen_static_fields_for(AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC_il2cpp_TypeInfo_var))->set_Instance_4(__this);
	}

IL_0013:
	{
		// activeAnchorTooltip = null;
		__this->set_activeAnchorTooltip_6((TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE *)NULL);
		// }
		return;
	}
}
// System.Void AnchorsManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnchorsManager_Start_m54FAFAC841140314D22F7BD34227F9DB3744ADA8 (AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC * __this, const RuntimeMethod* method)
{
	{
		// SubscribeOnEventAnchors();
		AnchorsManager_SubscribeOnEventAnchors_m0074851CA3D90C97A4411AAD0D39F6CE42D8D37C(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AnchorsManager::AllAnchorsTap()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnchorsManager_AllAnchorsTap_mA4F92305136FA54EDF0A19EBB964E49C247D8402 (AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mD643E85EDAC4C30B2A8311E2BF006DA050EDEFFB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mE3AC4D65283C491DD7D72CB75834911030B8D401_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m4FCC4BC0A3A876E6D9494F53E7574EDA93EB3401_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mDE06CDC58BB81D52E19406DF046B000078C00AEC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// foreach (var anchor in anchors)
		List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2 * L_0 = __this->get_anchors_5();
		NullCheck(L_0);
		Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900  L_1;
		L_1 = List_1_GetEnumerator_mDE06CDC58BB81D52E19406DF046B000078C00AEC(L_0, /*hidden argument*/List_1_GetEnumerator_mDE06CDC58BB81D52E19406DF046B000078C00AEC_RuntimeMethod_var);
		V_0 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0029;
		}

IL_000e:
		{
			// foreach (var anchor in anchors)
			TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * L_2;
			L_2 = Enumerator_get_Current_m4FCC4BC0A3A876E6D9494F53E7574EDA93EB3401_inline((Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900 *)(&V_0), /*hidden argument*/Enumerator_get_Current_m4FCC4BC0A3A876E6D9494F53E7574EDA93EB3401_RuntimeMethod_var);
			// anchor.ChangeState();
			TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * L_3 = L_2;
			NullCheck(L_3);
			TooltipSpawner_ChangeState_m87A3F1566826F2DB0C9F69CBA8132B75240D2AC9(L_3, /*hidden argument*/NULL);
			// anchor.CanActive = !isAll;
			bool L_4 = __this->get_isAll_7();
			NullCheck(L_3);
			L_3->set_CanActive_34((bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0));
		}

IL_0029:
		{
			// foreach (var anchor in anchors)
			bool L_5;
			L_5 = Enumerator_MoveNext_mE3AC4D65283C491DD7D72CB75834911030B8D401((Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mE3AC4D65283C491DD7D72CB75834911030B8D401_RuntimeMethod_var);
			if (L_5)
			{
				goto IL_000e;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x42, FINALLY_0034);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mD643E85EDAC4C30B2A8311E2BF006DA050EDEFFB((Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900 *)(&V_0), /*hidden argument*/Enumerator_Dispose_mD643E85EDAC4C30B2A8311E2BF006DA050EDEFFB_RuntimeMethod_var);
		IL2CPP_END_FINALLY(52)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x42, IL_0042)
	}

IL_0042:
	{
		// activeAnchorTooltip = null;
		__this->set_activeAnchorTooltip_6((TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE *)NULL);
		// }
		return;
	}
}
// System.Void AnchorsManager::SubscribeOnEventAnchors()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnchorsManager_SubscribeOnEventAnchors_m0074851CA3D90C97A4411AAD0D39F6CE42D8D37C (AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m667E5E34007C1D87D8312377ABC92E72E1DDDCB9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnchorsManager_ChangeActiveAnchor_mA4FBD1A34D1A68C468A15406D7EA691C32DE64C2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mD643E85EDAC4C30B2A8311E2BF006DA050EDEFFB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mE3AC4D65283C491DD7D72CB75834911030B8D401_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m4FCC4BC0A3A876E6D9494F53E7574EDA93EB3401_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mDE06CDC58BB81D52E19406DF046B000078C00AEC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// foreach (var anchor in anchors)
		List_1_tBBA4D8FF544E9C44749263F06BF2BA38D7D28CB2 * L_0 = __this->get_anchors_5();
		NullCheck(L_0);
		Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900  L_1;
		L_1 = List_1_GetEnumerator_mDE06CDC58BB81D52E19406DF046B000078C00AEC(L_0, /*hidden argument*/List_1_GetEnumerator_mDE06CDC58BB81D52E19406DF046B000078C00AEC_RuntimeMethod_var);
		V_0 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_000e:
		{
			// foreach (var anchor in anchors)
			TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * L_2;
			L_2 = Enumerator_get_Current_m4FCC4BC0A3A876E6D9494F53E7574EDA93EB3401_inline((Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900 *)(&V_0), /*hidden argument*/Enumerator_get_Current_m4FCC4BC0A3A876E6D9494F53E7574EDA93EB3401_RuntimeMethod_var);
			// anchor.Tapped += ChangeActiveAnchor;
			Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_3 = (Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 *)il2cpp_codegen_object_new(Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5_il2cpp_TypeInfo_var);
			Action_1__ctor_m667E5E34007C1D87D8312377ABC92E72E1DDDCB9(L_3, __this, (intptr_t)((intptr_t)AnchorsManager_ChangeActiveAnchor_mA4FBD1A34D1A68C468A15406D7EA691C32DE64C2_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m667E5E34007C1D87D8312377ABC92E72E1DDDCB9_RuntimeMethod_var);
			NullCheck(L_2);
			TooltipSpawner_add_Tapped_mE772ACF3E46C85E8B4098A0953DC7715277FFC5F(L_2, L_3, /*hidden argument*/NULL);
		}

IL_0026:
		{
			// foreach (var anchor in anchors)
			bool L_4;
			L_4 = Enumerator_MoveNext_mE3AC4D65283C491DD7D72CB75834911030B8D401((Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mE3AC4D65283C491DD7D72CB75834911030B8D401_RuntimeMethod_var);
			if (L_4)
			{
				goto IL_000e;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x3F, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mD643E85EDAC4C30B2A8311E2BF006DA050EDEFFB((Enumerator_tB9490998167C6EB3BCBAC32703CC6BDBA8E28900 *)(&V_0), /*hidden argument*/Enumerator_Dispose_mD643E85EDAC4C30B2A8311E2BF006DA050EDEFFB_RuntimeMethod_var);
		IL2CPP_END_FINALLY(49)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
	}

IL_003f:
	{
		// }
		return;
	}
}
// System.Void AnchorsManager::ChangeActiveAnchor(TooltipSpawner)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnchorsManager_ChangeActiveAnchor_mA4FBD1A34D1A68C468A15406D7EA691C32DE64C2 (AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC * __this, TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * ___anchorTooltip0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC * G_B7_0 = NULL;
	AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC * G_B6_0 = NULL;
	TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * G_B8_0 = NULL;
	AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC * G_B8_1 = NULL;
	{
		// if (isAll)
		bool L_0 = __this->get_isAll_7();
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// if (activeAnchorTooltip != null)
		TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * L_1 = __this->get_activeAnchorTooltip_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0048;
		}
	}
	{
		// if(activeAnchorTooltip.IsActive)
		TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * L_3 = __this->get_activeAnchorTooltip_6();
		NullCheck(L_3);
		bool L_4 = L_3->get_IsActive_33();
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		// activeAnchorTooltip.Tap();
		TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * L_5 = __this->get_activeAnchorTooltip_6();
		NullCheck(L_5);
		TooltipSpawner_Tap_mDFA9C2D75152A9CC0B8D1D798EE8093817D2BC16(L_5, /*hidden argument*/NULL);
	}

IL_002f:
	{
		// activeAnchorTooltip = activeAnchorTooltip == anchorTooltip ? null : anchorTooltip;
		TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * L_6 = __this->get_activeAnchorTooltip_6();
		TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * L_7 = ___anchorTooltip0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_6, L_7, /*hidden argument*/NULL);
		G_B6_0 = __this;
		if (L_8)
		{
			G_B7_0 = __this;
			goto IL_0041;
		}
	}
	{
		TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * L_9 = ___anchorTooltip0;
		G_B8_0 = L_9;
		G_B8_1 = G_B6_0;
		goto IL_0042;
	}

IL_0041:
	{
		G_B8_0 = ((TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE *)(NULL));
		G_B8_1 = G_B7_0;
	}

IL_0042:
	{
		NullCheck(G_B8_1);
		G_B8_1->set_activeAnchorTooltip_6(G_B8_0);
		// }
		return;
	}

IL_0048:
	{
		// activeAnchorTooltip = anchorTooltip;
		TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * L_10 = ___anchorTooltip0;
		__this->set_activeAnchorTooltip_6(L_10);
		// }
		return;
	}
}
// System.Void AnchorsManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnchorsManager__ctor_mFC337F78F0246302DD8D670D81E4ECF0160BBC43 (AnchorsManager_t0293109C7290322ADA8435F27BC761AB0B502BFC * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AnimationWindTurbine::ToggleAnimation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationWindTurbine_ToggleAnimation_m9ADB35EDB97DB6BA9854738BCE55AE421F692A40 (AnimationWindTurbine_t4BF2E55E0F8593B7F88AA65AA3303BE55D302062 * __this, const RuntimeMethod* method)
{
	{
		// isAnimated = !isAnimated;
		bool L_0 = __this->get_isAnimated_5();
		__this->set_isAnimated_5((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		// }
		return;
	}
}
// System.Void AnimationWindTurbine::Animate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationWindTurbine_Animate_m0ABDDA2D69816A58BC2769EC7FA6FD1D5D783F14 (AnimationWindTurbine_t4BF2E55E0F8593B7F88AA65AA3303BE55D302062 * __this, const RuntimeMethod* method)
{
	{
		// var eulers = blades.transform.eulerAngles;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_blades_4();
		NullCheck(L_0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_eulerAngles_mCF1E10C36ED1F03804A1D10A9BAB272E0EA8766F(L_1, /*hidden argument*/NULL);
		// xAngles += speed * Time.deltaTime;
		float L_3 = __this->get_xAngles_7();
		float L_4 = __this->get_speed_6();
		float L_5;
		L_5 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_xAngles_7(((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)))));
		// blades.transform.Rotate(0, speed * Time.deltaTime, 0, Space.Self);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = __this->get_blades_4();
		NullCheck(L_6);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_6, /*hidden argument*/NULL);
		float L_8 = __this->get_speed_6();
		float L_9;
		L_9 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_Rotate_mE77655C011C18F49CAD740CED7940EF1C7000357(L_7, (0.0f), ((float)il2cpp_codegen_multiply((float)L_8, (float)L_9)), (0.0f), 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AnimationWindTurbine::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationWindTurbine_Awake_m1F9DE8734EE59144A9CD695318FD1EE0BA0396D9 (AnimationWindTurbine_t4BF2E55E0F8593B7F88AA65AA3303BE55D302062 * __this, const RuntimeMethod* method)
{
	{
		// isAnimated = false;
		__this->set_isAnimated_5((bool)0);
		// }
		return;
	}
}
// System.Void AnimationWindTurbine::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationWindTurbine_Update_m3F1927A1B65B1B63CBEA1E07379D3857198F71FA (AnimationWindTurbine_t4BF2E55E0F8593B7F88AA65AA3303BE55D302062 * __this, const RuntimeMethod* method)
{
	{
		// if(isAnimated)
		bool L_0 = __this->get_isAnimated_5();
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		// Animate();
		AnimationWindTurbine_Animate_m0ABDDA2D69816A58BC2769EC7FA6FD1D5D783F14(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		// }
		return;
	}
}
// System.Void AnimationWindTurbine::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationWindTurbine__ctor_mFFFA2F5ED8BEFEA28F1A12FFC6D8220C3321F992 (AnimationWindTurbine_t4BF2E55E0F8593B7F88AA65AA3303BE55D302062 * __this, const RuntimeMethod* method)
{
	{
		// [SerializeField] private float speed = 9f;
		__this->set_speed_6((9.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ParseWind::OnChangeSlider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParseWind_OnChangeSlider_mCB56CDC37CBF3A50C793FD7F8945D162EFC5D27C (ParseWind_t4ABEBB3AABE820B96DD97941E446E648C8AD516C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mEB6F59307D7B220604F8BF85B5E403C6CB8EB5B8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m0147E66892FD97D8307DB5E727B5177A22E582DD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m169AEC581C3039A4FE73767BC83B5191827B7B13_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m15901002F3A52C49BF62161FED450C3A6B9CA7B5_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// foreach (var part in parts)
		List_1_t58405BB2F8822A176368945F201E343AA9AE6CCD * L_0 = __this->get_parts_5();
		NullCheck(L_0);
		Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006  L_1;
		L_1 = List_1_GetEnumerator_m15901002F3A52C49BF62161FED450C3A6B9CA7B5(L_0, /*hidden argument*/List_1_GetEnumerator_m15901002F3A52C49BF62161FED450C3A6B9CA7B5_RuntimeMethod_var);
		V_0 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0025;
		}

IL_000e:
		{
			// foreach (var part in parts)
			PartOfWind_t675167C1ED83369524C5CDE22EEB3CBFF95869E2 * L_2;
			L_2 = Enumerator_get_Current_m169AEC581C3039A4FE73767BC83B5191827B7B13_inline((Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006 *)(&V_0), /*hidden argument*/Enumerator_get_Current_m169AEC581C3039A4FE73767BC83B5191827B7B13_RuntimeMethod_var);
			// part.ChangePosition(slider.SliderValue);
			StepSlider_tD03ECCB31FD5B24E2C0E2C3B981B89C840C39ACB * L_3 = __this->get_slider_4();
			NullCheck(L_3);
			float L_4;
			L_4 = PinchSlider_get_SliderValue_m44C37EBD34DD63E6FBB6EEF83F8F52AD88E0D600_inline(L_3, /*hidden argument*/NULL);
			NullCheck(L_2);
			PartOfWind_ChangePosition_mC74BF7EA14CD257939991A4F213C8E369EEFBD9F(L_2, L_4, /*hidden argument*/NULL);
		}

IL_0025:
		{
			// foreach (var part in parts)
			bool L_5;
			L_5 = Enumerator_MoveNext_m0147E66892FD97D8307DB5E727B5177A22E582DD((Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_m0147E66892FD97D8307DB5E727B5177A22E582DD_RuntimeMethod_var);
			if (L_5)
			{
				goto IL_000e;
			}
		}

IL_002e:
		{
			IL2CPP_LEAVE(0x3E, FINALLY_0030);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mEB6F59307D7B220604F8BF85B5E403C6CB8EB5B8((Enumerator_tD76F7F2B001797A5B4AC038BBBC3BF96D8DC6006 *)(&V_0), /*hidden argument*/Enumerator_Dispose_mEB6F59307D7B220604F8BF85B5E403C6CB8EB5B8_RuntimeMethod_var);
		IL2CPP_END_FINALLY(48)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x3E, IL_003e)
	}

IL_003e:
	{
		// }
		return;
	}
}
// System.Void ParseWind::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParseWind__ctor_m78A0136F82E8E27607D96C94A261624FF90B9635 (ParseWind_t4ABEBB3AABE820B96DD97941E446E648C8AD516C * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PartOfWind::ChangePosition(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PartOfWind_ChangePosition_mC74BF7EA14CD257939991A4F213C8E369EEFBD9F (PartOfWind_t675167C1ED83369524C5CDE22EEB3CBFF95869E2 * __this, float ___value0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// var start = startPosition.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_startPosition_4();
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// var end = endPosition.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = __this->get_endPosition_5();
		NullCheck(L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		// transform.position = Vector3.Lerp(start, end, value);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = V_1;
		float L_7 = ___value0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(L_5, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_4, L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PartOfWind::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PartOfWind__ctor_m10C8BC534CA744F4F4223FAA565CFF34C96AFAC6 (PartOfWind_t675167C1ED83369524C5CDE22EEB3CBFF95869E2 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TooltipSpawner::add_Tapped(System.Action`1<TooltipSpawner>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipSpawner_add_Tapped_mE772ACF3E46C85E8B4098A0953DC7715277FFC5F (TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * __this, Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * V_0 = NULL;
	Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * V_1 = NULL;
	Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * V_2 = NULL;
	{
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_0 = __this->get_Tapped_32();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_1 = V_0;
		V_1 = L_1;
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_2 = V_1;
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 *)CastclassSealed((RuntimeObject*)L_4, Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5_il2cpp_TypeInfo_var));
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 ** L_5 = __this->get_address_of_Tapped_32();
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_6 = V_2;
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_7 = V_1;
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_8;
		L_8 = InterlockedCompareExchangeImpl<Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 *>((Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 **)L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_9 = V_0;
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 *)L_9) == ((RuntimeObject*)(Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TooltipSpawner::remove_Tapped(System.Action`1<TooltipSpawner>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipSpawner_remove_Tapped_mF6EC004071BA6B3CC042FA9DA05DC865DE4EB62B (TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * __this, Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * V_0 = NULL;
	Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * V_1 = NULL;
	Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * V_2 = NULL;
	{
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_0 = __this->get_Tapped_32();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_1 = V_0;
		V_1 = L_1;
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_2 = V_1;
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 *)CastclassSealed((RuntimeObject*)L_4, Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5_il2cpp_TypeInfo_var));
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 ** L_5 = __this->get_address_of_Tapped_32();
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_6 = V_2;
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_7 = V_1;
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_8;
		L_8 = InterlockedCompareExchangeImpl<Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 *>((Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 **)L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_9 = V_0;
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 *)L_9) == ((RuntimeObject*)(Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TooltipSpawner::ChangeState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipSpawner_ChangeState_m87A3F1566826F2DB0C9F69CBA8132B75240D2AC9 (TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * __this, const RuntimeMethod* method)
{
	{
		// if(!IsActive)
		bool L_0 = __this->get_IsActive_33();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// base.HandleTap();
		PrefabSpawner_HandleTap_mE4D022D7F25B1C19BA9EAA92CA64FE8F93FFD179(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		// IsActive = false;
		__this->set_IsActive_33((bool)0);
		// }
		return;
	}
}
// System.Void TooltipSpawner::Tap()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipSpawner_Tap_mDFA9C2D75152A9CC0B8D1D798EE8093817D2BC16 (TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * __this, const RuntimeMethod* method)
{
	{
		// if (!CanActive)
		bool L_0 = __this->get_CanActive_34();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// base.HandleTap();
		PrefabSpawner_HandleTap_mE4D022D7F25B1C19BA9EAA92CA64FE8F93FFD179(__this, /*hidden argument*/NULL);
		// IsActive = !IsActive;
		bool L_1 = __this->get_IsActive_33();
		__this->set_IsActive_33((bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0));
		// }
		return;
	}
}
// System.Void TooltipSpawner::HandleTap()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipSpawner_HandleTap_m7F4509FC63025A814839006EFFD9620408CF8F19 (TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_Invoke_mB325466CD8F2E6D4ECD81C594DC95027469165AC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * G_B4_0 = NULL;
	Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * G_B3_0 = NULL;
	{
		// if (!CanActive)
		bool L_0 = __this->get_CanActive_34();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// base.HandleTap();
		PrefabSpawner_HandleTap_mE4D022D7F25B1C19BA9EAA92CA64FE8F93FFD179(__this, /*hidden argument*/NULL);
		// IsActive = !IsActive;
		bool L_1 = __this->get_IsActive_33();
		__this->set_IsActive_33((bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0));
		// Tapped?.Invoke(this);
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_2 = __this->get_Tapped_32();
		Action_1_tC3CF22E20FDCF9EA9CFB6269D0BF3C320000B8B5 * L_3 = L_2;
		G_B3_0 = L_3;
		if (L_3)
		{
			G_B4_0 = L_3;
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		NullCheck(G_B4_0);
		Action_1_Invoke_mB325466CD8F2E6D4ECD81C594DC95027469165AC(G_B4_0, __this, /*hidden argument*/Action_1_Invoke_mB325466CD8F2E6D4ECD81C594DC95027469165AC_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void TooltipSpawner::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipSpawner_Awake_m61F616FF5759F13C81D5FD823675B8EEF52EC534 (TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * __this, const RuntimeMethod* method)
{
	{
		// Tap();
		TooltipSpawner_Tap_mDFA9C2D75152A9CC0B8D1D798EE8093817D2BC16(__this, /*hidden argument*/NULL);
		// Tap();
		TooltipSpawner_Tap_mDFA9C2D75152A9CC0B8D1D798EE8093817D2BC16(__this, /*hidden argument*/NULL);
		// sizeScale = transform.localScale.x;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_0, /*hidden argument*/NULL);
		float L_2 = L_1.get_x_2();
		__this->set_sizeScale_37(L_2);
		// }
		return;
	}
}
// System.Void TooltipSpawner::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipSpawner_Update_mFEB1B21CAE84D1C66FBE5272F21213C8C976AF0A (TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * __this, const RuntimeMethod* method)
{
	{
		// transform.position = anchorTransform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = __this->get_anchorTransform_35();
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_0, L_2, /*hidden argument*/NULL);
		// transform.localScale = sizeScale * parentTransform.localScale;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_4 = __this->get_sizeScale_37();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5 = __this->get_parentTransform_36();
		NullCheck(L_5);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_5, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline(L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_3, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TooltipSpawner::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipSpawner__ctor_m519001FF8E4A450994F1DFDA970FEDE5DF58C07A (TooltipSpawner_tD393B97AC593B61C20773DD09791FE3031D68DAE * __this, const RuntimeMethod* method)
{
	{
		// public bool CanActive = true;
		__this->set_CanActive_34((bool)1);
		ToolTipSpawner__ctor_m6F9E2CCD2106F582CD1A2CF59CD740C96B4EBC12(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float PinchSlider_get_SliderValue_m44C37EBD34DD63E6FBB6EEF83F8F52AD88E0D600_inline (PinchSlider_tEA96A3D58C33C120815BF4BA2AF6336535C976AB * __this, const RuntimeMethod* method)
{
	{
		// get { return sliderValue; }
		float L_0 = __this->get_sliderValue_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		float L_1;
		L_1 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___a0;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___b1;
		float L_5 = L_4.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_x_2();
		float L_8 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = ___a0;
		float L_10 = L_9.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = ___b1;
		float L_12 = L_11.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = ___a0;
		float L_14 = L_13.get_y_3();
		float L_15 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = ___a0;
		float L_17 = L_16.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = ___b1;
		float L_19 = L_18.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = ___a0;
		float L_21 = L_20.get_z_4();
		float L_22 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_23), ((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), (float)L_8)))), ((float)il2cpp_codegen_add((float)L_10, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_12, (float)L_14)), (float)L_15)))), ((float)il2cpp_codegen_add((float)L_17, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_19, (float)L_21)), (float)L_22)))), /*hidden argument*/NULL);
		V_0 = L_23;
		goto IL_0053;
	}

IL_0053:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_0;
		return L_24;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline (float ___d0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a1;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a1;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a1;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
