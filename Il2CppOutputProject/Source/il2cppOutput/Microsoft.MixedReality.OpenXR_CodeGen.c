﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Threading.Tasks.Task`1<Microsoft.MixedReality.ARSubsystems.XRAnchorStore> Microsoft.MixedReality.ARSubsystems.AnchorSubsystemExtensions::LoadAnchorStoreAsync(UnityEngine.XR.ARSubsystems.XRAnchorSubsystem)
extern void AnchorSubsystemExtensions_LoadAnchorStoreAsync_m6FE5CF203AAA962E70D9097D2832D42D94297AD8 (void);
// 0x00000002 System.Threading.Tasks.Task`1<Microsoft.MixedReality.ARSubsystems.XRAnchorStore> Microsoft.MixedReality.ARSubsystems.XRAnchorStore::LoadAsync(UnityEngine.XR.ARSubsystems.XRAnchorSubsystem)
extern void XRAnchorStore_LoadAsync_mDA1D03EF31EBE83909DFB324028B9BD3E0E2EE08 (void);
// 0x00000003 System.Void Microsoft.MixedReality.ARSubsystems.XRAnchorStore::.ctor(Microsoft.MixedReality.OpenXR.OpenXRAnchorStore)
extern void XRAnchorStore__ctor_mBB9244327CF659BD49651E9CF388FCBCF96BEE98 (void);
// 0x00000004 System.Boolean Microsoft.MixedReality.ARSubsystems.XRAnchorStore::TryPersistAnchor(System.String,UnityEngine.XR.ARSubsystems.TrackableId)
extern void XRAnchorStore_TryPersistAnchor_mDFC7EF535C882DE15CF999113AE78671EB2DD5D2 (void);
// 0x00000005 System.Void Microsoft.MixedReality.ARSubsystems.XRAnchorStore/<LoadAsync>d__0::MoveNext()
extern void U3CLoadAsyncU3Ed__0_MoveNext_m2A1537A907B67894A1892E2913EF1E9B8403E614 (void);
// 0x00000006 System.Void Microsoft.MixedReality.ARSubsystems.XRAnchorStore/<LoadAsync>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadAsyncU3Ed__0_SetStateMachine_mB84B24F4974916FB514D4D019E083C7773C8ADE3 (void);
// 0x00000007 System.UInt64 Microsoft.MixedReality.OpenXR.AnchorConverter::ToOpenXRHandle(System.IntPtr)
extern void AnchorConverter_ToOpenXRHandle_m576A122D22F8359B35B00A2C5DCE7240F475191D (void);
// 0x00000008 System.Object Microsoft.MixedReality.OpenXR.AnchorConverter::ToPerceptionSpatialAnchor(System.IntPtr)
extern void AnchorConverter_ToPerceptionSpatialAnchor_m93CFF55D4A2D954A0FC749ADE25409004767528F (void);
// 0x00000009 System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::OnEnable()
extern void EyeLevelSceneOrigin_OnEnable_m00ED1D8359AAEC619D26B2CF82258437A8C34CB9 (void);
// 0x0000000A System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::OnDisable()
extern void EyeLevelSceneOrigin_OnDisable_m95F5305D81E14652FDC301E38B966F372C845CE8 (void);
// 0x0000000B System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::XrInput_trackingOriginUpdated(UnityEngine.XR.XRInputSubsystem)
extern void EyeLevelSceneOrigin_XrInput_trackingOriginUpdated_m5FDA30688AE020A0517756F56ED6A54126BB26C6 (void);
// 0x0000000C System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::EnsureSceneOriginAtEyeLevel()
extern void EyeLevelSceneOrigin_EnsureSceneOriginAtEyeLevel_m34344C65A3ED5232852EC4B98BB369343B7EBE9F (void);
// 0x0000000D System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::SetEyeLevelTrackingOriginMode(UnityEngine.XR.XRInputSubsystem)
extern void EyeLevelSceneOrigin_SetEyeLevelTrackingOriginMode_m54B1F0894B64A8C1CE9B15094BC583E24CD8327F (void);
// 0x0000000E System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::.ctor()
extern void EyeLevelSceneOrigin__ctor_mB9D65067BD4ED7424ACE6EF387F3EE3AD26F17BD (void);
// 0x0000000F Microsoft.MixedReality.OpenXR.HandMeshTracker Microsoft.MixedReality.OpenXR.HandMeshTracker::get_Left()
extern void HandMeshTracker_get_Left_m834FB22088CF46F9EF46DAC8731E6DF270106FAF (void);
// 0x00000010 Microsoft.MixedReality.OpenXR.HandMeshTracker Microsoft.MixedReality.OpenXR.HandMeshTracker::get_Right()
extern void HandMeshTracker_get_Right_m6705D8B2C8AB6513519C4B8D8738FE36DFC33731 (void);
// 0x00000011 System.Void Microsoft.MixedReality.OpenXR.HandMeshTracker::.ctor(Microsoft.MixedReality.OpenXR.Handedness)
extern void HandMeshTracker__ctor_m96AEFA60A8B55A54D6A1992AE14935E6B54E3CCA (void);
// 0x00000012 System.Boolean Microsoft.MixedReality.OpenXR.HandMeshTracker::TryLocateHandMesh(Microsoft.MixedReality.OpenXR.FrameTime,UnityEngine.Pose&,Microsoft.MixedReality.OpenXR.HandPoseType)
extern void HandMeshTracker_TryLocateHandMesh_mF8FED4EFDF402198B60E71BD7E6D07AF73BE16B5 (void);
// 0x00000013 System.Boolean Microsoft.MixedReality.OpenXR.HandMeshTracker::TryGetHandMesh(Microsoft.MixedReality.OpenXR.FrameTime,UnityEngine.Mesh,Microsoft.MixedReality.OpenXR.HandPoseType)
extern void HandMeshTracker_TryGetHandMesh_m47B073A5300E55CC93A59C05DD28EF96C7739566 (void);
// 0x00000014 System.Void Microsoft.MixedReality.OpenXR.HandMeshTracker::.cctor()
extern void HandMeshTracker__cctor_m395D1B0978E9CDAF41F91885D7F0B3E60537C65C (void);
// 0x00000015 Microsoft.MixedReality.OpenXR.HandTracker Microsoft.MixedReality.OpenXR.HandTracker::get_Left()
extern void HandTracker_get_Left_m708C38E9BC40B0FBEB07E9163C3148E8007A1A7F (void);
// 0x00000016 Microsoft.MixedReality.OpenXR.HandTracker Microsoft.MixedReality.OpenXR.HandTracker::get_Right()
extern void HandTracker_get_Right_m65535255C8267CCABFFDD58A6BE59F4DBCCAB159 (void);
// 0x00000017 System.Void Microsoft.MixedReality.OpenXR.HandTracker::.ctor(Microsoft.MixedReality.OpenXR.Handedness)
extern void HandTracker__ctor_mFC00C937914EEA49539F9B8594FB66E72EFA3C86 (void);
// 0x00000018 System.Boolean Microsoft.MixedReality.OpenXR.HandTracker::TryLocateHandJoints(Microsoft.MixedReality.OpenXR.FrameTime,Microsoft.MixedReality.OpenXR.HandJointLocation[])
extern void HandTracker_TryLocateHandJoints_mB8FE6A877DE60EF653018450D6157F8205CB71D4 (void);
// 0x00000019 System.Void Microsoft.MixedReality.OpenXR.HandTracker::.cctor()
extern void HandTracker__cctor_mF1E48FB95599D8D57993209156F006B4118CD5DC (void);
// 0x0000001A System.Boolean Microsoft.MixedReality.OpenXR.HandJointLocation::get_IsTracked()
extern void HandJointLocation_get_IsTracked_m528CFAA9AD0EE23A6A17CCD1C6C57AA86E69E4FA (void);
// 0x0000001B UnityEngine.Pose Microsoft.MixedReality.OpenXR.HandJointLocation::get_Pose()
extern void HandJointLocation_get_Pose_mEC69083D5F01D41D2737848B69B570C88771B652 (void);
// 0x0000001C System.Single Microsoft.MixedReality.OpenXR.HandJointLocation::get_Radius()
extern void HandJointLocation_get_Radius_m59C2EE4AC8714AF1583CD70EA7CD5A68847C726F (void);
// 0x0000001D Microsoft.MixedReality.OpenXR.OpenXRContext Microsoft.MixedReality.OpenXR.OpenXRContext::get_Current()
extern void OpenXRContext_get_Current_m2C50D894964F4BC2F6C43F5BED56A79EA79344A9 (void);
// 0x0000001E System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRContext::get_Instance()
extern void OpenXRContext_get_Instance_mB67910BD8536AA7CF32EF4C87C2C1759DC957360 (void);
// 0x0000001F System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRContext::get_SystemId()
extern void OpenXRContext_get_SystemId_m176A221E49277FC64F92D66A7B2A61B0B0577F75 (void);
// 0x00000020 System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRContext::get_Session()
extern void OpenXRContext_get_Session_m5363D24082DC44974022573E80C77DE43D19AA74 (void);
// 0x00000021 System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRContext::get_SceneOriginSpace()
extern void OpenXRContext_get_SceneOriginSpace_m6BFD1AE8537A39EBAB77FF0176CDC44780363C91 (void);
// 0x00000022 System.IntPtr Microsoft.MixedReality.OpenXR.OpenXRContext::GetInstanceProcAddr(System.String)
extern void OpenXRContext_GetInstanceProcAddr_m2514E8BFBC0D814FDF0BB717EDA6E3955E4E32D8 (void);
// 0x00000023 System.Void Microsoft.MixedReality.OpenXR.OpenXRContext::.ctor()
extern void OpenXRContext__ctor_m3E03CDBC2ED10CAAF58D5F6C1222503E1663FE51 (void);
// 0x00000024 System.Void Microsoft.MixedReality.OpenXR.OpenXRContext::.cctor()
extern void OpenXRContext__cctor_mE94747D7A8788E3741302B0C527C2B0D849F561D (void);
// 0x00000025 System.Object Microsoft.MixedReality.OpenXR.PerceptionInterop::GetSceneCoordinateSystem(UnityEngine.Pose)
extern void PerceptionInterop_GetSceneCoordinateSystem_m154FE304CA6748BEE3B793838BC064B0AC99C45C (void);
// 0x00000026 Microsoft.MixedReality.OpenXR.SpatialGraphNode Microsoft.MixedReality.OpenXR.SpatialGraphNode::FromStaticNodeId(System.Guid)
extern void SpatialGraphNode_FromStaticNodeId_m52DFF9B952A4AF999931FDF7704A53F3D16C2A1D (void);
// 0x00000027 System.Guid Microsoft.MixedReality.OpenXR.SpatialGraphNode::get_Id()
extern void SpatialGraphNode_get_Id_m96236885EF759340AC6EE05414F163834A7268CD (void);
// 0x00000028 System.Void Microsoft.MixedReality.OpenXR.SpatialGraphNode::set_Id(System.Guid)
extern void SpatialGraphNode_set_Id_mF0E64EED4BA607D324C97A2D08183EC05AE760EC (void);
// 0x00000029 System.Boolean Microsoft.MixedReality.OpenXR.SpatialGraphNode::TryLocate(Microsoft.MixedReality.OpenXR.FrameTime,UnityEngine.Pose&)
extern void SpatialGraphNode_TryLocate_mD93733F219487C8988DEE38AC0C926DF17A76380 (void);
// 0x0000002A System.Void Microsoft.MixedReality.OpenXR.SpatialGraphNode::.ctor()
extern void SpatialGraphNode__ctor_m2D204450B9B811F0496D51322D7D8A3FFA55BDB6 (void);
// 0x0000002B System.Collections.Generic.IReadOnlyList`1<System.String> Microsoft.MixedReality.OpenXR.XRAnchorStore::get_PersistedAnchorNames()
extern void XRAnchorStore_get_PersistedAnchorNames_mD73343D81FDABEA8AF20B190C0572D499FDC63C3 (void);
// 0x0000002C UnityEngine.XR.ARSubsystems.TrackableId Microsoft.MixedReality.OpenXR.XRAnchorStore::LoadAnchor(System.String)
extern void XRAnchorStore_LoadAnchor_m336CC9B5F6E9D72CBBD435E824B6684670F0C015 (void);
// 0x0000002D System.Boolean Microsoft.MixedReality.OpenXR.XRAnchorStore::TryPersistAnchor(UnityEngine.XR.ARSubsystems.TrackableId,System.String)
extern void XRAnchorStore_TryPersistAnchor_m1A5584B25D7BB8F2AACCE6A466556F7C94E7691A (void);
// 0x0000002E System.Void Microsoft.MixedReality.OpenXR.XRAnchorStore::UnpersistAnchor(System.String)
extern void XRAnchorStore_UnpersistAnchor_m6F63B1F3D8E2406FEF4FBA4FD8E720EAD4F998CF (void);
// 0x0000002F System.Void Microsoft.MixedReality.OpenXR.XRAnchorStore::Clear()
extern void XRAnchorStore_Clear_m10AE0501361553F7F3598F95AFE026CE9DDFFECF (void);
// 0x00000030 System.Threading.Tasks.Task`1<Microsoft.MixedReality.OpenXR.XRAnchorStore> Microsoft.MixedReality.OpenXR.XRAnchorStore::LoadAsync(UnityEngine.XR.ARSubsystems.XRAnchorSubsystem)
extern void XRAnchorStore_LoadAsync_m822582D492817B68C688B330A1826EFC96EDA38D (void);
// 0x00000031 System.Void Microsoft.MixedReality.OpenXR.XRAnchorStore::.ctor(Microsoft.MixedReality.OpenXR.OpenXRAnchorStore)
extern void XRAnchorStore__ctor_m219B5DAF9949C2BB75DDBD45FF1D0BF250055BD7 (void);
// 0x00000032 System.Void Microsoft.MixedReality.OpenXR.XRAnchorStore/<LoadAsync>d__6::MoveNext()
extern void U3CLoadAsyncU3Ed__6_MoveNext_mA1103540F922A4061730833EAC5FA4F971FD4056 (void);
// 0x00000033 System.Void Microsoft.MixedReality.OpenXR.XRAnchorStore/<LoadAsync>d__6::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadAsyncU3Ed__6_SetStateMachine_m3DB639B56B9ADE4D2CF850C4968E94B21C254D42 (void);
// 0x00000034 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile::RegisterDeviceLayout()
extern void HPMixedRealityControllerProfile_RegisterDeviceLayout_m643E010D432A18B71C0238E9A27B556F86652723 (void);
// 0x00000035 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile::UnregisterDeviceLayout()
extern void HPMixedRealityControllerProfile_UnregisterDeviceLayout_m1C6B5C13FD59E7233D99A1CD4A73D486013057E6 (void);
// 0x00000036 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile::RegisterActionMapsWithRuntime()
extern void HPMixedRealityControllerProfile_RegisterActionMapsWithRuntime_mFB00DEBB805C8DF7A6CDA38C08D125DE7526A130 (void);
// 0x00000037 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile::.ctor()
extern void HPMixedRealityControllerProfile__ctor_m10B69791512D9F93E7B2C028EC47A30E65121FF8 (void);
// 0x00000038 UnityEngine.InputSystem.Controls.Vector2Control Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_thumbstick()
extern void HPMixedRealityController_get_thumbstick_m90CA4687E93563B0DEB602449DAE1FFFC911F62C (void);
// 0x00000039 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_thumbstick(UnityEngine.InputSystem.Controls.Vector2Control)
extern void HPMixedRealityController_set_thumbstick_mB63719233F55DCED55B0DEFE3F5D893D8D7B989C (void);
// 0x0000003A UnityEngine.InputSystem.Controls.AxisControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_grip()
extern void HPMixedRealityController_get_grip_mCE781A33AAF069E177C6EF5100164BD66236C810 (void);
// 0x0000003B System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_grip(UnityEngine.InputSystem.Controls.AxisControl)
extern void HPMixedRealityController_set_grip_m80A88B6E2D47AD5CC2722B7E10B4185FA96696DC (void);
// 0x0000003C UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_gripPressed()
extern void HPMixedRealityController_get_gripPressed_mEA1F4C574F8BB5A1D1A44FA98FD43AB5F52EE1B4 (void);
// 0x0000003D System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_gripPressed(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_gripPressed_m862349937A453211DF89235CDE719DFDC01DDC0E (void);
// 0x0000003E UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_menu()
extern void HPMixedRealityController_get_menu_mC0A16B388913E40C8EED70AE5F8CE640957F706A (void);
// 0x0000003F System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_menu(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_menu_mF0C460935A0A1C28FE859EF75BB5FC3E2140F691 (void);
// 0x00000040 UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_primaryButton()
extern void HPMixedRealityController_get_primaryButton_m378BDF8328E8A6DE3B0A33C985A464949669D8B8 (void);
// 0x00000041 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_primaryButton(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_primaryButton_mEA85F1585A82C77FA5693E33D0F7996783BCEA44 (void);
// 0x00000042 UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_secondaryButton()
extern void HPMixedRealityController_get_secondaryButton_m426DCE2368DD70DDFC5B3D0F67F9D6A70D0525DD (void);
// 0x00000043 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_secondaryButton(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_secondaryButton_m70E3935734AC36C792D1A54AFFDCF1C429E0353C (void);
// 0x00000044 UnityEngine.InputSystem.Controls.AxisControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_trigger()
extern void HPMixedRealityController_get_trigger_mF66176140D4592DE7122078E6242464EC87F63C8 (void);
// 0x00000045 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_trigger(UnityEngine.InputSystem.Controls.AxisControl)
extern void HPMixedRealityController_set_trigger_m5E824031767A7B4E03A4DDD6A73C590D414A3229 (void);
// 0x00000046 UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_triggerPressed()
extern void HPMixedRealityController_get_triggerPressed_m8ED9173BF9C6028DD0970D25D15AC87DEAF7DA45 (void);
// 0x00000047 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_triggerPressed(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_triggerPressed_m743356C12B8EFD746028482D7E57F5513914D3CE (void);
// 0x00000048 UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_thumbstickClicked()
extern void HPMixedRealityController_get_thumbstickClicked_m528DA9346A052CB8CF6D6BFBDE6AD670CC3A4846 (void);
// 0x00000049 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_thumbstickClicked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_thumbstickClicked_mF3DC1E0A64B894C96E88ACEDC198EB1769D8B9F1 (void);
// 0x0000004A UnityEngine.XR.OpenXR.Input.PoseControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_devicePose()
extern void HPMixedRealityController_get_devicePose_m4CA8FBCD335A15BFC9C1D2DFAC011CCB7FF6D9C7 (void);
// 0x0000004B System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_devicePose(UnityEngine.XR.OpenXR.Input.PoseControl)
extern void HPMixedRealityController_set_devicePose_mC0EAE470D05077DF7492B88D1A0549458217EA59 (void);
// 0x0000004C UnityEngine.XR.OpenXR.Input.PoseControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_pointer()
extern void HPMixedRealityController_get_pointer_m3FBCC9B7054E2FE2C0B78C8EB6B7D68330D42BC0 (void);
// 0x0000004D System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_pointer(UnityEngine.XR.OpenXR.Input.PoseControl)
extern void HPMixedRealityController_set_pointer_mD3E0295F051BB52FD90B7C704C1EF3D509F78466 (void);
// 0x0000004E UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_isTracked()
extern void HPMixedRealityController_get_isTracked_m6DEA5B5F2EECC0129E12B53ECF10812A570125FC (void);
// 0x0000004F System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_isTracked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_isTracked_mF02911DC0F1C1E2D6C8EDF61E4D7DEAE9CF85B5C (void);
// 0x00000050 UnityEngine.InputSystem.Controls.IntegerControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_trackingState()
extern void HPMixedRealityController_get_trackingState_mAF48A26032D476F52E4538464FA9183F5CB8E1BC (void);
// 0x00000051 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_trackingState(UnityEngine.InputSystem.Controls.IntegerControl)
extern void HPMixedRealityController_set_trackingState_m6387B052D3C16EDB339858D9E5B38EA1028975CA (void);
// 0x00000052 UnityEngine.InputSystem.Controls.Vector3Control Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_devicePosition()
extern void HPMixedRealityController_get_devicePosition_m684527E5770BAAEE45AFED473D4DACE9F5061B96 (void);
// 0x00000053 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_devicePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void HPMixedRealityController_set_devicePosition_m448F72744C9775C8F30D2865077F87A5A7E80011 (void);
// 0x00000054 UnityEngine.InputSystem.Controls.QuaternionControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_deviceRotation()
extern void HPMixedRealityController_get_deviceRotation_mDDDAD598D77716DB2322BFB0E9E7FC86A19FCFF9 (void);
// 0x00000055 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_deviceRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void HPMixedRealityController_set_deviceRotation_mF9F5F1DEBF0B7D67F3B41E68E9288AE1A2349160 (void);
// 0x00000056 UnityEngine.InputSystem.Controls.Vector3Control Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_pointerPosition()
extern void HPMixedRealityController_get_pointerPosition_m494846ED88B2D8FE279793989489381FDD3F2060 (void);
// 0x00000057 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_pointerPosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void HPMixedRealityController_set_pointerPosition_m9E7EEC6DF082911E9BA713EB47E901118FF1EE52 (void);
// 0x00000058 UnityEngine.InputSystem.Controls.QuaternionControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_pointerRotation()
extern void HPMixedRealityController_get_pointerRotation_m9B28C8E1D41987CFBC8AA2594EC7BD3BA47566C7 (void);
// 0x00000059 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_pointerRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void HPMixedRealityController_set_pointerRotation_m96F55CDCC866F08C06204FE1445AC66E24D79CF1 (void);
// 0x0000005A System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::FinishSetup()
extern void HPMixedRealityController_FinishSetup_m0BF2BC2EB2C115DBA7EE6117CE455BCB0FE22D1C (void);
// 0x0000005B System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::.ctor()
extern void HPMixedRealityController__ctor_m704F1B3A4B5E78F13EA461FA17C8E3F03C869F81 (void);
// 0x0000005C System.Void Microsoft.MixedReality.OpenXR.HandTrackingFeaturePlugin::.ctor()
extern void HandTrackingFeaturePlugin__ctor_mA99DA099D30237012B5FC959177DA41C5188360C (void);
// 0x0000005D System.Void Microsoft.MixedReality.OpenXR.HoloLensFeaturePlugin::.ctor()
extern void HoloLensFeaturePlugin__ctor_mAA52F59E8A11A9ADD7C3409D97E1BC6392E9433C (void);
// 0x0000005E System.IntPtr Microsoft.MixedReality.OpenXR.HoloLensFeaturePlugin::TryAcquireSceneCoordinateSystem(UnityEngine.Pose)
extern void HoloLensFeaturePlugin_TryAcquireSceneCoordinateSystem_m8AA8C79A1063C9C15CABC9955679256C0C013568 (void);
// 0x0000005F System.IntPtr Microsoft.MixedReality.OpenXR.HoloLensFeaturePlugin::TryAcquirePerceptionSpatialAnchor(System.UInt64)
extern void HoloLensFeaturePlugin_TryAcquirePerceptionSpatialAnchor_m1F8DCCEE788B18D068B6A5CFF8CD8718D0F2233B (void);
// 0x00000060 System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_Instance()
// 0x00000061 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_Instance(System.UInt64)
// 0x00000062 System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_SystemId()
// 0x00000063 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_SystemId(System.UInt64)
// 0x00000064 System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_Session()
// 0x00000065 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_Session(System.UInt64)
// 0x00000066 Microsoft.MixedReality.OpenXR.XrSessionState Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_SessionState()
// 0x00000067 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_SessionState(Microsoft.MixedReality.OpenXR.XrSessionState)
// 0x00000068 System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_SceneOriginSpace()
// 0x00000069 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_SceneOriginSpace(System.UInt64)
// 0x0000006A System.Boolean Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_IsAnchorExtensionSupported()
// 0x0000006B System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_IsAnchorExtensionSupported(System.Boolean)
// 0x0000006C System.IntPtr Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::GetInstanceProcAddr(System.String)
// 0x0000006D System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::.cctor()
// 0x0000006E System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::.ctor()
// 0x0000006F System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::AddSubsystemController(Microsoft.MixedReality.OpenXR.SubsystemController)
// 0x00000070 System.Boolean Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::IsExtensionEnabled(System.String,System.UInt32)
// 0x00000071 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSubsystemCreate()
// 0x00000072 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSubsystemStart()
// 0x00000073 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSubsystemStop()
// 0x00000074 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSubsystemDestroy()
// 0x00000075 System.IntPtr Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::HookGetInstanceProcAddr(System.IntPtr)
// 0x00000076 System.Boolean Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnInstanceCreate(System.UInt64)
// 0x00000077 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnInstanceDestroy(System.UInt64)
// 0x00000078 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSystemChange(System.UInt64)
// 0x00000079 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSessionCreate(System.UInt64)
// 0x0000007A System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSessionBegin(System.UInt64)
// 0x0000007B System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSessionStateChange(System.Int32,System.Int32)
// 0x0000007C System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSessionEnd(System.UInt64)
// 0x0000007D System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSessionDestroy(System.UInt64)
// 0x0000007E System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnAppSpaceChange(System.UInt64)
// 0x0000007F System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::Microsoft.MixedReality.OpenXR.ISubsystemPlugin.CreateSubsystem(System.Collections.Generic.List`1<TDescriptor>,System.String)
// 0x00000080 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::Microsoft.MixedReality.OpenXR.ISubsystemPlugin.StartSubsystem()
// 0x00000081 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::Microsoft.MixedReality.OpenXR.ISubsystemPlugin.StopSubsystem()
// 0x00000082 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::Microsoft.MixedReality.OpenXR.ISubsystemPlugin.DestroySubsystem()
// 0x00000083 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::<OnSubsystemCreate>b__31_0(Microsoft.MixedReality.OpenXR.SubsystemController)
// 0x00000084 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::<OnSubsystemStart>b__32_0(Microsoft.MixedReality.OpenXR.SubsystemController)
// 0x00000085 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::<OnSubsystemStop>b__33_0(Microsoft.MixedReality.OpenXR.SubsystemController)
// 0x00000086 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::<OnSubsystemDestroy>b__34_0(Microsoft.MixedReality.OpenXR.SubsystemController)
// 0x00000087 UnityEngine.XR.ARSubsystems.TrackableId Microsoft.MixedReality.OpenXR.FeatureUtils::ToTrackableId(System.Guid)
extern void FeatureUtils_ToTrackableId_m7C58515C6BE66FDF0FE0D6CE9DE7FD8575CD6201 (void);
// 0x00000088 System.Guid Microsoft.MixedReality.OpenXR.FeatureUtils::ToGuid(UnityEngine.XR.ARSubsystems.TrackableId)
extern void FeatureUtils_ToGuid_m4237B8C32661BD32DED6C8AE49B74B564CBDC657 (void);
// 0x00000089 Microsoft.MixedReality.OpenXR.NativeLibToken Microsoft.MixedReality.OpenXR.NativeLibTokenAttribute::get_NativeLibToken()
extern void NativeLibTokenAttribute_get_NativeLibToken_mC21339A02A50AAF0A09469B71CE8F598281B18DF (void);
// 0x0000008A System.Void Microsoft.MixedReality.OpenXR.NativeLibTokenAttribute::set_NativeLibToken(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLibTokenAttribute_set_NativeLibToken_mCF242DA8234A359AC667DFB290D6E3274ECE7F1C (void);
// 0x0000008B System.Void Microsoft.MixedReality.OpenXR.NativeLibTokenAttribute::.ctor()
extern void NativeLibTokenAttribute__ctor_m50A60218A276DE4E6F1880378DF7B2DA51FB3CDF (void);
// 0x0000008C System.Void Microsoft.MixedReality.OpenXR.NativeLib::InitializeNativeLibToken(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_InitializeNativeLibToken_mFB184561FD20A6D28015A889A2822E5478A79EA7 (void);
// 0x0000008D System.IntPtr Microsoft.MixedReality.OpenXR.NativeLib::HookGetInstanceProcAddr(Microsoft.MixedReality.OpenXR.NativeLibToken,System.IntPtr)
extern void NativeLib_HookGetInstanceProcAddr_m157ADD4B8EB9E3E3AAF58656B5039847AF021EFC (void);
// 0x0000008E System.IntPtr Microsoft.MixedReality.OpenXR.NativeLib::GetInstanceProcAddr(System.UInt64,System.IntPtr,System.String)
extern void NativeLib_GetInstanceProcAddr_m40438AEACC92223D1ABBD04706B146F8067E6015 (void);
// 0x0000008F System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetXrInstance(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_SetXrInstance_mA620691013E77E93076C5A7B09C2C743D3911E0F (void);
// 0x00000090 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetXrSystemId(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_SetXrSystemId_mEC2D002C63EF165D9E5CAA82CB618AFCA783849E (void);
// 0x00000091 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetXrSession(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_SetXrSession_m0648545CBF17F451692243EF96C3F65392C6109F (void);
// 0x00000092 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetXrSessionRunning(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Boolean)
extern void NativeLib_SetXrSessionRunning_mD1525E58239D377BF78EC8DC8D53241224BDCD77 (void);
// 0x00000093 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetSessionState(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt32)
extern void NativeLib_SetSessionState_m9CADC5BB9186E443906C0BC64F7B5D54ACACE52A (void);
// 0x00000094 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetSceneOriginSpace(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_SetSceneOriginSpace_m4CC1782569AF1074272FAA17C0356D2D5849C173 (void);
// 0x00000095 System.Void Microsoft.MixedReality.OpenXR.NativeLib::CreateSceneUnderstandingProvider(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_CreateSceneUnderstandingProvider_m4D9CA492647E1799956EE24DB5B0423A0A8C0674 (void);
// 0x00000096 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetSceneUnderstandingActive(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Boolean)
extern void NativeLib_SetSceneUnderstandingActive_mB7D4A4371E6C8E924ECC97402C9038EE7DA4E795 (void);
// 0x00000097 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetPlaneDetectionMode(Microsoft.MixedReality.OpenXR.NativeLibToken,UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void NativeLib_SetPlaneDetectionMode_m98AF20BC0B12F704420663B9F2D73351786088C9 (void);
// 0x00000098 System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetNumPlaneChanges(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.FrameTime,System.UInt32&,System.UInt32&,System.UInt32&)
extern void NativeLib_GetNumPlaneChanges_m5C186E6181A991D9FCA39AEC70760C3644B0C97E (void);
// 0x00000099 System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetPlaneChanges(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Void*,System.Void*,System.Void*)
extern void NativeLib_GetPlaneChanges_mF946EDC685918A4A4B05FD89E915D8312FAFA7A1 (void);
// 0x0000009A System.Void Microsoft.MixedReality.OpenXR.NativeLib::CreateAnchorProvider(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_CreateAnchorProvider_mD62A08A0A1E787428F806AF73A0B0B2989F95D5D (void);
// 0x0000009B System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryAddAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.FrameTime,UnityEngine.Quaternion,UnityEngine.Vector3,System.Void*)
extern void NativeLib_TryAddAnchor_mA1B54BF7D924E99565359BFDE0425FEFFF72E57B (void);
// 0x0000009C System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryRemoveAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Guid)
extern void NativeLib_TryRemoveAnchor_m8B8E983FC1A25AC44C563D66BE3DFB4DDD3AF55C (void);
// 0x0000009D System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetNumAnchorChanges(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.FrameTime,System.UInt32&,System.UInt32&,System.UInt32&)
extern void NativeLib_GetNumAnchorChanges_mD77D5E7A502379543D83FCFAE6C0AC925E9700D4 (void);
// 0x0000009E System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetAnchorChanges(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Void*,System.Void*,System.Void*)
extern void NativeLib_GetAnchorChanges_mC56A3A5CC5F6792C4E9F4B59BD36366BE136A62F (void);
// 0x0000009F System.Void Microsoft.MixedReality.OpenXR.NativeLib::LoadAnchorStore(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_LoadAnchorStore_mEC3B001BAE71458407293CD87F652BCB1A5496BD (void);
// 0x000000A0 System.UInt32 Microsoft.MixedReality.OpenXR.NativeLib::GetNumPersistedAnchorNames(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_GetNumPersistedAnchorNames_mA61982B0515B67E1370DBF04BBAA043DB81BA236 (void);
// 0x000000A1 System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetPersistedAnchorName(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt32,System.Text.StringBuilder,System.UInt32)
extern void NativeLib_GetPersistedAnchorName_m68291776F7CB8EEFF75223B029F1CA9541B93788 (void);
// 0x000000A2 System.Guid Microsoft.MixedReality.OpenXR.NativeLib::LoadPersistedAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.String)
extern void NativeLib_LoadPersistedAnchor_mC98529DE4118DB40E19CB54D4FAD72F4A3BACFF0 (void);
// 0x000000A3 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryPersistAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.String,System.Guid)
extern void NativeLib_TryPersistAnchor_mA2C9DAC7FFA0293D42BB711A11A59C1C2D9DD113 (void);
// 0x000000A4 System.Void Microsoft.MixedReality.OpenXR.NativeLib::UnpersistAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.String)
extern void NativeLib_UnpersistAnchor_mD0C43A58B2225C648FBAFD2D5C58EFE97CA3C96C (void);
// 0x000000A5 System.Void Microsoft.MixedReality.OpenXR.NativeLib::ClearPersistedAnchors(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_ClearPersistedAnchors_m2602D238B9735D76818D1504F2E1FDA6DBC581EF (void);
// 0x000000A6 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::GetHandJointData(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.Handedness,Microsoft.MixedReality.OpenXR.FrameTime,Microsoft.MixedReality.OpenXR.HandJointLocation[])
extern void NativeLib_GetHandJointData_m76B412D5FA3FE1DEC7C40D1F23A2A49E810E781E (void);
// 0x000000A7 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryLocateHandMesh(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.Handedness,Microsoft.MixedReality.OpenXR.FrameTime,Microsoft.MixedReality.OpenXR.HandPoseType,UnityEngine.Pose&)
extern void NativeLib_TryLocateHandMesh_mC8563FBFBF75F58C7197614DF63E8EBD48981982 (void);
// 0x000000A8 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryGetHandMesh(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.Handedness,Microsoft.MixedReality.OpenXR.FrameTime,Microsoft.MixedReality.OpenXR.HandPoseType,System.UInt64&,System.Int32&,UnityEngine.Vector3[],UnityEngine.Vector3[],System.UInt32&,System.Int32&,System.Int32[])
extern void NativeLib_TryGetHandMesh_mB2B2F2FF7A0A9E961040D0D222248F981AC7A371 (void);
// 0x000000A9 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryGetHandMeshBufferSizes(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt32&,System.UInt32&)
extern void NativeLib_TryGetHandMeshBufferSizes_mA703D940C61C162089F1E5A355495204D9F18DC2 (void);
// 0x000000AA System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetRemotingEnabled(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Boolean)
extern void NativeLib_SetRemotingEnabled_m4E5E5DAAD3425AE86D8A5FC22A1E1D2720D696C5 (void);
// 0x000000AB System.Void Microsoft.MixedReality.OpenXR.NativeLib::ConnectRemoting(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.Remoting.RemotingConfiguration)
extern void NativeLib_ConnectRemoting_mDF7AD65315C924C43474A2FB2AF8E048BAF59413 (void);
// 0x000000AC System.Void Microsoft.MixedReality.OpenXR.NativeLib::DisconnectRemoting(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_DisconnectRemoting_m10705E91DFD8747170EB64B959352FD565894E2B (void);
// 0x000000AD System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryGetRemotingConnectionState(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.Remoting.ConnectionState&,Microsoft.MixedReality.OpenXR.Remoting.DisconnectReason&)
extern void NativeLib_TryGetRemotingConnectionState_m485B3E4A115182AB90075C494B7C61E9D79BA715 (void);
// 0x000000AE System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryCreateSpaceFromStaticNodeId(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Guid,System.UInt64&)
extern void NativeLib_TryCreateSpaceFromStaticNodeId_m94695049AA25F010AA2E0F03402F1AAEAE30D7A2 (void);
// 0x000000AF System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryLocateSpace(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64,Microsoft.MixedReality.OpenXR.FrameTime,UnityEngine.Pose&)
extern void NativeLib_TryLocateSpace_m251737A8AC075318D0E63523E819FA62CCD310E0 (void);
// 0x000000B0 System.IntPtr Microsoft.MixedReality.OpenXR.NativeLib::TryAcquireSceneCoordinateSystem(Microsoft.MixedReality.OpenXR.NativeLibToken,UnityEngine.Pose)
extern void NativeLib_TryAcquireSceneCoordinateSystem_m969BE2F38C8E3D35AFE327D1C826C3C8BCF6C257 (void);
// 0x000000B1 System.IntPtr Microsoft.MixedReality.OpenXR.NativeLib::TryAcquirePerceptionSpatialAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_TryAcquirePerceptionSpatialAnchor_mBF622FA52886BAD6AE653E4C2890303ACA7B72E2 (void);
// 0x000000B2 System.Void Microsoft.MixedReality.OpenXR.NativeLib::.ctor()
extern void NativeLib__ctor_m95FA93BF8531F3C1AFF3CEAF0B19D894C1AC0223 (void);
// 0x000000B3 System.Threading.Tasks.Task`1<Microsoft.MixedReality.OpenXR.OpenXRAnchorStore> Microsoft.MixedReality.OpenXR.OpenXRAnchorStoreFactory::LoadAnchorStoreAsync(UnityEngine.XR.ARSubsystems.XRAnchorSubsystem)
extern void OpenXRAnchorStoreFactory_LoadAnchorStoreAsync_m573D135B96906B05C625C6396743124EF7517BA8 (void);
// 0x000000B4 System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStoreFactory::.cctor()
extern void OpenXRAnchorStoreFactory__cctor_m59748FB5E2E0629749C60A3636FDA765EE0401C7 (void);
// 0x000000B5 System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStoreFactory/<>c::.cctor()
extern void U3CU3Ec__cctor_m3088AC00990AA49150B0BDBD07FB097EC693CE3B (void);
// 0x000000B6 System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStoreFactory/<>c::.ctor()
extern void U3CU3Ec__ctor_mDF746EA21581AEA5E95ED46C9F512DEC179B39CF (void);
// 0x000000B7 Microsoft.MixedReality.OpenXR.OpenXRAnchorStore Microsoft.MixedReality.OpenXR.OpenXRAnchorStoreFactory/<>c::<LoadAnchorStoreAsync>b__1_0()
extern void U3CU3Ec_U3CLoadAnchorStoreAsyncU3Eb__1_0_m744C3D925614EE8875DD036FCD9C7E4CA9759E5C (void);
// 0x000000B8 System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::.ctor()
extern void OpenXRAnchorStore__ctor_m74886FF418C3C6A5CB05A930210901FCF72111BE (void);
// 0x000000B9 System.Collections.Generic.IReadOnlyList`1<System.String> Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::get_PersistedAnchorNames()
extern void OpenXRAnchorStore_get_PersistedAnchorNames_m3E634306C2C615A387B0DAA9E9E49B1BD4440FF6 (void);
// 0x000000BA System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::UpdatePersistedAnchorNames()
extern void OpenXRAnchorStore_UpdatePersistedAnchorNames_mF5D13CEBAE7DA85448E6A3982F06DB7096C08A15 (void);
// 0x000000BB UnityEngine.XR.ARSubsystems.TrackableId Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::LoadAnchor(System.String)
extern void OpenXRAnchorStore_LoadAnchor_m72A6B4AC424B841AC7C61ACE09A3DE1349D24564 (void);
// 0x000000BC System.Boolean Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::TryPersistAnchor(System.String,UnityEngine.XR.ARSubsystems.TrackableId)
extern void OpenXRAnchorStore_TryPersistAnchor_m58A011F594DA2FD4E91FF4D62A7E11F185E8CF66 (void);
// 0x000000BD System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::UnpersistAnchor(System.String)
extern void OpenXRAnchorStore_UnpersistAnchor_mAFAD6E56BB5089AD9E565FEB51083880FF3E5595 (void);
// 0x000000BE System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::Clear()
extern void OpenXRAnchorStore_Clear_m62B06197D9C72DA1C5B0627B401BD3E96E573AC3 (void);
// 0x000000BF System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::.cctor()
extern void OpenXRAnchorStore__cctor_m72938B00A3A2590791AF3ADC8695D46D6DFD9E01 (void);
// 0x000000C0 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem::RegisterDescriptor()
extern void AnchorSubsystem_RegisterDescriptor_mD1EA19E524C831940D55D6E869398D7467743A5F (void);
// 0x000000C1 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem::.ctor()
extern void AnchorSubsystem__ctor_m84546ED42C1B06B7B4D72FB3FB47195452F771D4 (void);
// 0x000000C2 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::.ctor()
extern void OpenXRProvider__ctor_m8B50C78F370DAA1BF1B94683917396050A01A96D (void);
// 0x000000C3 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::Destroy()
extern void OpenXRProvider_Destroy_m34E0310FDCD2421B7D4FCD4B5C42D32AF42F9331 (void);
// 0x000000C4 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::Start()
extern void OpenXRProvider_Start_m8BA1EC9D384E0BE3365A7E6F9544A94859238D01 (void);
// 0x000000C5 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::Stop()
extern void OpenXRProvider_Stop_mF4232DBA5EB4BA42AC88F526A68F39E359B65829 (void);
// 0x000000C6 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRAnchor> Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::GetChanges(UnityEngine.XR.ARSubsystems.XRAnchor,Unity.Collections.Allocator)
extern void OpenXRProvider_GetChanges_m5612871362052FB23935AD3382E5CBE4A280A83D (void);
// 0x000000C7 UnityEngine.XR.ARSubsystems.XRAnchor Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::ToXRAnchor(Microsoft.MixedReality.OpenXR.NativeAnchor)
extern void OpenXRProvider_ToXRAnchor_m15F91E24F867D3EB630EE93853319A5C46B674EA (void);
// 0x000000C8 System.Boolean Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::TryAddAnchor(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void OpenXRProvider_TryAddAnchor_m559F71D34E599191F0E30D5F0A95121A6FA95491 (void);
// 0x000000C9 System.Boolean Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::TryAttachAnchor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void OpenXRProvider_TryAttachAnchor_mFB175010BF63CED9F3AD51EEF933449F0BD8CFB9 (void);
// 0x000000CA System.Boolean Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::TryRemoveAnchor(UnityEngine.XR.ARSubsystems.TrackableId)
extern void OpenXRProvider_TryRemoveAnchor_m3E22FC896FF84E1F4B4BE1A32725E7DD0033587E (void);
// 0x000000CB System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystemController::.ctor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void AnchorSubsystemController__ctor_m4F7FF65F4F884DF2B91A47C88F5159F865745693 (void);
// 0x000000CC System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void AnchorSubsystemController_OnSubsystemCreate_m4E4B001545FA3B7C75C7F0365023266A9566D1EB (void);
// 0x000000CD System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystemController::OnSubsystemStart(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void AnchorSubsystemController_OnSubsystemStart_m3C680BC1B621CEADFA4DCEEF77AF8799EE76855E (void);
// 0x000000CE System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystemController::OnSubsystemStop(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void AnchorSubsystemController_OnSubsystemStop_m8385EF365649CCB657739CAEA18C0CBEE4C53FBB (void);
// 0x000000CF System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void AnchorSubsystemController_OnSubsystemDestroy_m6D606C537DD6B43E206A337EBCEB997ABCEF244D (void);
// 0x000000D0 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystemController::.cctor()
extern void AnchorSubsystemController__cctor_mB38DC6BA1853A59A4D217430605D9324003D6A95 (void);
// 0x000000D1 System.Void Microsoft.MixedReality.OpenXR.HandTrackingSubsystemController::.ctor(Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void HandTrackingSubsystemController__ctor_m10AA76E56772014EA9B556FCC46B006D127791EF (void);
// 0x000000D2 System.Void Microsoft.MixedReality.OpenXR.HandTrackingSubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void HandTrackingSubsystemController_OnSubsystemCreate_m6B6EFDA87453531D625F53271674847E84E5A35A (void);
// 0x000000D3 System.Void Microsoft.MixedReality.OpenXR.HandTrackingSubsystemController::OnSubsystemStart(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void HandTrackingSubsystemController_OnSubsystemStart_mB486E6F094B18692C07971C4699F04B52FF8160C (void);
// 0x000000D4 System.Void Microsoft.MixedReality.OpenXR.HandTrackingSubsystemController::OnSubsystemStop(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void HandTrackingSubsystemController_OnSubsystemStop_mFE10DEB260BAA3D3DED0F59ECC9AA5BE8A993528 (void);
// 0x000000D5 System.Void Microsoft.MixedReality.OpenXR.HandTrackingSubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void HandTrackingSubsystemController_OnSubsystemDestroy_m8ACF797438DCFABC73DB56B85224926ECF632F28 (void);
// 0x000000D6 System.Void Microsoft.MixedReality.OpenXR.MeshSubsystemController::.ctor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void MeshSubsystemController__ctor_m2FCF91093A5509CBA095BF858E4501824DFB7EB7 (void);
// 0x000000D7 System.Void Microsoft.MixedReality.OpenXR.MeshSubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void MeshSubsystemController_OnSubsystemCreate_m7E6487221D11E16CC2E16F4042749101AC6C642C (void);
// 0x000000D8 System.Void Microsoft.MixedReality.OpenXR.MeshSubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void MeshSubsystemController_OnSubsystemDestroy_m390EF07D9F45BD946A8A28560B730814472A64D2 (void);
// 0x000000D9 System.Void Microsoft.MixedReality.OpenXR.MeshSubsystemController::.cctor()
extern void MeshSubsystemController__cctor_m8D8FAA79E5AB045B232BB66E796655B2B5B1A2B4 (void);
// 0x000000DA System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem::RegisterDescriptor()
extern void PlaneSubsystem_RegisterDescriptor_mC49177E583BFA41281C5F20A9A3C3EB7E5929114 (void);
// 0x000000DB System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem::.ctor()
extern void PlaneSubsystem__ctor_m4E30B58BA23643D867632DBC1FF8B6F2DFBD90E5 (void);
// 0x000000DC System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::.ctor()
extern void OpenXRProvider__ctor_m33CF547778A8A43FA9F57EB88206B9310CDD4888 (void);
// 0x000000DD System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::Start()
extern void OpenXRProvider_Start_m9CD935BA3200A3130CD96A1A0B1DE4C9FC22E407 (void);
// 0x000000DE System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::Stop()
extern void OpenXRProvider_Stop_m38A55D0BAE49F7D000397E458633B075B22B8B4E (void);
// 0x000000DF System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::Destroy()
extern void OpenXRProvider_Destroy_m0B72F6321683F87DC37E48D08E55420E0352304D (void);
// 0x000000E0 UnityEngine.XR.ARSubsystems.PlaneDetectionMode Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::get_currentPlaneDetectionMode()
extern void OpenXRProvider_get_currentPlaneDetectionMode_m8E928963BFEC07256214C098588F7348D33EBDE2 (void);
// 0x000000E1 UnityEngine.XR.ARSubsystems.PlaneDetectionMode Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::get_requestedPlaneDetectionMode()
extern void OpenXRProvider_get_requestedPlaneDetectionMode_m6701E8BE54B57E1011C7FD5794A7A7F84C44230E (void);
// 0x000000E2 System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::set_requestedPlaneDetectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void OpenXRProvider_set_requestedPlaneDetectionMode_m66DEF51C5B0D6E05A088CFAB180C17C8FBD33026 (void);
// 0x000000E3 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.BoundedPlane> Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::GetChanges(UnityEngine.XR.ARSubsystems.BoundedPlane,Unity.Collections.Allocator)
extern void OpenXRProvider_GetChanges_m0BFC0C66812A3BEBC36594FD8F6AD582A1D9CB7C (void);
// 0x000000E4 UnityEngine.XR.ARSubsystems.PlaneClassification Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::ToPlaneClassification(Microsoft.MixedReality.OpenXR.XrSceneObjectKindMSFT)
extern void OpenXRProvider_ToPlaneClassification_m63AA9EC46CF4DD00CF716707D54772A90B19C61E (void);
// 0x000000E5 UnityEngine.XR.ARSubsystems.BoundedPlane Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::ToBoundedPlane(Microsoft.MixedReality.OpenXR.NativePlane,UnityEngine.XR.ARSubsystems.BoundedPlane)
extern void OpenXRProvider_ToBoundedPlane_m22B24059DBA52B5CDCC9659554CF5E49282D4423 (void);
// 0x000000E6 System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystemController::.ctor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void PlaneSubsystemController__ctor_m476572A035D5FC9A24559E1C557410C137334A8D (void);
// 0x000000E7 System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void PlaneSubsystemController_OnSubsystemCreate_m2F3B4AEE4441D13FDD2CE398B43A8202F19249BE (void);
// 0x000000E8 System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void PlaneSubsystemController_OnSubsystemDestroy_m06F036C60B18A23CCE65B1E7D54B18C73AA0BF39 (void);
// 0x000000E9 System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystemController::.cctor()
extern void PlaneSubsystemController__cctor_m77D8EBD4728640503E17AB1ABF91BDE440EB83E4 (void);
// 0x000000EA System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystem::RegisterDescriptor()
extern void RaycastSubsystem_RegisterDescriptor_mDFFDCFFE9613F761ABF930299B6E11011BDEDBF9 (void);
// 0x000000EB System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystem::.ctor()
extern void RaycastSubsystem__ctor_mA4F69055FB87DDA90367FFCF63DF84FEA87E808A (void);
// 0x000000EC System.Boolean Microsoft.MixedReality.OpenXR.RaycastSubsystem/OpenXRProvider::TryAddRaycast(UnityEngine.Vector2,System.Single,UnityEngine.XR.ARSubsystems.XRRaycast&)
extern void OpenXRProvider_TryAddRaycast_mB48E29D969B1CE6D31EE955C12D95FB2EFF1DE96 (void);
// 0x000000ED System.Boolean Microsoft.MixedReality.OpenXR.RaycastSubsystem/OpenXRProvider::TryAddRaycast(UnityEngine.Ray,System.Single,UnityEngine.XR.ARSubsystems.XRRaycast&)
extern void OpenXRProvider_TryAddRaycast_m6D4C27ECB9FC547CB35DFF38C600E3D1F8AF9151 (void);
// 0x000000EE System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystem/OpenXRProvider::RemoveRaycast(UnityEngine.XR.ARSubsystems.TrackableId)
extern void OpenXRProvider_RemoveRaycast_mD6A70356FD0287EC2F3B228CAB093133BEBDAFBE (void);
// 0x000000EF System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystem/OpenXRProvider::.ctor()
extern void OpenXRProvider__ctor_m1AE2C017B4ED0990DB8F3EA9B8BC39732C6561A6 (void);
// 0x000000F0 System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystemController::.ctor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void RaycastSubsystemController__ctor_m9E585309950D8B13F2D3EA50F073901987A31110 (void);
// 0x000000F1 System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void RaycastSubsystemController_OnSubsystemCreate_m3A1E02241704B9D5B27424BAA768DDE18E5E6F43 (void);
// 0x000000F2 System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void RaycastSubsystemController_OnSubsystemDestroy_m5BA7E4A22EC1DE0485D061BE00168C106F7FB298 (void);
// 0x000000F3 System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystemController::.cctor()
extern void RaycastSubsystemController__cctor_m41E20C1FF5E28DF73888EBB5E57B6DF27EA237C8 (void);
// 0x000000F4 System.UInt64 Microsoft.MixedReality.OpenXR.IOpenXRContext::get_Instance()
// 0x000000F5 System.UInt64 Microsoft.MixedReality.OpenXR.IOpenXRContext::get_SystemId()
// 0x000000F6 System.UInt64 Microsoft.MixedReality.OpenXR.IOpenXRContext::get_Session()
// 0x000000F7 Microsoft.MixedReality.OpenXR.XrSessionState Microsoft.MixedReality.OpenXR.IOpenXRContext::get_SessionState()
// 0x000000F8 System.UInt64 Microsoft.MixedReality.OpenXR.IOpenXRContext::get_SceneOriginSpace()
// 0x000000F9 System.Boolean Microsoft.MixedReality.OpenXR.IOpenXRContext::get_IsAnchorExtensionSupported()
// 0x000000FA System.Void Microsoft.MixedReality.OpenXR.ISubsystemPlugin::CreateSubsystem(System.Collections.Generic.List`1<TDescriptor>,System.String)
// 0x000000FB System.Void Microsoft.MixedReality.OpenXR.ISubsystemPlugin::StartSubsystem()
// 0x000000FC System.Void Microsoft.MixedReality.OpenXR.ISubsystemPlugin::StopSubsystem()
// 0x000000FD System.Void Microsoft.MixedReality.OpenXR.ISubsystemPlugin::DestroySubsystem()
// 0x000000FE System.Void Microsoft.MixedReality.OpenXR.SubsystemController::.ctor(Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void SubsystemController__ctor_mE34F00D01DC143D97F415AB978C7D33C249B8C52 (void);
// 0x000000FF System.Void Microsoft.MixedReality.OpenXR.SubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void SubsystemController_OnSubsystemCreate_m396D9B3FD8B2ED051C4D06791EC59776FD015A18 (void);
// 0x00000100 System.Void Microsoft.MixedReality.OpenXR.SubsystemController::OnSubsystemStart(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void SubsystemController_OnSubsystemStart_m2E0C8E95A753022A97FB798636D0EE0AB0AC1DE6 (void);
// 0x00000101 System.Void Microsoft.MixedReality.OpenXR.SubsystemController::OnSubsystemStop(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void SubsystemController_OnSubsystemStop_mC887515BE16532B457067DD0A8D9CD5E0E405A63 (void);
// 0x00000102 System.Void Microsoft.MixedReality.OpenXR.SubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void SubsystemController_OnSubsystemDestroy_m95CD8DCE8ABFD005A7B819C60A0522057A882EC8 (void);
// 0x00000103 System.Void Microsoft.MixedReality.OpenXR.Preview.HandTracker::.ctor(Microsoft.MixedReality.OpenXR.Preview.Handedness,Microsoft.MixedReality.OpenXR.Preview.HandPoseType)
extern void HandTracker__ctor_m8A233C7E4A16C0C05450FA276C2151B1A58A0A3D (void);
// 0x00000104 System.Boolean Microsoft.MixedReality.OpenXR.Preview.HandTracker::TryLocateHandJoints(Microsoft.MixedReality.OpenXR.FrameTime,Microsoft.MixedReality.OpenXR.Preview.HandJointLocation[])
extern void HandTracker_TryLocateHandJoints_m1043ECB88351DA411C4CAA43BA9C336D98BC3DEC (void);
// 0x00000105 Microsoft.MixedReality.OpenXR.Preview.PoseFlags Microsoft.MixedReality.OpenXR.Preview.HandJointLocation::get_PoseFlags()
extern void HandJointLocation_get_PoseFlags_m1CD68F1D99A888442B52E06B2464692300714423 (void);
// 0x00000106 UnityEngine.Quaternion Microsoft.MixedReality.OpenXR.Preview.HandJointLocation::get_Rotation()
extern void HandJointLocation_get_Rotation_mE08792490B53A7951D778D5C2F9F7F7B89494E3D (void);
// 0x00000107 UnityEngine.Vector3 Microsoft.MixedReality.OpenXR.Preview.HandJointLocation::get_Position()
extern void HandJointLocation_get_Position_mD2FACF4383DD4FC9EE45505113C56C09C6636042 (void);
// 0x00000108 System.Single Microsoft.MixedReality.OpenXR.Preview.HandJointLocation::get_Radius()
extern void HandJointLocation_get_Radius_mDCE67E74FD5000DD8235D62B0A014B661F42BF83 (void);
// 0x00000109 System.Void Microsoft.MixedReality.OpenXR.Preview.HandJointLocation::.ctor(Microsoft.MixedReality.OpenXR.HandJointLocation)
extern void HandJointLocation__ctor_mDA21AEEBD97E42890321984B6B31E573A9A0E132 (void);
// 0x0000010A Microsoft.MixedReality.OpenXR.Remoting.RemotingConfiguration Microsoft.MixedReality.OpenXR.Remoting.AppRemoting::get_Configuration()
extern void AppRemoting_get_Configuration_m518C2AFDA11E111274D91A3877515F2EE21B93A3 (void);
// 0x0000010B System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemoting::set_Configuration(Microsoft.MixedReality.OpenXR.Remoting.RemotingConfiguration)
extern void AppRemoting_set_Configuration_mA37F1AC0EE17FCA0DFD43484FA0608E0D203DFD1 (void);
// 0x0000010C System.Collections.IEnumerator Microsoft.MixedReality.OpenXR.Remoting.AppRemoting::Connect(Microsoft.MixedReality.OpenXR.Remoting.RemotingConfiguration)
extern void AppRemoting_Connect_mFDF9ACF483419C1C1D1072B7B4E60AC4D714A3CF (void);
// 0x0000010D System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemoting::Disconnect()
extern void AppRemoting_Disconnect_m0F52220F566CCABD661D36D312329997DE95FA97 (void);
// 0x0000010E System.Boolean Microsoft.MixedReality.OpenXR.Remoting.AppRemoting::TryGetConnectionState(Microsoft.MixedReality.OpenXR.Remoting.ConnectionState&,Microsoft.MixedReality.OpenXR.Remoting.DisconnectReason&)
extern void AppRemoting_TryGetConnectionState_mD1BA1EC2E4A11E6D62FB67DC4218F69A3358394D (void);
// 0x0000010F System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::.ctor(System.Int32)
extern void U3CConnectU3Ed__4__ctor_m9B7169E748BDE0D567455598D91A91A7D2D60CA3 (void);
// 0x00000110 System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::System.IDisposable.Dispose()
extern void U3CConnectU3Ed__4_System_IDisposable_Dispose_mF2F8F709F0C863F20922D10016EF3769BF9CABA7 (void);
// 0x00000111 System.Boolean Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::MoveNext()
extern void U3CConnectU3Ed__4_MoveNext_m623AFEFAF62DB7F23C1C4288ED8D4F464ECB5FBF (void);
// 0x00000112 System.Object Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CConnectU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAEAED26889E7742A923273906179CB6DB875EF7D (void);
// 0x00000113 System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::System.Collections.IEnumerator.Reset()
extern void U3CConnectU3Ed__4_System_Collections_IEnumerator_Reset_mCAB5E7A2915C5628DF53ABD73DD21D5536C5A493 (void);
// 0x00000114 System.Object Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CConnectU3Ed__4_System_Collections_IEnumerator_get_Current_m5FC03290D91E6AC38AFA1D30CD249EB00999BAAC (void);
// 0x00000115 System.IntPtr Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::HookGetInstanceProcAddr(System.IntPtr)
extern void AppRemotingPlugin_HookGetInstanceProcAddr_mCE3FA55800CDF0365DD7619A59363B1C28B85705 (void);
// 0x00000116 System.Boolean Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::OnInstanceCreate(System.UInt64)
extern void AppRemotingPlugin_OnInstanceCreate_m19936D9D67E1ED10D7883A781B09C9BCC14E2D51 (void);
// 0x00000117 System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::OnInstanceDestroy(System.UInt64)
extern void AppRemotingPlugin_OnInstanceDestroy_mC8471F0A053507D63236A43490D6E288840AE422 (void);
// 0x00000118 System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::OnSystemChange(System.UInt64)
extern void AppRemotingPlugin_OnSystemChange_m2C939F7BEF8D3C948548ADC2905CDCEE7AC58BB8 (void);
// 0x00000119 System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::OnSessionStateChange(System.Int32,System.Int32)
extern void AppRemotingPlugin_OnSessionStateChange_m999AD7415BD4F2C2B6942921C52959CA416EAE08 (void);
// 0x0000011A System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::.ctor()
extern void AppRemotingPlugin__ctor_m41EF8B18529ABCF0EAA76A6D05CB9875CCFB4D2D (void);
// 0x0000011B System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin/<>c::.cctor()
extern void U3CU3Ec__cctor_mCEC795D0A26F437D463415A2BB6FBB67A5356A2E (void);
// 0x0000011C System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin/<>c::.ctor()
extern void U3CU3Ec__ctor_m9418F353D31FDE71C5354EB90B571268388FFDB0 (void);
// 0x0000011D System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin/<>c::<OnSystemChange>b__8_0()
extern void U3CU3Ec_U3COnSystemChangeU3Eb__8_0_m7C5D2079DAD009BBB06E25CBB01DD510D2BDA3D6 (void);
// 0x0000011E System.IntPtr Microsoft.MixedReality.OpenXR.Remoting.EditorRemotingPlugin::HookGetInstanceProcAddr(System.IntPtr)
extern void EditorRemotingPlugin_HookGetInstanceProcAddr_m923D29C37BE4B348DFEBD6332CAC26F36A1EFC2F (void);
// 0x0000011F System.Boolean Microsoft.MixedReality.OpenXR.Remoting.EditorRemotingPlugin::OnInstanceCreate(System.UInt64)
extern void EditorRemotingPlugin_OnInstanceCreate_m34F5AC8920D4896D3C5B896A81A739CA31FE40E5 (void);
// 0x00000120 System.Void Microsoft.MixedReality.OpenXR.Remoting.EditorRemotingPlugin::OnInstanceDestroy(System.UInt64)
extern void EditorRemotingPlugin_OnInstanceDestroy_m1397B99CCBD8BF66B3F27336C8D271FAF64EC4CE (void);
// 0x00000121 System.Void Microsoft.MixedReality.OpenXR.Remoting.EditorRemotingPlugin::OnSystemChange(System.UInt64)
extern void EditorRemotingPlugin_OnSystemChange_mD012D792DF758CAD0AC38DC9C9DA38B3547AB491 (void);
// 0x00000122 System.Void Microsoft.MixedReality.OpenXR.Remoting.EditorRemotingPlugin::OnSessionStateChange(System.Int32,System.Int32)
extern void EditorRemotingPlugin_OnSessionStateChange_m7E613D18BADA7865C3ECFCB106AFC51518F04262 (void);
// 0x00000123 System.Boolean Microsoft.MixedReality.OpenXR.Remoting.EditorRemotingPlugin::HasValidSettings()
extern void EditorRemotingPlugin_HasValidSettings_mB2CAC3F8CB424B0739284D685B733469927F91DD (void);
// 0x00000124 System.Void Microsoft.MixedReality.OpenXR.Remoting.EditorRemotingPlugin::.ctor()
extern void EditorRemotingPlugin__ctor_m655BD77B2FD6453C717BDFB05C45D5DF39BF49C4 (void);
// 0x00000125 System.Void Microsoft.MixedReality.OpenXR.Remoting.EditorRemotingPlugin::<OnSystemChange>b__13_0()
extern void EditorRemotingPlugin_U3COnSystemChangeU3Eb__13_0_m2AD00416A38236601D9DC419598D621ABF7F64A7 (void);
// 0x00000126 System.UInt64 Microsoft.MixedReality.OpenXR.ARFoundation.ARAnchorExtensions::GetOpenXRHandle(UnityEngine.XR.ARFoundation.ARAnchor)
extern void ARAnchorExtensions_GetOpenXRHandle_m07C39102695713B89B09A0AC5930F96FE51A7A80 (void);
// 0x00000127 System.Threading.Tasks.Task`1<Microsoft.MixedReality.OpenXR.XRAnchorStore> Microsoft.MixedReality.OpenXR.ARFoundation.AnchorManagerExtensions::LoadAnchorStoreAsync(UnityEngine.XR.ARFoundation.ARAnchorManager)
extern void AnchorManagerExtensions_LoadAnchorStoreAsync_m7FFB9D7FFB415A46DDB8822EE8FE81A220056F7B (void);
// 0x00000128 System.UInt64 Microsoft.MixedReality.OpenXR.ARSubsystems.XRAnchorExtensions::GetOpenXRHandle(UnityEngine.XR.ARSubsystems.XRAnchor)
extern void XRAnchorExtensions_GetOpenXRHandle_m3AB1B804A8570EA669D000A64CAC3B0BD1C08B94 (void);
// 0x00000129 System.Threading.Tasks.Task`1<Microsoft.MixedReality.OpenXR.XRAnchorStore> Microsoft.MixedReality.OpenXR.ARSubsystems.AnchorSubsystemExtensions::LoadAnchorStoreAsync(UnityEngine.XR.ARSubsystems.XRAnchorSubsystem)
extern void AnchorSubsystemExtensions_LoadAnchorStoreAsync_m04BE110C74F6AEA0055C448C3C74C02EBACA231D (void);
static Il2CppMethodPointer s_methodPointers[297] = 
{
	AnchorSubsystemExtensions_LoadAnchorStoreAsync_m6FE5CF203AAA962E70D9097D2832D42D94297AD8,
	XRAnchorStore_LoadAsync_mDA1D03EF31EBE83909DFB324028B9BD3E0E2EE08,
	XRAnchorStore__ctor_mBB9244327CF659BD49651E9CF388FCBCF96BEE98,
	XRAnchorStore_TryPersistAnchor_mDFC7EF535C882DE15CF999113AE78671EB2DD5D2,
	U3CLoadAsyncU3Ed__0_MoveNext_m2A1537A907B67894A1892E2913EF1E9B8403E614,
	U3CLoadAsyncU3Ed__0_SetStateMachine_mB84B24F4974916FB514D4D019E083C7773C8ADE3,
	AnchorConverter_ToOpenXRHandle_m576A122D22F8359B35B00A2C5DCE7240F475191D,
	AnchorConverter_ToPerceptionSpatialAnchor_m93CFF55D4A2D954A0FC749ADE25409004767528F,
	EyeLevelSceneOrigin_OnEnable_m00ED1D8359AAEC619D26B2CF82258437A8C34CB9,
	EyeLevelSceneOrigin_OnDisable_m95F5305D81E14652FDC301E38B966F372C845CE8,
	EyeLevelSceneOrigin_XrInput_trackingOriginUpdated_m5FDA30688AE020A0517756F56ED6A54126BB26C6,
	EyeLevelSceneOrigin_EnsureSceneOriginAtEyeLevel_m34344C65A3ED5232852EC4B98BB369343B7EBE9F,
	EyeLevelSceneOrigin_SetEyeLevelTrackingOriginMode_m54B1F0894B64A8C1CE9B15094BC583E24CD8327F,
	EyeLevelSceneOrigin__ctor_mB9D65067BD4ED7424ACE6EF387F3EE3AD26F17BD,
	HandMeshTracker_get_Left_m834FB22088CF46F9EF46DAC8731E6DF270106FAF,
	HandMeshTracker_get_Right_m6705D8B2C8AB6513519C4B8D8738FE36DFC33731,
	HandMeshTracker__ctor_m96AEFA60A8B55A54D6A1992AE14935E6B54E3CCA,
	HandMeshTracker_TryLocateHandMesh_mF8FED4EFDF402198B60E71BD7E6D07AF73BE16B5,
	HandMeshTracker_TryGetHandMesh_m47B073A5300E55CC93A59C05DD28EF96C7739566,
	HandMeshTracker__cctor_m395D1B0978E9CDAF41F91885D7F0B3E60537C65C,
	HandTracker_get_Left_m708C38E9BC40B0FBEB07E9163C3148E8007A1A7F,
	HandTracker_get_Right_m65535255C8267CCABFFDD58A6BE59F4DBCCAB159,
	HandTracker__ctor_mFC00C937914EEA49539F9B8594FB66E72EFA3C86,
	HandTracker_TryLocateHandJoints_mB8FE6A877DE60EF653018450D6157F8205CB71D4,
	HandTracker__cctor_mF1E48FB95599D8D57993209156F006B4118CD5DC,
	HandJointLocation_get_IsTracked_m528CFAA9AD0EE23A6A17CCD1C6C57AA86E69E4FA,
	HandJointLocation_get_Pose_mEC69083D5F01D41D2737848B69B570C88771B652,
	HandJointLocation_get_Radius_m59C2EE4AC8714AF1583CD70EA7CD5A68847C726F,
	OpenXRContext_get_Current_m2C50D894964F4BC2F6C43F5BED56A79EA79344A9,
	OpenXRContext_get_Instance_mB67910BD8536AA7CF32EF4C87C2C1759DC957360,
	OpenXRContext_get_SystemId_m176A221E49277FC64F92D66A7B2A61B0B0577F75,
	OpenXRContext_get_Session_m5363D24082DC44974022573E80C77DE43D19AA74,
	OpenXRContext_get_SceneOriginSpace_m6BFD1AE8537A39EBAB77FF0176CDC44780363C91,
	OpenXRContext_GetInstanceProcAddr_m2514E8BFBC0D814FDF0BB717EDA6E3955E4E32D8,
	OpenXRContext__ctor_m3E03CDBC2ED10CAAF58D5F6C1222503E1663FE51,
	OpenXRContext__cctor_mE94747D7A8788E3741302B0C527C2B0D849F561D,
	PerceptionInterop_GetSceneCoordinateSystem_m154FE304CA6748BEE3B793838BC064B0AC99C45C,
	SpatialGraphNode_FromStaticNodeId_m52DFF9B952A4AF999931FDF7704A53F3D16C2A1D,
	SpatialGraphNode_get_Id_m96236885EF759340AC6EE05414F163834A7268CD,
	SpatialGraphNode_set_Id_mF0E64EED4BA607D324C97A2D08183EC05AE760EC,
	SpatialGraphNode_TryLocate_mD93733F219487C8988DEE38AC0C926DF17A76380,
	SpatialGraphNode__ctor_m2D204450B9B811F0496D51322D7D8A3FFA55BDB6,
	XRAnchorStore_get_PersistedAnchorNames_mD73343D81FDABEA8AF20B190C0572D499FDC63C3,
	XRAnchorStore_LoadAnchor_m336CC9B5F6E9D72CBBD435E824B6684670F0C015,
	XRAnchorStore_TryPersistAnchor_m1A5584B25D7BB8F2AACCE6A466556F7C94E7691A,
	XRAnchorStore_UnpersistAnchor_m6F63B1F3D8E2406FEF4FBA4FD8E720EAD4F998CF,
	XRAnchorStore_Clear_m10AE0501361553F7F3598F95AFE026CE9DDFFECF,
	XRAnchorStore_LoadAsync_m822582D492817B68C688B330A1826EFC96EDA38D,
	XRAnchorStore__ctor_m219B5DAF9949C2BB75DDBD45FF1D0BF250055BD7,
	U3CLoadAsyncU3Ed__6_MoveNext_mA1103540F922A4061730833EAC5FA4F971FD4056,
	U3CLoadAsyncU3Ed__6_SetStateMachine_m3DB639B56B9ADE4D2CF850C4968E94B21C254D42,
	HPMixedRealityControllerProfile_RegisterDeviceLayout_m643E010D432A18B71C0238E9A27B556F86652723,
	HPMixedRealityControllerProfile_UnregisterDeviceLayout_m1C6B5C13FD59E7233D99A1CD4A73D486013057E6,
	HPMixedRealityControllerProfile_RegisterActionMapsWithRuntime_mFB00DEBB805C8DF7A6CDA38C08D125DE7526A130,
	HPMixedRealityControllerProfile__ctor_m10B69791512D9F93E7B2C028EC47A30E65121FF8,
	HPMixedRealityController_get_thumbstick_m90CA4687E93563B0DEB602449DAE1FFFC911F62C,
	HPMixedRealityController_set_thumbstick_mB63719233F55DCED55B0DEFE3F5D893D8D7B989C,
	HPMixedRealityController_get_grip_mCE781A33AAF069E177C6EF5100164BD66236C810,
	HPMixedRealityController_set_grip_m80A88B6E2D47AD5CC2722B7E10B4185FA96696DC,
	HPMixedRealityController_get_gripPressed_mEA1F4C574F8BB5A1D1A44FA98FD43AB5F52EE1B4,
	HPMixedRealityController_set_gripPressed_m862349937A453211DF89235CDE719DFDC01DDC0E,
	HPMixedRealityController_get_menu_mC0A16B388913E40C8EED70AE5F8CE640957F706A,
	HPMixedRealityController_set_menu_mF0C460935A0A1C28FE859EF75BB5FC3E2140F691,
	HPMixedRealityController_get_primaryButton_m378BDF8328E8A6DE3B0A33C985A464949669D8B8,
	HPMixedRealityController_set_primaryButton_mEA85F1585A82C77FA5693E33D0F7996783BCEA44,
	HPMixedRealityController_get_secondaryButton_m426DCE2368DD70DDFC5B3D0F67F9D6A70D0525DD,
	HPMixedRealityController_set_secondaryButton_m70E3935734AC36C792D1A54AFFDCF1C429E0353C,
	HPMixedRealityController_get_trigger_mF66176140D4592DE7122078E6242464EC87F63C8,
	HPMixedRealityController_set_trigger_m5E824031767A7B4E03A4DDD6A73C590D414A3229,
	HPMixedRealityController_get_triggerPressed_m8ED9173BF9C6028DD0970D25D15AC87DEAF7DA45,
	HPMixedRealityController_set_triggerPressed_m743356C12B8EFD746028482D7E57F5513914D3CE,
	HPMixedRealityController_get_thumbstickClicked_m528DA9346A052CB8CF6D6BFBDE6AD670CC3A4846,
	HPMixedRealityController_set_thumbstickClicked_mF3DC1E0A64B894C96E88ACEDC198EB1769D8B9F1,
	HPMixedRealityController_get_devicePose_m4CA8FBCD335A15BFC9C1D2DFAC011CCB7FF6D9C7,
	HPMixedRealityController_set_devicePose_mC0EAE470D05077DF7492B88D1A0549458217EA59,
	HPMixedRealityController_get_pointer_m3FBCC9B7054E2FE2C0B78C8EB6B7D68330D42BC0,
	HPMixedRealityController_set_pointer_mD3E0295F051BB52FD90B7C704C1EF3D509F78466,
	HPMixedRealityController_get_isTracked_m6DEA5B5F2EECC0129E12B53ECF10812A570125FC,
	HPMixedRealityController_set_isTracked_mF02911DC0F1C1E2D6C8EDF61E4D7DEAE9CF85B5C,
	HPMixedRealityController_get_trackingState_mAF48A26032D476F52E4538464FA9183F5CB8E1BC,
	HPMixedRealityController_set_trackingState_m6387B052D3C16EDB339858D9E5B38EA1028975CA,
	HPMixedRealityController_get_devicePosition_m684527E5770BAAEE45AFED473D4DACE9F5061B96,
	HPMixedRealityController_set_devicePosition_m448F72744C9775C8F30D2865077F87A5A7E80011,
	HPMixedRealityController_get_deviceRotation_mDDDAD598D77716DB2322BFB0E9E7FC86A19FCFF9,
	HPMixedRealityController_set_deviceRotation_mF9F5F1DEBF0B7D67F3B41E68E9288AE1A2349160,
	HPMixedRealityController_get_pointerPosition_m494846ED88B2D8FE279793989489381FDD3F2060,
	HPMixedRealityController_set_pointerPosition_m9E7EEC6DF082911E9BA713EB47E901118FF1EE52,
	HPMixedRealityController_get_pointerRotation_m9B28C8E1D41987CFBC8AA2594EC7BD3BA47566C7,
	HPMixedRealityController_set_pointerRotation_m96F55CDCC866F08C06204FE1445AC66E24D79CF1,
	HPMixedRealityController_FinishSetup_m0BF2BC2EB2C115DBA7EE6117CE455BCB0FE22D1C,
	HPMixedRealityController__ctor_m704F1B3A4B5E78F13EA461FA17C8E3F03C869F81,
	HandTrackingFeaturePlugin__ctor_mA99DA099D30237012B5FC959177DA41C5188360C,
	HoloLensFeaturePlugin__ctor_mAA52F59E8A11A9ADD7C3409D97E1BC6392E9433C,
	HoloLensFeaturePlugin_TryAcquireSceneCoordinateSystem_m8AA8C79A1063C9C15CABC9955679256C0C013568,
	HoloLensFeaturePlugin_TryAcquirePerceptionSpatialAnchor_m1F8DCCEE788B18D068B6A5CFF8CD8718D0F2233B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FeatureUtils_ToTrackableId_m7C58515C6BE66FDF0FE0D6CE9DE7FD8575CD6201,
	FeatureUtils_ToGuid_m4237B8C32661BD32DED6C8AE49B74B564CBDC657,
	NativeLibTokenAttribute_get_NativeLibToken_mC21339A02A50AAF0A09469B71CE8F598281B18DF,
	NativeLibTokenAttribute_set_NativeLibToken_mCF242DA8234A359AC667DFB290D6E3274ECE7F1C,
	NativeLibTokenAttribute__ctor_m50A60218A276DE4E6F1880378DF7B2DA51FB3CDF,
	NativeLib_InitializeNativeLibToken_mFB184561FD20A6D28015A889A2822E5478A79EA7,
	NativeLib_HookGetInstanceProcAddr_m157ADD4B8EB9E3E3AAF58656B5039847AF021EFC,
	NativeLib_GetInstanceProcAddr_m40438AEACC92223D1ABBD04706B146F8067E6015,
	NativeLib_SetXrInstance_mA620691013E77E93076C5A7B09C2C743D3911E0F,
	NativeLib_SetXrSystemId_mEC2D002C63EF165D9E5CAA82CB618AFCA783849E,
	NativeLib_SetXrSession_m0648545CBF17F451692243EF96C3F65392C6109F,
	NativeLib_SetXrSessionRunning_mD1525E58239D377BF78EC8DC8D53241224BDCD77,
	NativeLib_SetSessionState_m9CADC5BB9186E443906C0BC64F7B5D54ACACE52A,
	NativeLib_SetSceneOriginSpace_m4CC1782569AF1074272FAA17C0356D2D5849C173,
	NativeLib_CreateSceneUnderstandingProvider_m4D9CA492647E1799956EE24DB5B0423A0A8C0674,
	NativeLib_SetSceneUnderstandingActive_mB7D4A4371E6C8E924ECC97402C9038EE7DA4E795,
	NativeLib_SetPlaneDetectionMode_m98AF20BC0B12F704420663B9F2D73351786088C9,
	NativeLib_GetNumPlaneChanges_m5C186E6181A991D9FCA39AEC70760C3644B0C97E,
	NativeLib_GetPlaneChanges_mF946EDC685918A4A4B05FD89E915D8312FAFA7A1,
	NativeLib_CreateAnchorProvider_mD62A08A0A1E787428F806AF73A0B0B2989F95D5D,
	NativeLib_TryAddAnchor_mA1B54BF7D924E99565359BFDE0425FEFFF72E57B,
	NativeLib_TryRemoveAnchor_m8B8E983FC1A25AC44C563D66BE3DFB4DDD3AF55C,
	NativeLib_GetNumAnchorChanges_mD77D5E7A502379543D83FCFAE6C0AC925E9700D4,
	NativeLib_GetAnchorChanges_mC56A3A5CC5F6792C4E9F4B59BD36366BE136A62F,
	NativeLib_LoadAnchorStore_mEC3B001BAE71458407293CD87F652BCB1A5496BD,
	NativeLib_GetNumPersistedAnchorNames_mA61982B0515B67E1370DBF04BBAA043DB81BA236,
	NativeLib_GetPersistedAnchorName_m68291776F7CB8EEFF75223B029F1CA9541B93788,
	NativeLib_LoadPersistedAnchor_mC98529DE4118DB40E19CB54D4FAD72F4A3BACFF0,
	NativeLib_TryPersistAnchor_mA2C9DAC7FFA0293D42BB711A11A59C1C2D9DD113,
	NativeLib_UnpersistAnchor_mD0C43A58B2225C648FBAFD2D5C58EFE97CA3C96C,
	NativeLib_ClearPersistedAnchors_m2602D238B9735D76818D1504F2E1FDA6DBC581EF,
	NativeLib_GetHandJointData_m76B412D5FA3FE1DEC7C40D1F23A2A49E810E781E,
	NativeLib_TryLocateHandMesh_mC8563FBFBF75F58C7197614DF63E8EBD48981982,
	NativeLib_TryGetHandMesh_mB2B2F2FF7A0A9E961040D0D222248F981AC7A371,
	NativeLib_TryGetHandMeshBufferSizes_mA703D940C61C162089F1E5A355495204D9F18DC2,
	NativeLib_SetRemotingEnabled_m4E5E5DAAD3425AE86D8A5FC22A1E1D2720D696C5,
	NativeLib_ConnectRemoting_mDF7AD65315C924C43474A2FB2AF8E048BAF59413,
	NativeLib_DisconnectRemoting_m10705E91DFD8747170EB64B959352FD565894E2B,
	NativeLib_TryGetRemotingConnectionState_m485B3E4A115182AB90075C494B7C61E9D79BA715,
	NativeLib_TryCreateSpaceFromStaticNodeId_m94695049AA25F010AA2E0F03402F1AAEAE30D7A2,
	NativeLib_TryLocateSpace_m251737A8AC075318D0E63523E819FA62CCD310E0,
	NativeLib_TryAcquireSceneCoordinateSystem_m969BE2F38C8E3D35AFE327D1C826C3C8BCF6C257,
	NativeLib_TryAcquirePerceptionSpatialAnchor_mBF622FA52886BAD6AE653E4C2890303ACA7B72E2,
	NativeLib__ctor_m95FA93BF8531F3C1AFF3CEAF0B19D894C1AC0223,
	OpenXRAnchorStoreFactory_LoadAnchorStoreAsync_m573D135B96906B05C625C6396743124EF7517BA8,
	OpenXRAnchorStoreFactory__cctor_m59748FB5E2E0629749C60A3636FDA765EE0401C7,
	U3CU3Ec__cctor_m3088AC00990AA49150B0BDBD07FB097EC693CE3B,
	U3CU3Ec__ctor_mDF746EA21581AEA5E95ED46C9F512DEC179B39CF,
	U3CU3Ec_U3CLoadAnchorStoreAsyncU3Eb__1_0_m744C3D925614EE8875DD036FCD9C7E4CA9759E5C,
	OpenXRAnchorStore__ctor_m74886FF418C3C6A5CB05A930210901FCF72111BE,
	OpenXRAnchorStore_get_PersistedAnchorNames_m3E634306C2C615A387B0DAA9E9E49B1BD4440FF6,
	OpenXRAnchorStore_UpdatePersistedAnchorNames_mF5D13CEBAE7DA85448E6A3982F06DB7096C08A15,
	OpenXRAnchorStore_LoadAnchor_m72A6B4AC424B841AC7C61ACE09A3DE1349D24564,
	OpenXRAnchorStore_TryPersistAnchor_m58A011F594DA2FD4E91FF4D62A7E11F185E8CF66,
	OpenXRAnchorStore_UnpersistAnchor_mAFAD6E56BB5089AD9E565FEB51083880FF3E5595,
	OpenXRAnchorStore_Clear_m62B06197D9C72DA1C5B0627B401BD3E96E573AC3,
	OpenXRAnchorStore__cctor_m72938B00A3A2590791AF3ADC8695D46D6DFD9E01,
	AnchorSubsystem_RegisterDescriptor_mD1EA19E524C831940D55D6E869398D7467743A5F,
	AnchorSubsystem__ctor_m84546ED42C1B06B7B4D72FB3FB47195452F771D4,
	OpenXRProvider__ctor_m8B50C78F370DAA1BF1B94683917396050A01A96D,
	OpenXRProvider_Destroy_m34E0310FDCD2421B7D4FCD4B5C42D32AF42F9331,
	OpenXRProvider_Start_m8BA1EC9D384E0BE3365A7E6F9544A94859238D01,
	OpenXRProvider_Stop_mF4232DBA5EB4BA42AC88F526A68F39E359B65829,
	OpenXRProvider_GetChanges_m5612871362052FB23935AD3382E5CBE4A280A83D,
	OpenXRProvider_ToXRAnchor_m15F91E24F867D3EB630EE93853319A5C46B674EA,
	OpenXRProvider_TryAddAnchor_m559F71D34E599191F0E30D5F0A95121A6FA95491,
	OpenXRProvider_TryAttachAnchor_mFB175010BF63CED9F3AD51EEF933449F0BD8CFB9,
	OpenXRProvider_TryRemoveAnchor_m3E22FC896FF84E1F4B4BE1A32725E7DD0033587E,
	AnchorSubsystemController__ctor_m4F7FF65F4F884DF2B91A47C88F5159F865745693,
	AnchorSubsystemController_OnSubsystemCreate_m4E4B001545FA3B7C75C7F0365023266A9566D1EB,
	AnchorSubsystemController_OnSubsystemStart_m3C680BC1B621CEADFA4DCEEF77AF8799EE76855E,
	AnchorSubsystemController_OnSubsystemStop_m8385EF365649CCB657739CAEA18C0CBEE4C53FBB,
	AnchorSubsystemController_OnSubsystemDestroy_m6D606C537DD6B43E206A337EBCEB997ABCEF244D,
	AnchorSubsystemController__cctor_mB38DC6BA1853A59A4D217430605D9324003D6A95,
	HandTrackingSubsystemController__ctor_m10AA76E56772014EA9B556FCC46B006D127791EF,
	HandTrackingSubsystemController_OnSubsystemCreate_m6B6EFDA87453531D625F53271674847E84E5A35A,
	HandTrackingSubsystemController_OnSubsystemStart_mB486E6F094B18692C07971C4699F04B52FF8160C,
	HandTrackingSubsystemController_OnSubsystemStop_mFE10DEB260BAA3D3DED0F59ECC9AA5BE8A993528,
	HandTrackingSubsystemController_OnSubsystemDestroy_m8ACF797438DCFABC73DB56B85224926ECF632F28,
	MeshSubsystemController__ctor_m2FCF91093A5509CBA095BF858E4501824DFB7EB7,
	MeshSubsystemController_OnSubsystemCreate_m7E6487221D11E16CC2E16F4042749101AC6C642C,
	MeshSubsystemController_OnSubsystemDestroy_m390EF07D9F45BD946A8A28560B730814472A64D2,
	MeshSubsystemController__cctor_m8D8FAA79E5AB045B232BB66E796655B2B5B1A2B4,
	PlaneSubsystem_RegisterDescriptor_mC49177E583BFA41281C5F20A9A3C3EB7E5929114,
	PlaneSubsystem__ctor_m4E30B58BA23643D867632DBC1FF8B6F2DFBD90E5,
	OpenXRProvider__ctor_m33CF547778A8A43FA9F57EB88206B9310CDD4888,
	OpenXRProvider_Start_m9CD935BA3200A3130CD96A1A0B1DE4C9FC22E407,
	OpenXRProvider_Stop_m38A55D0BAE49F7D000397E458633B075B22B8B4E,
	OpenXRProvider_Destroy_m0B72F6321683F87DC37E48D08E55420E0352304D,
	OpenXRProvider_get_currentPlaneDetectionMode_m8E928963BFEC07256214C098588F7348D33EBDE2,
	OpenXRProvider_get_requestedPlaneDetectionMode_m6701E8BE54B57E1011C7FD5794A7A7F84C44230E,
	OpenXRProvider_set_requestedPlaneDetectionMode_m66DEF51C5B0D6E05A088CFAB180C17C8FBD33026,
	OpenXRProvider_GetChanges_m0BFC0C66812A3BEBC36594FD8F6AD582A1D9CB7C,
	OpenXRProvider_ToPlaneClassification_m63AA9EC46CF4DD00CF716707D54772A90B19C61E,
	OpenXRProvider_ToBoundedPlane_m22B24059DBA52B5CDCC9659554CF5E49282D4423,
	PlaneSubsystemController__ctor_m476572A035D5FC9A24559E1C557410C137334A8D,
	PlaneSubsystemController_OnSubsystemCreate_m2F3B4AEE4441D13FDD2CE398B43A8202F19249BE,
	PlaneSubsystemController_OnSubsystemDestroy_m06F036C60B18A23CCE65B1E7D54B18C73AA0BF39,
	PlaneSubsystemController__cctor_m77D8EBD4728640503E17AB1ABF91BDE440EB83E4,
	RaycastSubsystem_RegisterDescriptor_mDFFDCFFE9613F761ABF930299B6E11011BDEDBF9,
	RaycastSubsystem__ctor_mA4F69055FB87DDA90367FFCF63DF84FEA87E808A,
	OpenXRProvider_TryAddRaycast_mB48E29D969B1CE6D31EE955C12D95FB2EFF1DE96,
	OpenXRProvider_TryAddRaycast_m6D4C27ECB9FC547CB35DFF38C600E3D1F8AF9151,
	OpenXRProvider_RemoveRaycast_mD6A70356FD0287EC2F3B228CAB093133BEBDAFBE,
	OpenXRProvider__ctor_m1AE2C017B4ED0990DB8F3EA9B8BC39732C6561A6,
	RaycastSubsystemController__ctor_m9E585309950D8B13F2D3EA50F073901987A31110,
	RaycastSubsystemController_OnSubsystemCreate_m3A1E02241704B9D5B27424BAA768DDE18E5E6F43,
	RaycastSubsystemController_OnSubsystemDestroy_m5BA7E4A22EC1DE0485D061BE00168C106F7FB298,
	RaycastSubsystemController__cctor_m41E20C1FF5E28DF73888EBB5E57B6DF27EA237C8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SubsystemController__ctor_mE34F00D01DC143D97F415AB978C7D33C249B8C52,
	SubsystemController_OnSubsystemCreate_m396D9B3FD8B2ED051C4D06791EC59776FD015A18,
	SubsystemController_OnSubsystemStart_m2E0C8E95A753022A97FB798636D0EE0AB0AC1DE6,
	SubsystemController_OnSubsystemStop_mC887515BE16532B457067DD0A8D9CD5E0E405A63,
	SubsystemController_OnSubsystemDestroy_m95CD8DCE8ABFD005A7B819C60A0522057A882EC8,
	HandTracker__ctor_m8A233C7E4A16C0C05450FA276C2151B1A58A0A3D,
	HandTracker_TryLocateHandJoints_m1043ECB88351DA411C4CAA43BA9C336D98BC3DEC,
	HandJointLocation_get_PoseFlags_m1CD68F1D99A888442B52E06B2464692300714423,
	HandJointLocation_get_Rotation_mE08792490B53A7951D778D5C2F9F7F7B89494E3D,
	HandJointLocation_get_Position_mD2FACF4383DD4FC9EE45505113C56C09C6636042,
	HandJointLocation_get_Radius_mDCE67E74FD5000DD8235D62B0A014B661F42BF83,
	HandJointLocation__ctor_mDA21AEEBD97E42890321984B6B31E573A9A0E132,
	AppRemoting_get_Configuration_m518C2AFDA11E111274D91A3877515F2EE21B93A3,
	AppRemoting_set_Configuration_mA37F1AC0EE17FCA0DFD43484FA0608E0D203DFD1,
	AppRemoting_Connect_mFDF9ACF483419C1C1D1072B7B4E60AC4D714A3CF,
	AppRemoting_Disconnect_m0F52220F566CCABD661D36D312329997DE95FA97,
	AppRemoting_TryGetConnectionState_mD1BA1EC2E4A11E6D62FB67DC4218F69A3358394D,
	U3CConnectU3Ed__4__ctor_m9B7169E748BDE0D567455598D91A91A7D2D60CA3,
	U3CConnectU3Ed__4_System_IDisposable_Dispose_mF2F8F709F0C863F20922D10016EF3769BF9CABA7,
	U3CConnectU3Ed__4_MoveNext_m623AFEFAF62DB7F23C1C4288ED8D4F464ECB5FBF,
	U3CConnectU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAEAED26889E7742A923273906179CB6DB875EF7D,
	U3CConnectU3Ed__4_System_Collections_IEnumerator_Reset_mCAB5E7A2915C5628DF53ABD73DD21D5536C5A493,
	U3CConnectU3Ed__4_System_Collections_IEnumerator_get_Current_m5FC03290D91E6AC38AFA1D30CD249EB00999BAAC,
	AppRemotingPlugin_HookGetInstanceProcAddr_mCE3FA55800CDF0365DD7619A59363B1C28B85705,
	AppRemotingPlugin_OnInstanceCreate_m19936D9D67E1ED10D7883A781B09C9BCC14E2D51,
	AppRemotingPlugin_OnInstanceDestroy_mC8471F0A053507D63236A43490D6E288840AE422,
	AppRemotingPlugin_OnSystemChange_m2C939F7BEF8D3C948548ADC2905CDCEE7AC58BB8,
	AppRemotingPlugin_OnSessionStateChange_m999AD7415BD4F2C2B6942921C52959CA416EAE08,
	AppRemotingPlugin__ctor_m41EF8B18529ABCF0EAA76A6D05CB9875CCFB4D2D,
	U3CU3Ec__cctor_mCEC795D0A26F437D463415A2BB6FBB67A5356A2E,
	U3CU3Ec__ctor_m9418F353D31FDE71C5354EB90B571268388FFDB0,
	U3CU3Ec_U3COnSystemChangeU3Eb__8_0_m7C5D2079DAD009BBB06E25CBB01DD510D2BDA3D6,
	EditorRemotingPlugin_HookGetInstanceProcAddr_m923D29C37BE4B348DFEBD6332CAC26F36A1EFC2F,
	EditorRemotingPlugin_OnInstanceCreate_m34F5AC8920D4896D3C5B896A81A739CA31FE40E5,
	EditorRemotingPlugin_OnInstanceDestroy_m1397B99CCBD8BF66B3F27336C8D271FAF64EC4CE,
	EditorRemotingPlugin_OnSystemChange_mD012D792DF758CAD0AC38DC9C9DA38B3547AB491,
	EditorRemotingPlugin_OnSessionStateChange_m7E613D18BADA7865C3ECFCB106AFC51518F04262,
	EditorRemotingPlugin_HasValidSettings_mB2CAC3F8CB424B0739284D685B733469927F91DD,
	EditorRemotingPlugin__ctor_m655BD77B2FD6453C717BDFB05C45D5DF39BF49C4,
	EditorRemotingPlugin_U3COnSystemChangeU3Eb__13_0_m2AD00416A38236601D9DC419598D621ABF7F64A7,
	ARAnchorExtensions_GetOpenXRHandle_m07C39102695713B89B09A0AC5930F96FE51A7A80,
	AnchorManagerExtensions_LoadAnchorStoreAsync_m7FFB9D7FFB415A46DDB8822EE8FE81A220056F7B,
	XRAnchorExtensions_GetOpenXRHandle_m3AB1B804A8570EA669D000A64CAC3B0BD1C08B94,
	AnchorSubsystemExtensions_LoadAnchorStoreAsync_m04BE110C74F6AEA0055C448C3C74C02EBACA231D,
};
extern void U3CLoadAsyncU3Ed__0_MoveNext_m2A1537A907B67894A1892E2913EF1E9B8403E614_AdjustorThunk (void);
extern void U3CLoadAsyncU3Ed__0_SetStateMachine_mB84B24F4974916FB514D4D019E083C7773C8ADE3_AdjustorThunk (void);
extern void HandJointLocation_get_IsTracked_m528CFAA9AD0EE23A6A17CCD1C6C57AA86E69E4FA_AdjustorThunk (void);
extern void HandJointLocation_get_Pose_mEC69083D5F01D41D2737848B69B570C88771B652_AdjustorThunk (void);
extern void HandJointLocation_get_Radius_m59C2EE4AC8714AF1583CD70EA7CD5A68847C726F_AdjustorThunk (void);
extern void U3CLoadAsyncU3Ed__6_MoveNext_mA1103540F922A4061730833EAC5FA4F971FD4056_AdjustorThunk (void);
extern void U3CLoadAsyncU3Ed__6_SetStateMachine_m3DB639B56B9ADE4D2CF850C4968E94B21C254D42_AdjustorThunk (void);
extern void HandJointLocation_get_PoseFlags_m1CD68F1D99A888442B52E06B2464692300714423_AdjustorThunk (void);
extern void HandJointLocation_get_Rotation_mE08792490B53A7951D778D5C2F9F7F7B89494E3D_AdjustorThunk (void);
extern void HandJointLocation_get_Position_mD2FACF4383DD4FC9EE45505113C56C09C6636042_AdjustorThunk (void);
extern void HandJointLocation_get_Radius_mDCE67E74FD5000DD8235D62B0A014B661F42BF83_AdjustorThunk (void);
extern void HandJointLocation__ctor_mDA21AEEBD97E42890321984B6B31E573A9A0E132_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[12] = 
{
	{ 0x06000005, U3CLoadAsyncU3Ed__0_MoveNext_m2A1537A907B67894A1892E2913EF1E9B8403E614_AdjustorThunk },
	{ 0x06000006, U3CLoadAsyncU3Ed__0_SetStateMachine_mB84B24F4974916FB514D4D019E083C7773C8ADE3_AdjustorThunk },
	{ 0x0600001A, HandJointLocation_get_IsTracked_m528CFAA9AD0EE23A6A17CCD1C6C57AA86E69E4FA_AdjustorThunk },
	{ 0x0600001B, HandJointLocation_get_Pose_mEC69083D5F01D41D2737848B69B570C88771B652_AdjustorThunk },
	{ 0x0600001C, HandJointLocation_get_Radius_m59C2EE4AC8714AF1583CD70EA7CD5A68847C726F_AdjustorThunk },
	{ 0x06000032, U3CLoadAsyncU3Ed__6_MoveNext_mA1103540F922A4061730833EAC5FA4F971FD4056_AdjustorThunk },
	{ 0x06000033, U3CLoadAsyncU3Ed__6_SetStateMachine_m3DB639B56B9ADE4D2CF850C4968E94B21C254D42_AdjustorThunk },
	{ 0x06000105, HandJointLocation_get_PoseFlags_m1CD68F1D99A888442B52E06B2464692300714423_AdjustorThunk },
	{ 0x06000106, HandJointLocation_get_Rotation_mE08792490B53A7951D778D5C2F9F7F7B89494E3D_AdjustorThunk },
	{ 0x06000107, HandJointLocation_get_Position_mD2FACF4383DD4FC9EE45505113C56C09C6636042_AdjustorThunk },
	{ 0x06000108, HandJointLocation_get_Radius_mDCE67E74FD5000DD8235D62B0A014B661F42BF83_AdjustorThunk },
	{ 0x06000109, HandJointLocation__ctor_mDA21AEEBD97E42890321984B6B31E573A9A0E132_AdjustorThunk },
};
static const int32_t s_InvokerIndices[297] = 
{
	6338,
	6338,
	3660,
	1616,
	4512,
	3660,
	6265,
	6336,
	4512,
	4512,
	3660,
	4512,
	6459,
	4512,
	6564,
	6564,
	3626,
	985,
	995,
	6597,
	6564,
	6564,
	3626,
	1590,
	6597,
	4465,
	4440,
	4472,
	6564,
	4396,
	4396,
	4396,
	4396,
	2749,
	4512,
	6597,
	6339,
	6331,
	4365,
	3597,
	1584,
	4512,
	4429,
	3365,
	1641,
	3660,
	4512,
	6338,
	3660,
	4512,
	3660,
	4512,
	4512,
	4512,
	4512,
	4429,
	3660,
	4429,
	3660,
	4429,
	3660,
	4429,
	3660,
	4429,
	3660,
	4429,
	3660,
	4429,
	3660,
	4429,
	3660,
	4429,
	3660,
	4429,
	3660,
	4429,
	3660,
	4429,
	3660,
	4429,
	3660,
	4429,
	3660,
	4429,
	3660,
	4429,
	3660,
	4429,
	3660,
	4512,
	4512,
	4512,
	4512,
	2750,
	2747,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	6427,
	6211,
	4396,
	3627,
	4512,
	6455,
	5739,
	5386,
	6019,
	6019,
	6019,
	6022,
	6018,
	6019,
	6455,
	6022,
	6018,
	4977,
	5282,
	6455,
	4920,
	5864,
	4977,
	5282,
	6455,
	6244,
	5283,
	5680,
	5470,
	6020,
	6455,
	5214,
	4919,
	4619,
	5466,
	6022,
	6021,
	6455,
	5466,
	5467,
	5215,
	5740,
	5738,
	4512,
	6338,
	6597,
	6597,
	4512,
	4429,
	4512,
	4429,
	4512,
	3365,
	1616,
	3660,
	4512,
	6597,
	6597,
	4512,
	4512,
	4512,
	4512,
	4512,
	1212,
	3836,
	1619,
	1044,
	3220,
	2067,
	3660,
	3660,
	3660,
	3660,
	6597,
	3660,
	3660,
	3660,
	3660,
	3660,
	2067,
	3660,
	3660,
	6597,
	6597,
	4512,
	4512,
	4512,
	4512,
	4512,
	4395,
	4395,
	3626,
	1211,
	2567,
	1216,
	2067,
	3660,
	3660,
	6597,
	6597,
	4512,
	1045,
	1035,
	3721,
	4512,
	2067,
	3660,
	3660,
	6597,
	4396,
	4396,
	4396,
	4395,
	4396,
	4465,
	-1,
	-1,
	-1,
	-1,
	3660,
	3660,
	3660,
	3660,
	3660,
	1902,
	1590,
	4396,
	4444,
	4507,
	4472,
	3599,
	6582,
	6461,
	6341,
	6597,
	5823,
	3626,
	4512,
	4465,
	4429,
	4512,
	4429,
	2748,
	3123,
	3627,
	3627,
	1902,
	4512,
	6597,
	4512,
	4512,
	2748,
	3123,
	3627,
	3627,
	1902,
	4465,
	4512,
	4512,
	6266,
	6338,
	6271,
	6338,
};
static const Il2CppTokenRangePair s_rgctxIndices[5] = 
{
	{ 0x02000017, { 0, 14 } },
	{ 0x0600007F, { 14, 1 } },
	{ 0x06000080, { 15, 1 } },
	{ 0x06000081, { 16, 1 } },
	{ 0x06000082, { 17, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[18] = 
{
	{ (Il2CppRGCTXDataType)3, 33341 },
	{ (Il2CppRGCTXDataType)1, 715 },
	{ (Il2CppRGCTXDataType)2, 7533 },
	{ (Il2CppRGCTXDataType)3, 33336 },
	{ (Il2CppRGCTXDataType)3, 33338 },
	{ (Il2CppRGCTXDataType)3, 33339 },
	{ (Il2CppRGCTXDataType)3, 33337 },
	{ (Il2CppRGCTXDataType)3, 33342 },
	{ (Il2CppRGCTXDataType)3, 33340 },
	{ (Il2CppRGCTXDataType)3, 33343 },
	{ (Il2CppRGCTXDataType)3, 33347 },
	{ (Il2CppRGCTXDataType)3, 33345 },
	{ (Il2CppRGCTXDataType)3, 33346 },
	{ (Il2CppRGCTXDataType)3, 33344 },
	{ (Il2CppRGCTXDataType)3, 47989 },
	{ (Il2CppRGCTXDataType)3, 47993 },
	{ (Il2CppRGCTXDataType)3, 47995 },
	{ (Il2CppRGCTXDataType)3, 47991 },
};
extern const CustomAttributesCacheGenerator g_Microsoft_MixedReality_OpenXR_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Microsoft_MixedReality_OpenXR_CodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_OpenXR_CodeGenModule = 
{
	"Microsoft.MixedReality.OpenXR.dll",
	297,
	s_methodPointers,
	12,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	5,
	s_rgctxIndices,
	18,
	s_rgctxValues,
	NULL,
	g_Microsoft_MixedReality_OpenXR_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
