﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E (void);
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2 (void);
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8 (void);
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_mB89E91246572F009281D79730950808F17C3F353 (void);
// 0x00000005 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000007 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000009 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000000A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000000B System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000C System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderByDescending(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000D System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::UnionIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000010 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Intersect(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000011 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::IntersectIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000012 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000013 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000014 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000015 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000016 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000017 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000018 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000019 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x0000001A System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001B System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001C System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001D System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001E System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000001F System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000020 TAccumulate System.Linq.Enumerable::Aggregate(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
// 0x00000021 System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Sum_m6CFC8CEAC70AE3C469A5D1993FAF8EEEC6A06FB5 (void);
// 0x00000022 System.Void System.Linq.Enumerable/Iterator`1::.ctor()
// 0x00000023 TSource System.Linq.Enumerable/Iterator`1::get_Current()
// 0x00000024 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1::Clone()
// 0x00000025 System.Void System.Linq.Enumerable/Iterator`1::Dispose()
// 0x00000026 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1::GetEnumerator()
// 0x00000027 System.Boolean System.Linq.Enumerable/Iterator`1::MoveNext()
// 0x00000028 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000029 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002A System.Object System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000002B System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000002C System.Void System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000002D System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000002E System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Clone()
// 0x0000002F System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::Dispose()
// 0x00000030 System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1::MoveNext()
// 0x00000031 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000032 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000033 System.Void System.Linq.Enumerable/WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Clone()
// 0x00000035 System.Boolean System.Linq.Enumerable/WhereArrayIterator`1::MoveNext()
// 0x00000036 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000037 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000038 System.Void System.Linq.Enumerable/WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000039 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Clone()
// 0x0000003A System.Boolean System.Linq.Enumerable/WhereListIterator`1::MoveNext()
// 0x0000003B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000003C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000003D System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000003E System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Clone()
// 0x0000003F System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Dispose()
// 0x00000040 System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000041 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000042 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000043 System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000044 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Clone()
// 0x00000045 System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2::MoveNext()
// 0x00000046 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000047 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000048 System.Void System.Linq.Enumerable/WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000049 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Clone()
// 0x0000004A System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2::MoveNext()
// 0x0000004B System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000004C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000004D System.Void System.Linq.Enumerable/<>c__DisplayClass6_0`1::.ctor()
// 0x0000004E System.Boolean System.Linq.Enumerable/<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000004F System.Void System.Linq.Enumerable/<>c__DisplayClass7_0`3::.ctor()
// 0x00000050 TResult System.Linq.Enumerable/<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000051 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x00000052 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x00000053 System.Boolean System.Linq.Enumerable/<TakeIterator>d__25`1::MoveNext()
// 0x00000054 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::<>m__Finally1()
// 0x00000055 TSource System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000056 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x00000057 System.Object System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x00000058 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000059 System.Collections.IEnumerator System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005A System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::.ctor(System.Int32)
// 0x0000005B System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::System.IDisposable.Dispose()
// 0x0000005C System.Boolean System.Linq.Enumerable/<UnionIterator>d__71`1::MoveNext()
// 0x0000005D System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::<>m__Finally1()
// 0x0000005E System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::<>m__Finally2()
// 0x0000005F TSource System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000060 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerator.Reset()
// 0x00000061 System.Object System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerator.get_Current()
// 0x00000062 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000063 System.Collections.IEnumerator System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000064 System.Void System.Linq.Enumerable/<IntersectIterator>d__74`1::.ctor(System.Int32)
// 0x00000065 System.Void System.Linq.Enumerable/<IntersectIterator>d__74`1::System.IDisposable.Dispose()
// 0x00000066 System.Boolean System.Linq.Enumerable/<IntersectIterator>d__74`1::MoveNext()
// 0x00000067 System.Void System.Linq.Enumerable/<IntersectIterator>d__74`1::<>m__Finally1()
// 0x00000068 TSource System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000069 System.Void System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.IEnumerator.Reset()
// 0x0000006A System.Object System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.IEnumerator.get_Current()
// 0x0000006B System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000006C System.Collections.IEnumerator System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000006D System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x0000006E System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000006F System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x00000070 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x00000071 System.Boolean System.Linq.Set`1::Remove(TElement)
// 0x00000072 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x00000073 System.Void System.Linq.Set`1::Resize()
// 0x00000074 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x00000075 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000076 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000077 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000078 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000079 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x0000007A System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::.ctor(System.Int32)
// 0x0000007B System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x0000007C System.Boolean System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::MoveNext()
// 0x0000007D TElement System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x0000007E System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x0000007F System.Object System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000080 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000081 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000082 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x00000083 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x00000084 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x00000085 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x00000086 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000087 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x00000088 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x00000089 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x0000008A System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x0000008B TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000008C System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x0000008D System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000008E System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000008F System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000090 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000091 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000092 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000093 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000094 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000095 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000096 System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000097 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000098 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000099 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000009A System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000009B System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x0000009C System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000009D System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000009E System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000009F System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000A0 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000A1 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000A2 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000A3 System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000A4 System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
// 0x000000A5 System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
// 0x000000A6 T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
// 0x000000A7 System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000A8 System.Void System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[168] = 
{
	Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E,
	Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2,
	Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8,
	Error_NoElements_mB89E91246572F009281D79730950808F17C3F353,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Sum_m6CFC8CEAC70AE3C469A5D1993FAF8EEEC6A06FB5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[168] = 
{
	6338,
	6338,
	6564,
	6564,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	6247,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[57] = 
{
	{ 0x02000004, { 87, 4 } },
	{ 0x02000005, { 91, 9 } },
	{ 0x02000006, { 102, 7 } },
	{ 0x02000007, { 111, 10 } },
	{ 0x02000008, { 123, 11 } },
	{ 0x02000009, { 137, 9 } },
	{ 0x0200000A, { 149, 12 } },
	{ 0x0200000B, { 164, 1 } },
	{ 0x0200000C, { 165, 2 } },
	{ 0x0200000D, { 167, 8 } },
	{ 0x0200000E, { 175, 12 } },
	{ 0x0200000F, { 187, 12 } },
	{ 0x02000010, { 199, 2 } },
	{ 0x02000012, { 201, 8 } },
	{ 0x02000014, { 209, 3 } },
	{ 0x02000015, { 214, 5 } },
	{ 0x02000016, { 219, 7 } },
	{ 0x02000017, { 226, 3 } },
	{ 0x02000018, { 229, 7 } },
	{ 0x02000019, { 236, 4 } },
	{ 0x0200001A, { 240, 21 } },
	{ 0x0200001C, { 261, 2 } },
	{ 0x06000005, { 0, 10 } },
	{ 0x06000006, { 10, 10 } },
	{ 0x06000007, { 20, 5 } },
	{ 0x06000008, { 25, 5 } },
	{ 0x06000009, { 30, 1 } },
	{ 0x0600000A, { 31, 2 } },
	{ 0x0600000B, { 33, 2 } },
	{ 0x0600000C, { 35, 2 } },
	{ 0x0600000D, { 37, 1 } },
	{ 0x0600000E, { 38, 1 } },
	{ 0x0600000F, { 39, 2 } },
	{ 0x06000010, { 41, 1 } },
	{ 0x06000011, { 42, 2 } },
	{ 0x06000012, { 44, 3 } },
	{ 0x06000013, { 47, 2 } },
	{ 0x06000014, { 49, 4 } },
	{ 0x06000015, { 53, 4 } },
	{ 0x06000016, { 57, 4 } },
	{ 0x06000017, { 61, 3 } },
	{ 0x06000018, { 64, 3 } },
	{ 0x06000019, { 67, 1 } },
	{ 0x0600001A, { 68, 1 } },
	{ 0x0600001B, { 69, 3 } },
	{ 0x0600001C, { 72, 3 } },
	{ 0x0600001D, { 75, 2 } },
	{ 0x0600001E, { 77, 2 } },
	{ 0x0600001F, { 79, 5 } },
	{ 0x06000020, { 84, 3 } },
	{ 0x06000031, { 100, 2 } },
	{ 0x06000036, { 109, 2 } },
	{ 0x0600003B, { 121, 2 } },
	{ 0x06000041, { 134, 3 } },
	{ 0x06000046, { 146, 3 } },
	{ 0x0600004B, { 161, 3 } },
	{ 0x06000078, { 212, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[263] = 
{
	{ (Il2CppRGCTXDataType)2, 6251 },
	{ (Il2CppRGCTXDataType)3, 24643 },
	{ (Il2CppRGCTXDataType)2, 9907 },
	{ (Il2CppRGCTXDataType)2, 9209 },
	{ (Il2CppRGCTXDataType)3, 42058 },
	{ (Il2CppRGCTXDataType)2, 6885 },
	{ (Il2CppRGCTXDataType)2, 9231 },
	{ (Il2CppRGCTXDataType)3, 42102 },
	{ (Il2CppRGCTXDataType)2, 9218 },
	{ (Il2CppRGCTXDataType)3, 42074 },
	{ (Il2CppRGCTXDataType)2, 6252 },
	{ (Il2CppRGCTXDataType)3, 24644 },
	{ (Il2CppRGCTXDataType)2, 9938 },
	{ (Il2CppRGCTXDataType)2, 9240 },
	{ (Il2CppRGCTXDataType)3, 42118 },
	{ (Il2CppRGCTXDataType)2, 6911 },
	{ (Il2CppRGCTXDataType)2, 9264 },
	{ (Il2CppRGCTXDataType)3, 42260 },
	{ (Il2CppRGCTXDataType)2, 9252 },
	{ (Il2CppRGCTXDataType)3, 42183 },
	{ (Il2CppRGCTXDataType)2, 1112 },
	{ (Il2CppRGCTXDataType)3, 99 },
	{ (Il2CppRGCTXDataType)3, 100 },
	{ (Il2CppRGCTXDataType)2, 3571 },
	{ (Il2CppRGCTXDataType)3, 15390 },
	{ (Il2CppRGCTXDataType)2, 1113 },
	{ (Il2CppRGCTXDataType)3, 109 },
	{ (Il2CppRGCTXDataType)3, 110 },
	{ (Il2CppRGCTXDataType)2, 3584 },
	{ (Il2CppRGCTXDataType)3, 15397 },
	{ (Il2CppRGCTXDataType)3, 47111 },
	{ (Il2CppRGCTXDataType)2, 1152 },
	{ (Il2CppRGCTXDataType)3, 278 },
	{ (Il2CppRGCTXDataType)2, 7546 },
	{ (Il2CppRGCTXDataType)3, 33526 },
	{ (Il2CppRGCTXDataType)2, 7547 },
	{ (Il2CppRGCTXDataType)3, 33527 },
	{ (Il2CppRGCTXDataType)3, 20188 },
	{ (Il2CppRGCTXDataType)3, 47145 },
	{ (Il2CppRGCTXDataType)2, 1155 },
	{ (Il2CppRGCTXDataType)3, 301 },
	{ (Il2CppRGCTXDataType)3, 47071 },
	{ (Il2CppRGCTXDataType)2, 1139 },
	{ (Il2CppRGCTXDataType)3, 230 },
	{ (Il2CppRGCTXDataType)2, 1453 },
	{ (Il2CppRGCTXDataType)3, 2650 },
	{ (Il2CppRGCTXDataType)3, 2651 },
	{ (Il2CppRGCTXDataType)2, 6886 },
	{ (Il2CppRGCTXDataType)3, 26714 },
	{ (Il2CppRGCTXDataType)2, 5397 },
	{ (Il2CppRGCTXDataType)2, 3811 },
	{ (Il2CppRGCTXDataType)2, 4046 },
	{ (Il2CppRGCTXDataType)2, 4344 },
	{ (Il2CppRGCTXDataType)2, 5398 },
	{ (Il2CppRGCTXDataType)2, 3812 },
	{ (Il2CppRGCTXDataType)2, 4047 },
	{ (Il2CppRGCTXDataType)2, 4345 },
	{ (Il2CppRGCTXDataType)2, 5399 },
	{ (Il2CppRGCTXDataType)2, 3813 },
	{ (Il2CppRGCTXDataType)2, 4048 },
	{ (Il2CppRGCTXDataType)2, 4346 },
	{ (Il2CppRGCTXDataType)2, 4049 },
	{ (Il2CppRGCTXDataType)2, 4347 },
	{ (Il2CppRGCTXDataType)3, 15391 },
	{ (Il2CppRGCTXDataType)2, 5396 },
	{ (Il2CppRGCTXDataType)2, 4045 },
	{ (Il2CppRGCTXDataType)2, 4343 },
	{ (Il2CppRGCTXDataType)2, 2466 },
	{ (Il2CppRGCTXDataType)2, 4031 },
	{ (Il2CppRGCTXDataType)2, 4032 },
	{ (Il2CppRGCTXDataType)2, 4341 },
	{ (Il2CppRGCTXDataType)3, 15389 },
	{ (Il2CppRGCTXDataType)2, 4030 },
	{ (Il2CppRGCTXDataType)2, 4340 },
	{ (Il2CppRGCTXDataType)3, 15388 },
	{ (Il2CppRGCTXDataType)2, 3810 },
	{ (Il2CppRGCTXDataType)2, 4044 },
	{ (Il2CppRGCTXDataType)2, 3809 },
	{ (Il2CppRGCTXDataType)3, 47037 },
	{ (Il2CppRGCTXDataType)3, 14230 },
	{ (Il2CppRGCTXDataType)2, 3377 },
	{ (Il2CppRGCTXDataType)2, 4034 },
	{ (Il2CppRGCTXDataType)2, 4342 },
	{ (Il2CppRGCTXDataType)2, 4570 },
	{ (Il2CppRGCTXDataType)2, 4073 },
	{ (Il2CppRGCTXDataType)2, 4353 },
	{ (Il2CppRGCTXDataType)3, 15598 },
	{ (Il2CppRGCTXDataType)3, 24645 },
	{ (Il2CppRGCTXDataType)3, 24647 },
	{ (Il2CppRGCTXDataType)2, 825 },
	{ (Il2CppRGCTXDataType)3, 24646 },
	{ (Il2CppRGCTXDataType)3, 24655 },
	{ (Il2CppRGCTXDataType)2, 6255 },
	{ (Il2CppRGCTXDataType)2, 9219 },
	{ (Il2CppRGCTXDataType)3, 42075 },
	{ (Il2CppRGCTXDataType)3, 24656 },
	{ (Il2CppRGCTXDataType)2, 4128 },
	{ (Il2CppRGCTXDataType)2, 4395 },
	{ (Il2CppRGCTXDataType)3, 15404 },
	{ (Il2CppRGCTXDataType)3, 47003 },
	{ (Il2CppRGCTXDataType)2, 9253 },
	{ (Il2CppRGCTXDataType)3, 42184 },
	{ (Il2CppRGCTXDataType)3, 24648 },
	{ (Il2CppRGCTXDataType)2, 6254 },
	{ (Il2CppRGCTXDataType)2, 9210 },
	{ (Il2CppRGCTXDataType)3, 42059 },
	{ (Il2CppRGCTXDataType)3, 15403 },
	{ (Il2CppRGCTXDataType)3, 24649 },
	{ (Il2CppRGCTXDataType)3, 47002 },
	{ (Il2CppRGCTXDataType)2, 9241 },
	{ (Il2CppRGCTXDataType)3, 42119 },
	{ (Il2CppRGCTXDataType)3, 24662 },
	{ (Il2CppRGCTXDataType)2, 6256 },
	{ (Il2CppRGCTXDataType)2, 9232 },
	{ (Il2CppRGCTXDataType)3, 42103 },
	{ (Il2CppRGCTXDataType)3, 26775 },
	{ (Il2CppRGCTXDataType)3, 12287 },
	{ (Il2CppRGCTXDataType)3, 15405 },
	{ (Il2CppRGCTXDataType)3, 12286 },
	{ (Il2CppRGCTXDataType)3, 24663 },
	{ (Il2CppRGCTXDataType)3, 47004 },
	{ (Il2CppRGCTXDataType)2, 9265 },
	{ (Il2CppRGCTXDataType)3, 42261 },
	{ (Il2CppRGCTXDataType)3, 24676 },
	{ (Il2CppRGCTXDataType)2, 6258 },
	{ (Il2CppRGCTXDataType)2, 9255 },
	{ (Il2CppRGCTXDataType)3, 42186 },
	{ (Il2CppRGCTXDataType)3, 24677 },
	{ (Il2CppRGCTXDataType)2, 4131 },
	{ (Il2CppRGCTXDataType)2, 4398 },
	{ (Il2CppRGCTXDataType)3, 15409 },
	{ (Il2CppRGCTXDataType)3, 15408 },
	{ (Il2CppRGCTXDataType)2, 9221 },
	{ (Il2CppRGCTXDataType)3, 42077 },
	{ (Il2CppRGCTXDataType)3, 47010 },
	{ (Il2CppRGCTXDataType)2, 9254 },
	{ (Il2CppRGCTXDataType)3, 42185 },
	{ (Il2CppRGCTXDataType)3, 24669 },
	{ (Il2CppRGCTXDataType)2, 6257 },
	{ (Il2CppRGCTXDataType)2, 9243 },
	{ (Il2CppRGCTXDataType)3, 42121 },
	{ (Il2CppRGCTXDataType)3, 15407 },
	{ (Il2CppRGCTXDataType)3, 15406 },
	{ (Il2CppRGCTXDataType)3, 24670 },
	{ (Il2CppRGCTXDataType)2, 9220 },
	{ (Il2CppRGCTXDataType)3, 42076 },
	{ (Il2CppRGCTXDataType)3, 47009 },
	{ (Il2CppRGCTXDataType)2, 9242 },
	{ (Il2CppRGCTXDataType)3, 42120 },
	{ (Il2CppRGCTXDataType)3, 24683 },
	{ (Il2CppRGCTXDataType)2, 6259 },
	{ (Il2CppRGCTXDataType)2, 9267 },
	{ (Il2CppRGCTXDataType)3, 42263 },
	{ (Il2CppRGCTXDataType)3, 26776 },
	{ (Il2CppRGCTXDataType)3, 12289 },
	{ (Il2CppRGCTXDataType)3, 15411 },
	{ (Il2CppRGCTXDataType)3, 15410 },
	{ (Il2CppRGCTXDataType)3, 12288 },
	{ (Il2CppRGCTXDataType)3, 24684 },
	{ (Il2CppRGCTXDataType)2, 9222 },
	{ (Il2CppRGCTXDataType)3, 42078 },
	{ (Il2CppRGCTXDataType)3, 47011 },
	{ (Il2CppRGCTXDataType)2, 9266 },
	{ (Il2CppRGCTXDataType)3, 42262 },
	{ (Il2CppRGCTXDataType)3, 15401 },
	{ (Il2CppRGCTXDataType)3, 15402 },
	{ (Il2CppRGCTXDataType)3, 15412 },
	{ (Il2CppRGCTXDataType)3, 280 },
	{ (Il2CppRGCTXDataType)2, 4120 },
	{ (Il2CppRGCTXDataType)2, 4389 },
	{ (Il2CppRGCTXDataType)3, 282 },
	{ (Il2CppRGCTXDataType)2, 821 },
	{ (Il2CppRGCTXDataType)2, 1153 },
	{ (Il2CppRGCTXDataType)3, 279 },
	{ (Il2CppRGCTXDataType)3, 281 },
	{ (Il2CppRGCTXDataType)3, 303 },
	{ (Il2CppRGCTXDataType)3, 304 },
	{ (Il2CppRGCTXDataType)2, 8561 },
	{ (Il2CppRGCTXDataType)3, 38773 },
	{ (Il2CppRGCTXDataType)2, 4123 },
	{ (Il2CppRGCTXDataType)2, 4391 },
	{ (Il2CppRGCTXDataType)3, 38774 },
	{ (Il2CppRGCTXDataType)3, 306 },
	{ (Il2CppRGCTXDataType)2, 823 },
	{ (Il2CppRGCTXDataType)2, 1156 },
	{ (Il2CppRGCTXDataType)3, 302 },
	{ (Il2CppRGCTXDataType)3, 305 },
	{ (Il2CppRGCTXDataType)3, 232 },
	{ (Il2CppRGCTXDataType)2, 8559 },
	{ (Il2CppRGCTXDataType)3, 38770 },
	{ (Il2CppRGCTXDataType)2, 4117 },
	{ (Il2CppRGCTXDataType)2, 4387 },
	{ (Il2CppRGCTXDataType)3, 38771 },
	{ (Il2CppRGCTXDataType)3, 38772 },
	{ (Il2CppRGCTXDataType)3, 234 },
	{ (Il2CppRGCTXDataType)2, 819 },
	{ (Il2CppRGCTXDataType)2, 1140 },
	{ (Il2CppRGCTXDataType)3, 231 },
	{ (Il2CppRGCTXDataType)3, 233 },
	{ (Il2CppRGCTXDataType)2, 9953 },
	{ (Il2CppRGCTXDataType)2, 2467 },
	{ (Il2CppRGCTXDataType)3, 14270 },
	{ (Il2CppRGCTXDataType)2, 3393 },
	{ (Il2CppRGCTXDataType)2, 10362 },
	{ (Il2CppRGCTXDataType)3, 38767 },
	{ (Il2CppRGCTXDataType)3, 38768 },
	{ (Il2CppRGCTXDataType)2, 4586 },
	{ (Il2CppRGCTXDataType)3, 38769 },
	{ (Il2CppRGCTXDataType)2, 734 },
	{ (Il2CppRGCTXDataType)2, 1116 },
	{ (Il2CppRGCTXDataType)3, 152 },
	{ (Il2CppRGCTXDataType)3, 33501 },
	{ (Il2CppRGCTXDataType)2, 7548 },
	{ (Il2CppRGCTXDataType)3, 33528 },
	{ (Il2CppRGCTXDataType)2, 1454 },
	{ (Il2CppRGCTXDataType)3, 2652 },
	{ (Il2CppRGCTXDataType)3, 33507 },
	{ (Il2CppRGCTXDataType)3, 12229 },
	{ (Il2CppRGCTXDataType)2, 855 },
	{ (Il2CppRGCTXDataType)3, 33502 },
	{ (Il2CppRGCTXDataType)2, 7543 },
	{ (Il2CppRGCTXDataType)3, 3079 },
	{ (Il2CppRGCTXDataType)2, 1476 },
	{ (Il2CppRGCTXDataType)2, 2643 },
	{ (Il2CppRGCTXDataType)3, 12247 },
	{ (Il2CppRGCTXDataType)3, 33503 },
	{ (Il2CppRGCTXDataType)3, 12224 },
	{ (Il2CppRGCTXDataType)3, 12225 },
	{ (Il2CppRGCTXDataType)3, 12223 },
	{ (Il2CppRGCTXDataType)3, 12226 },
	{ (Il2CppRGCTXDataType)2, 2639 },
	{ (Il2CppRGCTXDataType)2, 10011 },
	{ (Il2CppRGCTXDataType)3, 15399 },
	{ (Il2CppRGCTXDataType)3, 12228 },
	{ (Il2CppRGCTXDataType)2, 3963 },
	{ (Il2CppRGCTXDataType)3, 12227 },
	{ (Il2CppRGCTXDataType)2, 3818 },
	{ (Il2CppRGCTXDataType)2, 9947 },
	{ (Il2CppRGCTXDataType)2, 4076 },
	{ (Il2CppRGCTXDataType)2, 4355 },
	{ (Il2CppRGCTXDataType)3, 14249 },
	{ (Il2CppRGCTXDataType)2, 3386 },
	{ (Il2CppRGCTXDataType)3, 16250 },
	{ (Il2CppRGCTXDataType)3, 16251 },
	{ (Il2CppRGCTXDataType)3, 16256 },
	{ (Il2CppRGCTXDataType)2, 4580 },
	{ (Il2CppRGCTXDataType)3, 16253 },
	{ (Il2CppRGCTXDataType)3, 48125 },
	{ (Il2CppRGCTXDataType)2, 2647 },
	{ (Il2CppRGCTXDataType)3, 12277 },
	{ (Il2CppRGCTXDataType)1, 3954 },
	{ (Il2CppRGCTXDataType)2, 9959 },
	{ (Il2CppRGCTXDataType)3, 16252 },
	{ (Il2CppRGCTXDataType)1, 9959 },
	{ (Il2CppRGCTXDataType)1, 4580 },
	{ (Il2CppRGCTXDataType)2, 10360 },
	{ (Il2CppRGCTXDataType)2, 9959 },
	{ (Il2CppRGCTXDataType)3, 16257 },
	{ (Il2CppRGCTXDataType)3, 16255 },
	{ (Il2CppRGCTXDataType)3, 16254 },
	{ (Il2CppRGCTXDataType)2, 625 },
	{ (Il2CppRGCTXDataType)3, 12290 },
	{ (Il2CppRGCTXDataType)2, 834 },
};
extern const CustomAttributesCacheGenerator g_System_Core_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Core_CodeGenModule;
const Il2CppCodeGenModule g_System_Core_CodeGenModule = 
{
	"System.Core.dll",
	168,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	57,
	s_rgctxIndices,
	263,
	s_rgctxValues,
	NULL,
	g_System_Core_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
